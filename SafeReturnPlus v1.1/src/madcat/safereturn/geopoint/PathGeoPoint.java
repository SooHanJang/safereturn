package madcat.safereturn.geopoint;

import java.io.Serializable;

import com.google.android.maps.GeoPoint;

public class PathGeoPoint extends GeoPoint implements Serializable{
	
	private static final long serialVersionUID = 42L;
	
	public PathGeoPoint(int latitudeE6, int longitudeE6) {
		super(latitudeE6, longitudeE6);
	}
}
