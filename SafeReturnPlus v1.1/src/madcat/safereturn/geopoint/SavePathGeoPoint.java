package madcat.safereturn.geopoint;

import java.io.Serializable;

public class SavePathGeoPoint implements Serializable {

	private static final long serialVersionUID = 42L;
	
	private int mStartLatE6, mStartLonE6, mDestLatE6, mDestLonE6;
	
	public SavePathGeoPoint(int startLatE6, int startLonE6, int destLatE6, int destLonE6) {
		this.mStartLatE6 = startLatE6;
		this.mStartLonE6 = startLonE6;
		this.mDestLatE6 = destLatE6;
		this.mDestLonE6 = destLonE6;
	}
	
	public double getStartLat() {
		return mStartLatE6 / 1E6;
	}
	
	public double getStartLon() {
		return mStartLonE6 / 1E6;
	}
	
	public double getDestLat() {
		return mDestLatE6 / 1E6;
	}
	
	public double getDestLon() {
		return mDestLonE6 / 1E6;
	}

	public int getStartLatE6() {
		return mStartLatE6;
	}

	public void setStartLatE6(int startLatE6) {
		this.mStartLatE6 = startLatE6;
	}

	public int getStartLonE6() {
		return mStartLonE6;
	}

	public void setStartLonE6(int startLonE6) {
		this.mStartLonE6 = startLonE6;
	}

	public int getDestLatE6() {
		return mDestLatE6;
	}

	public void setDestLatE6(int destLatE6) {
		this.mDestLatE6 = destLatE6;
	}

	public int getDestLonE6() {
		return mDestLonE6;
	}

	public void setDestLonE6(int destLonE6) {
		this.mDestLonE6 = destLonE6;
	}

}
