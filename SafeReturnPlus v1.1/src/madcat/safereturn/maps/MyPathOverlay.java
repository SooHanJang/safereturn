package madcat.safereturn.maps;

import java.io.Serializable;

import madcat.safereturn.geopoint.PathGeoPoint;
import madcat.safereturn.geopoint.PathPoint;
import madcat.safereturn.geopoint.SavePathGeoPoint;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class MyPathOverlay extends Overlay implements Serializable {

	private static final long serialVersionUID = 42L;
//	private static final String TAG = "MyPathOverlay";
	
	private PathGeoPoint mStartGeoPoint, mEndGeoPoint;
	private SavePathGeoPoint mSavePathGeoPoint;
	private Paint mPaint;
	private PathPoint mP1, mP2;
	
	public void setMyPathOverlay(SavePathGeoPoint saveGeoPoint, PathPoint p1, PathPoint p2, Paint paint) {
		this.mSavePathGeoPoint = saveGeoPoint;
		this.mP1 = p1;
		this.mP2 = p2;
		this.mPaint = paint;
		
		createGeoPoint();
	}
	
	public void createGeoPoint() {
		this.mStartGeoPoint = new PathGeoPoint(mSavePathGeoPoint.getStartLatE6(), mSavePathGeoPoint.getStartLonE6());
		this.mEndGeoPoint = new PathGeoPoint(mSavePathGeoPoint.getDestLatE6(), mSavePathGeoPoint.getDestLonE6());
	}
	
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		super.draw(canvas, mapView, shadow);
		
		Path mPath = new Path();
		
		Projection projection = mapView.getProjection();
		
		projection.toPixels(mStartGeoPoint, mP1);
		projection.toPixels(mEndGeoPoint, mP2);
		
		mPath.moveTo(mP2.x, mP2.y);
		mPath.lineTo(mP1.x, mP1.y);
		
		canvas.drawPath(mPath, mPaint);
	}
	
	public String getStartGeoString() {
		return mStartGeoPoint.toString();
	}
	
	public String getDestGeoString() {
		return mEndGeoPoint.toString();
	}

	
}
