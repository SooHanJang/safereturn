package madcat.safereturn.maps;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.Utils;
import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class DestPinOverlay extends Overlay {
	
//	private static final String TAG = "DestPinOverlay";
	
	private ImageView mPinImage;
	private Context mContext;
	private MessagePool mMessagePool;
	private GeoPoint mGeoPoint;

	private int mViewFlag;
	private String mDestAddr;

	public DestPinOverlay(Context context, int flag) {
		mContext = context;
		mMessagePool = (MessagePool)mContext.getApplicationContext();
		mPinImage = new ImageView(mContext);
		mViewFlag = flag;
	}
	
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		super.draw(canvas, mapView, shadow);
		createPinImage(mGeoPoint, mapView);
	}
	
	private void createPinImage(GeoPoint p, MapView mapView) {
		mapView.removeView(mPinImage);
		
		MapView.LayoutParams pin_params = Utils.getMapLayoutParams(40, 80, p);
		pin_params.mode = MapView.LayoutParams.MODE_MAP;
		
		mPinImage.setBackgroundResource(R.drawable.pin_marker_dest);
		mPinImage.setLayoutParams(pin_params);
		mPinImage.setVisibility(View.VISIBLE);
		
		mapView.addView(mPinImage, pin_params);
	}
	
	public boolean onTap(GeoPoint p, MapView mapView) {
		if(mGeoPoint != null) {
			int compareDistance = (int)Utils.distanceBetween(mGeoPoint.getLatitudeE6()/1E6, mGeoPoint.getLongitudeE6()/1E6, 
					p.getLatitudeE6()/1E6, p.getLongitudeE6()/1E6);
			
//			Log.d(TAG, "compareDistance : " + compareDistance);
			
			if(compareDistance <= 25) {
				if(mViewFlag == Constants.PIN_MAIN_VIEW_FLAG && mMessagePool.getEditDest().getText().toString().length() != 0) {
					Toast.makeText(mContext, mMessagePool.getEditDest().getText().toString(), Toast.LENGTH_SHORT).show();
				} else if(mViewFlag == Constants.PIN_PATH_VIEW_FLAG) {
					if(mDestAddr.length() != 0) {
						Toast.makeText(mContext, mDestAddr, Toast.LENGTH_SHORT).show();
					}
				}
			}
		}
		
		return super.onTap(p, mapView);
	}
	
	public void setGeoPoint(GeoPoint geoPoint) { mGeoPoint = geoPoint; }
	public void setDestAddr(String destAddr) { mDestAddr = destAddr; }
	
	public void clearOverlay(MapView mapView) {
		if(mPinImage.isShown()) {
//			Log.d(TAG, "mPinImage ȣ��");
			mapView.removeView(mPinImage);
		}
	}
}
