package madcat.safereturn.maps;

import madcat.safereturn.utils.MessagePool;
import android.content.Context;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

import com.google.android.maps.MapView;

public class ZoomOnGestureListener extends SimpleOnGestureListener {
	
//	private static final String TAG = "ZoomOnGestureListener";
	private MapView mapView = null;
	private MessagePool mMessagePool;

	public ZoomOnGestureListener(MapView mapView, Context context) {
		this.mapView = mapView;
		this.mMessagePool = (MessagePool)context.getApplicationContext();
	}

//	public boolean onDoubleTap(MotionEvent e) {
//		mapView.getController().zoomIn();
//		return true;
//	}
	
	public void onLongPress(MotionEvent e) {
//		Log.d(TAG, "�� Ŭ�� ����");
		mMessagePool.setLongClickFlag(true);
//		Log.d(TAG, "�� Ŭ�� ��");
	}
}
