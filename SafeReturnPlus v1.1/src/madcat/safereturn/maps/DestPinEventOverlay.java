package madcat.safereturn.maps;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.Utils;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class DestPinEventOverlay extends Overlay {

//	private static final String TAG 						=	"DestPinEventOverlay";
	private final int DEFAULT_OVERLAY_SIZE					=	3;

	private Context mContext;
	private MessagePool mMessagePool;
	
	private ImageView mPinImage;
	private Button mBtnPin;
	private String mAddress;
	
	private MapView mMapView;
	
	public DestPinEventOverlay(Context context) {
		mContext = context;
		mMessagePool = (MessagePool)mContext.getApplicationContext();
		mPinImage = new ImageView(mContext);
		mBtnPin = new Button(mContext);
	}
	
	class AsyncGetAddres extends AsyncTask<GeoPoint, GeoPoint, Void> {
		ProgressDialog progressDialog;
		
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(mContext, null, "현재 위치의 주소를 가져오는 중...");
		}
		
		protected Void doInBackground(GeoPoint... geoPoint) {
//			Log.d(TAG, "전달할 주소 좌표 : " + geoPoint[0].getLatitudeE6()/1E6 + ", " + geoPoint[0].getLongitudeE6()/1E6);
			
			mAddress = Utils.getAddress(mContext, geoPoint[0].getLatitudeE6()/1E6, geoPoint[0].getLongitudeE6()/1E6);
			publishProgress(geoPoint);
			
			return null;
		}
		
		protected void onProgressUpdate(GeoPoint... geoPoint) {
			if(mAddress != null) {
				if(mAddress.length() != 0) {
					mMessagePool.getEditDest().setText(mAddress);
					mMessagePool.setGeoDest(geoPoint[0]);
				} else {
					if(mPinImage != null) {
						if(mPinImage.isShown()) {
							mMapView.removeView(mPinImage);
						}
					}
					
					Toast.makeText(mContext, "현재 주소를 가져올 수 없습니다. 3G 나 Wifi 상태를 확인하여 주세요.", Toast.LENGTH_SHORT).show();
				}
			} else {
				if(mPinImage != null) {
					if(mPinImage.isShown()) {
						mMapView.removeView(mPinImage);
					}
				}
				
				Toast.makeText(mContext, "현재 주소를 가져올 수 없습니다. 3G 나 Wifi 상태를 확인하여 주세요.", Toast.LENGTH_SHORT).show();
			}
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}
	}
	
	@Override
	public boolean onTap(final GeoPoint p, final MapView mapView) {
		// Long Click 이 발생
		if(mMessagePool.getReturningFlag() == false &&	mMessagePool.getLongClickFlag() == true 
				&& mMessagePool.getDestFlag() == false) {
			
			mMapView = mapView;
			
			// 위치 설정 등록이 클릭되서 화면에 이미 표시되어져 있는 상태라면 화면에 표시된 정보들을 전부 다시 초기화 한다.
			if(mMessagePool.getFavoriteFlag() != false) {
				int mapOverlaySize = mapView.getOverlays().size();
				
				for(int i=(mapOverlaySize-1); i >= DEFAULT_OVERLAY_SIZE; i--) {
					mapView.getOverlays().remove(i);
				}
				
				mapView.removeAllViews();
				mapView.invalidate();
				
				mMessagePool.getEditHourText().setText("");
				mMessagePool.getEditMinText().setText("");
				mMessagePool.setFavoriteFlag(false);
			}
			
			createPinImage(p, mapView);

			AsyncGetAddres asyncGetAddress = new AsyncGetAddres();
			asyncGetAddress.execute(p);
		} else if(mMessagePool.getReturningFlag() == false && mMessagePool.getLongClickFlag() == false && 
				mMessagePool.getDestFlag() == true) {		// Long 클릭이 아닌 onTap 이 발생하였다면
			createPinButton(p, mapView);									// 도착지 설정이 담겨있는 핀 버튼 레이아웃 이미지를 표시
		} 
		
		mMessagePool.setLongClickFlag(false);
		return super.onTap(p, mapView);
	}
	
	private void createPinButton(final GeoPoint p, final MapView mapView) {
		if(mPinImage.isShown()) {
			mapView.removeView(mPinImage);
		}
		
		mapView.removeView(mBtnPin);
		
		MapView.LayoutParams btnPin_params = Utils.getMapLayoutParams(MapView.LayoutParams.WRAP_CONTENT, MapView.LayoutParams.WRAP_CONTENT, p);
		btnPin_params.mode = MapView.LayoutParams.MODE_MAP;
		
		mBtnPin.setText(Constants.DEST_BALLON_TEXT);
		mBtnPin.setBackgroundResource(R.drawable.pin_marker_destballon);
		mBtnPin.setLayoutParams(btnPin_params);
		mBtnPin.setVisibility(View.VISIBLE);
		
		mBtnPin.setOnClickListener(new OnClickListener() {				// 도착지로 설정 버튼이 클릭되었다면
			public void onClick(View v) {
				// 도착지에다가 현재 선택된 장소의 주소를 입력한 뒤
				
				mMessagePool.setDestFlag(false);
				mMessagePool.getSummaryLayout().setVisibility(View.GONE);
				mMessagePool.getFullLayout().setVisibility(View.VISIBLE);
				
				if(mMessagePool.getFavoriteFlag() != false) {
					int mapOverlaySize = mapView.getOverlays().size();
					
					for(int i=(mapOverlaySize-1); i >= DEFAULT_OVERLAY_SIZE; i--) {
						mapView.getOverlays().remove(i);
					}
					
					mapView.removeAllViews();
					mapView.invalidate();
					
					mMessagePool.getEditHourText().setText("");
					mMessagePool.getEditMinText().setText("");
					mMessagePool.setFavoriteFlag(false);
				}
				
				createPinImage(p, mapView);
				
				AsyncGetAddres asyncGetAddress = new AsyncGetAddres();
				asyncGetAddress.execute(p);
			}
		});
		
		mapView.addView(mBtnPin, btnPin_params);
	}
	
	public void createPinImage(GeoPoint p, MapView mapView) {
		if(mBtnPin.isShown()) {
			mapView.removeView(mBtnPin);
		}
		
		mapView.removeView(mPinImage);
		
		MapView.LayoutParams pin_params = Utils.getMapLayoutParams(40, 80, p);
		pin_params.mode = MapView.LayoutParams.MODE_MAP;
		
		mPinImage.setBackgroundResource(R.drawable.pin_marker_dest);
		mPinImage.setLayoutParams(pin_params);
		mPinImage.setVisibility(View.VISIBLE);
		
		mapView.addView(mPinImage, pin_params);
	}
	
	public void clearOverlay(MapView mapView) {
		if(mPinImage.isShown()) {
			mapView.removeView(mPinImage);
		}
	}
}
