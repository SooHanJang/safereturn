package madcat.safereturn.maps;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.Toast;

import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;

public class PoliceOverlay extends ItemizedOverlay<OverlayItem> {
	private ArrayList<OverlayItem> mOverlays = new ArrayList<OverlayItem>();
	private Context mContext;

	public PoliceOverlay(Drawable defaultMarker, Context context) {
		super(boundCenter(defaultMarker));
		mContext = context;
	}
	
	@Override
	protected OverlayItem createItem(int i) {
		return mOverlays.get(i);
	}
	
	@Override
	protected boolean onTap(int index) {
		Toast.makeText(mContext, mOverlays.get(index).getTitle() + " Tel : " + mOverlays.get(index).getSnippet(), Toast.LENGTH_SHORT).show();
		
		return super.onTap(index);
	}

	@Override
	public int size() {
		return mOverlays.size();
	}
	
	public void addOverlay(OverlayItem overlay) {
		mOverlays.add(overlay);
		populate();
	}
	
	public void overlayPopulate() {
		populate();
	}
	
	public void clearOverlay() {
		mOverlays.clear();
	}
}
