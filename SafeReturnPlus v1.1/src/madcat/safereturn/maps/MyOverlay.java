package madcat.safereturn.maps;

import madcat.safereturn.pro.R;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.Utils;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.location.Location;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;

public class MyOverlay extends MyLocationOverlay {

//	private static final String TAG					=	"MyOverlay";
	private final double TEMP_LAT					=	37341047d;
	
	private Context mContext;
	private MessagePool mMessagePool;
	private Bitmap mMyMarker = null;
	private MapView mMapView;
	private boolean mHitedLoc = false;
	private Paint mPaint;
	private Point mPoint;
	private int mMeterRadius;
	private int mRangeValue;
	private RectF mRectOveal;
	private GeoPoint mMyLocation;
	
	public MyOverlay(Context context, MapView mapView, int range) {
		super(context, mapView);
//		Log.d(TAG, "myOverlay 생성자 호출");
		
		mContext = context;
		mMessagePool = (MessagePool)mContext.getApplicationContext();
		mMapView = mapView;
		mRangeValue = range;
		mMyMarker = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.pin_marker_me);
		
		initMyOverlay(mapView);
	}
	
	public void setMyLocation(GeoPoint geoPoint) {
		this.mMyLocation = geoPoint;
	}
	
	private void initMyOverlay(MapView mapView) {
		// 페인트 객체 생성 및 설정
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setStyle(Paint.Style.FILL);
		mPaint.setColor(Color.BLUE);
		mPaint.setAlpha(50);
		
		mPoint = new Point();
		mRectOveal = new RectF();
	}
	
	@Override
	protected void drawMyLocation(Canvas canvas, MapView mapView,
			Location lastFix, GeoPoint myLocation, long when) {
		
//		Log.d(TAG, "drawMyLocation 호출");

		// 포인트 객체 생성 및 설정
		mapView.getProjection().toPixels(myLocation, mPoint);
		
		// 내 위치 아이콘을 그려주는 과정
		canvas.drawBitmap(mMyMarker, mPoint.x - (mMyMarker.getWidth() / 2), mPoint.y - (mMyMarker.getHeight() / 2), null);
		
		mMeterRadius = Utils.metersToRadius(mRangeValue, mapView, TEMP_LAT);
		
		// 반경을 그리기 위한 과정
		mRectOveal.set(mPoint.x - mMeterRadius, mPoint.y - mMeterRadius, mPoint.x + mMeterRadius, mPoint.y + mMeterRadius);
		canvas.drawOval(mRectOveal, mPaint);
	}
	
	@Override
	public synchronized void onLocationChanged(Location location) {
		super.onLocationChanged(location);
		
		// 안전귀가 혹은 긴급 보안 모드를 시작하면 사용자의 현재 위치로 Map 을 animate 하기
		if(mMessagePool.getReturningFlag() || mMessagePool.getEmergencyFlag()) {		
			mMapView.getController().animateTo(getMyLocation());
		}
	}
	
	@Override
	public boolean onTap(GeoPoint p, MapView map) {
		mHitedLoc = false;
		return super.onTap(p, map);
	}
	
	@Override
	protected boolean dispatchTap() {				// 자신의 위치에 onTap 이 발생했을 경우 호출되는 callBack 메소드
		mHitedLoc = true;
//		Toast.makeText(mContext, "날 클릭했습니다.", Toast.LENGTH_SHORT).show();
		return super.dispatchTap();
	}
}
