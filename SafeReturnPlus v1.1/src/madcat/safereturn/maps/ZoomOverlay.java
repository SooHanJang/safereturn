package madcat.safereturn.maps;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class ZoomOverlay extends Overlay {
//	private static final String TAG = "ZoomOverlay";
	private GestureDetector gestureDetector = null;
	private Context mContext;
	
	public ZoomOverlay(Context context, MapView mapView) {
//		Log.d(TAG, "ZoomOverlay 생성자 호출");
		this.mContext = context;
		gestureDetector = new GestureDetector(mContext, new ZoomOnGestureListener(mapView, context));
	}

	public boolean onTouchEvent(MotionEvent motionEvent, MapView mapView) {					// 현재 실행중인 화면에 터치가 발생했을 경우 이를 감지하고 처리하는 메소드
		return gestureDetector.onTouchEvent(motionEvent);
	}
}
