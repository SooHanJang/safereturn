package madcat.safereturn.service;

import madcat.safereturn.pro.R;
import madcat.safereturn.blackbox.BCtrlService;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.AddressModifyDialog;
import madcat.safereturn.dialog.DestEndPopUpDialog;
import madcat.safereturn.maps.DestPinEventOverlay;
import madcat.safereturn.maps.ZoomOverlay;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.Utils;
import madcat.safereturn.widget.WidgetSafeReturn;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothServerSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

public class MapReceiver extends BroadcastReceiver {

//	private static final String TAG = " MapReceiver";
	private Context mContext;
	private MessagePool mMessagePool;
	private MapView mMapView;
	private String mStartAdr, mDestAdr;
	
	// 진동 관련 변수
	private Thread mVibratorThread;
	private boolean mVibratorFlag										=	true;
	private final int VIBRATOR_DELAY									=	1500;
	
	@Override
	public void onReceive(final Context context, Intent intent) {
		mMessagePool = (MessagePool)context.getApplicationContext();
		
		final boolean destEnter = intent.getBooleanExtra(LocationManager.KEY_PROXIMITY_ENTERING, true);
		
		final Vibrator vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);
		
		// 진동 설정
		if(mVibratorThread == null) {
			mVibratorThread = new Thread(new Runnable() {
				public void run() {
					Vibrator vi = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);
	
					while(mVibratorFlag) {
						vi.vibrate(VIBRATOR_DELAY);
						SystemClock.sleep(VIBRATOR_DELAY + 1000);
					}
				}
			});
		}
		
		if(destEnter == true) {		// 도착지에 도착하였다면
//			Log.d(TAG, "도착지 도착 호출");
			mVibratorThread.start();
			
			mMapView = mMessagePool.getMapView();
			mContext = mMessagePool.getContext();
			
			final DestEndPopUpDialog destEndPopUpDialog = new DestEndPopUpDialog(mContext, R.style.PopupDialog, 
					mMessagePool.getFavoriteFlag());
			destEndPopUpDialog.show();
			destEndPopUpDialog.setOnDismissListener(new OnDismissListener() {
				public void onDismiss(DialogInterface dialog) {
					if(destEndPopUpDialog.getFlag() == 1) {		// 확인 버튼 클릭
						resetMapView(mMapView, mContext);			// 화면에 그려진 경로 초기화
						mMessagePool.setReturningTimerFlag(false);		// 귀가 타이머 종료
						
						Utils.widgetStateUpdate(context, WidgetSafeReturn.WIDGET_STATE_START_NEW);
						NotificationSafeReturn.getInstance().removeNotification();		// 등록된 Notification 해제
						
						mContext.stopService(new Intent(mContext, SensorCallService.class));
						mContext.stopService(new Intent(mContext, BCtrlService.class));
						mMessagePool.setRunningBService(false);
						
					} else if(destEndPopUpDialog.getFlag() == 2) {		// 위치 등록 버튼 클릭
						mMessagePool.setReturningTimerFlag(false);		// 귀가 타이머 종료
						
						GeoPoint startGeoPoint = mMessagePool.getGeoStart();
						GeoPoint destGeoPoint = mMessagePool.getGeoDest();
						
						double startLat = startGeoPoint.getLatitudeE6() / 1E6;
						double startLon = startGeoPoint.getLongitudeE6() / 1E6;
						double destLat = destGeoPoint.getLatitudeE6() / 1E6;
						double destLon = destGeoPoint.getLongitudeE6() / 1E6;
						
						AsyncGetAddress asyncGetAddressPopup = new AsyncGetAddress(startLat, startLon, destLat, destLon);
						asyncGetAddressPopup.execute();
						
						Utils.widgetStateUpdate(context, WidgetSafeReturn.WIDGET_STATE_START_NEW);
						NotificationSafeReturn.getInstance().removeNotification();		// 등록된 Notification 해제
						mContext.stopService(mMessagePool.getServiceIntent());		// 서비스 중지
						mContext.stopService(mMessagePool.getBServiceIntent());
						mMessagePool.setRunningBService(false);
					}
					
					// WakeLock Service 해제
					WakeLockService serviceWakeLock = WakeLockService.getInstance();
					serviceWakeLock.releaseWakeLock();
					
					// 진동 Thread 해제
					mVibratorFlag = false;
					mVibratorThread = null;
				}
			});
		}
	}
	
	class AsyncGetAddress extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog progressDialog;
		double startLat, startLon, destLat, destLon;
		
		public AsyncGetAddress(double startLat, double startLon, double destLat, double destLon) {
			this.startLat = startLat;
			this.startLon = startLon;
			this.destLat = destLat;
			this.destLon = destLon;
		}
		 
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(mContext, "", "잠시만 기다려 주세요...");
		}
		 
	    protected Void doInBackground(Void... params) {
	    	mStartAdr = Utils.getAddress(mContext, startLat, startLon);
	    	mDestAdr = Utils.getAddress(mContext, destLat, destLon);
	    	
	    	if(mStartAdr == null) {
	    		mStartAdr = "";
	    	}
	    	
	    	if(mDestAdr == null) {
	    		mDestAdr = "";
	    	}
	    	
	    	return null;
	    }
		 
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			
			showRegisterPathDialog(mContext, mStartAdr, mDestAdr, startLat, startLon, destLat, destLon);
	    }
		 
	 }
	
	private void showRegisterPathDialog(final Context context, final String startAddress, final String destAddress, 
			final double startLat, final double startLon, final double destLat, final double destLon) {
		
		final Dialog registePathDialog = new Dialog(context, R.style.PopupDialog);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		registePathDialog.getWindow().setAttributes(lpWindow);
		
		registePathDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		registePathDialog.setContentView(R.layout.dialog_path_register);
		
		final int destHour = mMessagePool.getDestHour();
		final int destMin = mMessagePool.getDestMin();
		
		final EditText editStart = (EditText)registePathDialog.findViewById(R.id.dialog_path_register_input_start);		// 출발지명 객체 생성
		final EditText editDest = (EditText)registePathDialog.findViewById(R.id.dialog_path_register_input_dest);		// 도착지명 객체 생성
		final EditText editStartAddress = (EditText)registePathDialog.findViewById(R.id.dialog_path_register_input_start_address);
		final EditText editDestAddress = (EditText)registePathDialog.findViewById(R.id.dialog_path_register_input_dest_address);
		
		TextView textDestTime = (TextView)registePathDialog.findViewById(R.id.dialog_path_time);
		ImageButton btnModifyStartAddress = (ImageButton)registePathDialog.findViewById(R.id.dialog_path_register_start_address_modify);
		ImageButton btnModifyDestAddress = (ImageButton)registePathDialog.findViewById(R.id.dialog_path_register_dest_address_modify);
		ImageButton btnOk = (ImageButton)registePathDialog.findViewById(R.id.dialog_path_register_btn_ok);
		ImageButton btnCancel = (ImageButton)registePathDialog.findViewById(R.id.dialog_path_register_btn_cancel);

		editStartAddress.setText(startAddress);		// 시작 주소 설정
		editDestAddress.setText(destAddress);		// 도착주소 설정
		textDestTime.setText(destHour + "시간 " + destMin + "분 소요");
		
		btnModifyStartAddress.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				registePathDialog.dismiss();
				
				final AddressModifyDialog startAddrModifyDialog = new AddressModifyDialog(mContext, R.style.PopupDialog, 
						Constants.DIALOG_FLAG_MOIDFY_START_ADDR, editStartAddress.getText().toString());
				startAddrModifyDialog.show();
				
				startAddrModifyDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						editStartAddress.setText(startAddrModifyDialog.getAddrText());
						mStartAdr = startAddrModifyDialog.getAddrText();
						registePathDialog.show();
					}
				});
			}
		});
		
		btnModifyDestAddress.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				registePathDialog.dismiss();
				
				final AddressModifyDialog destAddrModifyDialog = new AddressModifyDialog(mContext, R.style.PopupDialog, 
						Constants.DIALOG_FLAG_MOIDFY_DEST_ADDR, editDestAddress.getText().toString());
				destAddrModifyDialog.show();
				
				destAddrModifyDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						editDestAddress.setText(destAddrModifyDialog.getAddrText());
						mDestAdr = destAddrModifyDialog.getAddrText();
						registePathDialog.show();
					}
				});
			}
		});
		
		btnOk.setOnClickListener(new View.OnClickListener() {	// 확인 버튼을 누르면 위치 등록 정보들을 전부 DB 에 INSERT
			public void onClick(View v) {
				
				byte[] pathBytes = Utils.serializeObject(mMessagePool.getArraySavePathGeoPoint());
				
				// DB 에 Path 삽입을 위해 준비
				String startName = editStart.getText().toString();
				String destName = editDest.getText().toString();		// 사용자가 입력한 경로명을 저장하기 위한 String 변수
				
				if(startName.length() != 0 && destName.length() != 0 &&
						mStartAdr.length() != 0 && mDestAdr.length() != 0) {		// 출발지명과 도착지명이 비어있지 않다면
					AsyncRegistePath asyncRegistePath = 
						new AsyncRegistePath(startName, destName, startLat, startLon, destLat, 
								destLon, destHour, destMin, pathBytes, registePathDialog);
					asyncRegistePath.execute();
					
				} else {
					Toast.makeText(mContext, "등록할 경로명을 입력해 주세요", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		btnCancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				registePathDialog.dismiss();
				
				// 이전 Dialog 를 계속해서 호출해주기 위해, 현재 상태를 그대로 다시 Receiver 를 호출한다.
				Intent intent = new Intent(context, MapReceiver.class);
				PendingIntent pending = PendingIntent.getBroadcast(context, 1, intent, 0);
				
				try {
					pending.send();
				} catch (CanceledException e) {
					e.printStackTrace();
				}
			}
		});
		registePathDialog.show();
	}
	
	private class AsyncRegistePath extends AsyncTask<Void, Void, Void> {

		private double startLat, startLon, destLat, destLon;
		private String startName, destName;
		private int destHour, destMin;
		private byte[] pathBytes;
		private ProgressDialog progressDialog;
		private Dialog registePathDialog;
		
		public AsyncRegistePath(String startName, String destName, double startLat, double startLon, double destLat, double destLon,
							int destHour, int destMin, byte[] pathBytes, Dialog registePathDialog) {
			this.startName = startName;
			this.destName = destName;
			this.startLat = startLat;
			this.startLon = startLon;
			this.destLat = destLat;
			this.destLon = destLon;
			this.destHour = destHour;
			this.destMin = destMin;
			this.pathBytes = pathBytes;
			this.registePathDialog = registePathDialog;
		}
		
		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(mContext, null, "경로를 등록중입니다...");
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			String startAddr = Utils.getAddress(mContext, startLat, startLon);
			String destAddr = Utils.getAddress(mContext, destLat, destLon);
			
			Utils.insertDbPath(startName, startLat, startLon, startAddr, 
					destName, destLat, destLon, destAddr, destHour, destMin, pathBytes);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			
			resetMapView(mMapView, mContext);			// 위치 정보를 저장하고 나면 화면에 그려진 경로 초기화
			registePathDialog.dismiss();
		}
	}
	
	// 다시 처음 설정부분으로 돌아가도록 초기화하는 메소드
	private void resetMapView(MapView mapView, Context context) {
//		Log.d(TAG, "resetDest 호출");
		
		LocationManager locationManager = mMessagePool.getLocationManager();
		PendingIntent pending = mMessagePool.getDestPendingIntent();
		RelativeLayout fullSettingLayout = mMessagePool.getFullLayout();
		LinearLayout emergencyLayout = mMessagePool.getEmergencyLayout();
		
		mMessagePool.setReturningFlag(false);
		mMessagePool.setFavoriteFlag(false);
		
		if(mMessagePool.getAddProximityFlag()== true) {
			locationManager.removeProximityAlert(pending);			// 등록된 addProximityAlert 을 제거한 뒤
			mMessagePool.setAddProximityFlag(false);
		}
		
		// 화면에 Layout 을 설정 레이아웃으로 바꿔준 뒤
		fullSettingLayout.setVisibility(View.VISIBLE);
		emergencyLayout.setVisibility(View.GONE);
		
//		Log.d(TAG, "mapView Overlay 삭제");
		mapView.getOverlays().clear();

		// 사용자의 경로를 기록해둔 ArrayList를 초기화
		if(mMessagePool.getArraySavePathGeoPoint() != null) {
			mMessagePool.getArraySavePathGeoPoint().clear();
		}
		
		// 기본으로 사용할 Overlay 재등록
		Utils.setMyLocationOverlay(mapView, context, mMessagePool.getRange());
		
		DestPinEventOverlay destPinEventOverlay = new DestPinEventOverlay(context);
		mapView.getOverlays().add(destPinEventOverlay);
		mapView.getOverlays().add(new ZoomOverlay(context, mapView));
		
		mapView.removeAllViews();
		mapView.invalidate();
		
		mMessagePool.setMapView(mapView);
	}
}
