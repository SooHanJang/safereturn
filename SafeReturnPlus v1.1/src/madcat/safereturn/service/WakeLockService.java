package madcat.safereturn.service;

import madcat.safereturn.constants.Constants;
import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

public class WakeLockService {
	
	private final String TAG										=	"WakeLockService";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private static WakeLockService mInstance						=	new WakeLockService();

	private WakeLock mWakeLock;
	
	private WakeLockService() {}
	
	public void registeWakeLock(Context context) {
		if(mWakeLock == null) {
			PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
			mWakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, TAG);
			mWakeLock.acquire();
		}
	}
	
	public void releaseWakeLock() {
		if(mWakeLock != null) {
			mWakeLock.release();
			mWakeLock = null;
		}
	}
	
	public static WakeLockService getInstance() {
		return mInstance;
	}
}
