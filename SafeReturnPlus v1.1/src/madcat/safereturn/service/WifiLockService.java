package madcat.safereturn.service;

import madcat.safereturn.constants.Constants;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.util.Log;

public class WifiLockService {

	private static final String TAG										=	"WifiLockService";
	private static final boolean DEVELOPE_MODE							=	Constants.DEVELOPE_MODE;
	
	private static WifiLock mWifiLock;
	
	public static void registeWifiLock(Context context) {
		if(mWifiLock == null) {
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "registeWifiLock");
			}
			
			
			WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
			mWifiLock = wifiManager.createWifiLock(TAG);
			mWifiLock.setReferenceCounted(true);
			mWifiLock.acquire();
		}
	}
	
	public static void releaseWifiLock() {
		if(mWifiLock != null) {
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "releaseWifiLock");
			}
			
			mWifiLock.release();
			mWifiLock = null;
		}
	}
	
}
