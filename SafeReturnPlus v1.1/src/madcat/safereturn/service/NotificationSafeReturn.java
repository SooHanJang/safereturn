package madcat.safereturn.service;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.menu.EmergencyMode;
import madcat.safereturn.menu.StartReturn;
import madcat.safereturn.utils.MessagePool;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class NotificationSafeReturn {
	
//	private static final String TAG = "SafeNotification";
	
	private static NotificationSafeReturn mSafeNotification = new NotificationSafeReturn();
	
	private Context mContext;
	private MessagePool mMessagePool;
	private NotificationManager mNotificationManager;
	private Notification mNotification;
	private PendingIntent mPendingIntent;
	
	private NotificationSafeReturn() {}
	
	public void setContext(Context context) {
		mContext = context;
		mMessagePool = (MessagePool)mContext.getApplicationContext();
	}
	
	public static NotificationSafeReturn getInstance() {
		return mSafeNotification;
	}
	
	public void registNotification() {
		 mNotificationManager = (NotificationManager)mContext.getSystemService(Context.NOTIFICATION_SERVICE);
		 
		 Intent intent = null;
		 
		 if(mMessagePool.getEmergencyFlag()) {
			 intent = new Intent(mContext, EmergencyMode.class);
		 } else {
			 intent = new Intent(mContext, StartReturn.class);
		 }
		 
		 mPendingIntent = PendingIntent.getActivity(mContext, 0, intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK 
															   				   | Intent.FLAG_ACTIVITY_CLEAR_TOP 
															   				   | Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
	}
	
	public void updateNotification(String safeRegeion, int cctvNumber, int convenienceNumber, int policeNumber) {
		
		mNotification = new Notification(R.drawable.notification_icon_green, "현재 지역은 " + safeRegeion + "입니다.", 
				System.currentTimeMillis());
		mNotification.flags = Notification.FLAG_ONGOING_EVENT;
		
		if(safeRegeion.equals("안전 지역") || safeRegeion.equals("일반 지역")) {
			mNotification.icon=R.drawable.notification_icon_green;
		} else if(safeRegeion.equals("주의 지역")) {
			mNotification.icon=R.drawable.notification_icon_yellow;
		} else {
			mNotification.icon=R.drawable.notification_icon_red;
		}
		
		mNotification.setLatestEventInfo(mContext, "SafeReturn Alert 작동 중", safeRegeion + " CCTV(" + cctvNumber + "), " +
				"편의점(" + convenienceNumber + "), " +
				"경찰서(" + policeNumber + ")", mPendingIntent);
		mNotificationManager.notify(Constants.APP_NUMBER, mNotification);
	}
	
	public void removeNotification() {
		if(mNotificationManager != null) {
			mNotificationManager.cancel(Constants.APP_NUMBER);
		}
	}
	
	public Notification getNotification() {
		return mNotification;
	}
}
