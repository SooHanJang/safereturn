package madcat.safereturn.service;

import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.EmergencySiren;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

public class SirenAlarmService extends Activity {
	
	private final String TAG										=	"ServiceSirenAlarm";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private EmergencySiren mEmergencySiren;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mContext = this;
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "mEmergencySiren Object : " + mEmergencySiren);
		}

		mEmergencySiren = mEmergencySiren.getInstance();
		mEmergencySiren.setContext(mContext);
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "플레이 상태 : " + mEmergencySiren.isPlaySiren());
		}
		
		if(!mEmergencySiren.isPlaySiren()) {
			mEmergencySiren.initMediaPlayer();
			mEmergencySiren.startMediaPlayer();
		} else {
			mEmergencySiren.stopMediaPlayer();
		}
		
		finish();
	}
}
