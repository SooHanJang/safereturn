package madcat.safereturn.service;

import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.ShakeCallSensor;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

public class SensorCallService extends Service {
	
//	private static final String TAG										=	"SafeReturnService";
	private static final int TIMER_PERIOD								=	3 * 1000;
	
	private Context mContext;
	
	private ShakeCallSensor mShakeSensor;
	private MessagePool mMessagePool;
	private Thread mServiceThread;
	private SharedPreferences mConfigPref;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		mContext = this;
		mMessagePool = (MessagePool)mContext.getApplicationContext();
		mConfigPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mShakeSensor = new ShakeCallSensor(this);
		
		mShakeSensor.registerSensor();
		mServiceThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while(mMessagePool.getServiceFlag()) {
						String friendsTel = mConfigPref.getString(Constants.SETTING_EMERGENCY_TEL, "112");
						
//						Log.d(TAG, "친구 전화번호 : " + friendsTel);
						
						mShakeSensor.setEmergencyTel(friendsTel, 0);
						Thread.sleep(TIMER_PERIOD);
						
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		
		if(!mServiceThread.isAlive()) {
			mServiceThread.start();
		}
		
		startForeground(Constants.APP_NUMBER, NotificationSafeReturn.getInstance().getNotification());
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mMessagePool.setServiceFlag(false);
		mShakeSensor.unregisterSensor();
		
		stopForeground(true);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}






