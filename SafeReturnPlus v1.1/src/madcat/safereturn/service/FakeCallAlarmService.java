package madcat.safereturn.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import madcat.safereturn.adapter.EmergencyAddress;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.fake.FakeCall;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

public class FakeCallAlarmService extends Activity {
	
	private final String TAG										=	"FakeCallAlarm";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private MessagePool mMessagePool;
	private SharedPreferences mConfigPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		mContext = this;
		mConfigPref = mContext.getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mMessagePool = (MessagePool)getApplicationContext();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "FakeCall Flag 상태 : " + mMessagePool.getFakeCallFlag());
		}
		
		if(!mMessagePool.getFakeCallFlag()) {
			String fakeTelName = null;
			String fakeTelNumber = null;
			int fakeDelayTime = mConfigPref.getInt(Constants.SETTING_FAKE_TIME, 5);
			
			if(mConfigPref.getBoolean(Constants.SETTING_FAKE_CUSTOM_CHECK, false)) {
				fakeTelName = mConfigPref.getString(Constants.SETTING_FAKE_CUSTOM_NAME, "");
				fakeTelNumber = mConfigPref.getString(Constants.SETTING_FAKE_CUSTOM_PHONE_NUMBER, "");
			} else {
				ArrayList<EmergencyAddress> friendsItems = Utils.getEmergencyFriendsList();
				Random random = new Random(System.currentTimeMillis());
				int itemSize = friendsItems.size();
				int telPosition = random.nextInt(itemSize);

				fakeTelName = friendsItems.get(telPosition).getFriendsName();
				fakeTelNumber = friendsItems.get(telPosition).getFriendsTel();
			}
			
			Intent fakeCallIntent = new Intent(FakeCallAlarmService.this, FakeCall.class);
			fakeCallIntent.putExtra(Constants.PUT_FAKE_CALL_NAME, fakeTelName);
			fakeCallIntent.putExtra(Constants.PUT_FAKE_CALL_TEL, fakeTelNumber);
			PendingIntent fakeCallPending = PendingIntent.getActivity(mContext, 0, fakeCallIntent, PendingIntent.FLAG_ONE_SHOT);
			
			Date currentDate = new Date();
			currentDate.setTime(System.currentTimeMillis() + (fakeDelayTime * 1000));
			
			AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
			alarmManager.set(AlarmManager.RTC_WAKEUP, currentDate.getTime(), fakeCallPending);
			
			mMessagePool.setFakeCallFlag(true);
		} else {
			Toast.makeText(mContext, "예약된 FakeCall 이 존재합니다.", Toast.LENGTH_SHORT).show();
		}
		
		finish();
	}
	
}
