package madcat.safereturn.service;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.fake.FakeCall;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class NotificationFakeCall {
	
//	private static final String TAG = "SafeNotification";
	
	private static NotificationFakeCall mFakeCallNotification = new NotificationFakeCall();
	
	private Context mContext;
	private NotificationManager mNotificationManager;
	private Notification mNotification;
	private Intent mIntent;
	private PendingIntent mPendingIntent;
	
	private NotificationFakeCall() {}
	
	public void setContext(Context context) {
		mContext = context;
	}
	
	public static NotificationFakeCall getInstance() {
		return mFakeCallNotification;
	}
	
	public void registNotification() {
		 mNotificationManager = (NotificationManager)mContext.getSystemService(Context.NOTIFICATION_SERVICE);
		 
		 mIntent = new Intent(mContext, FakeCall.class);
		 mPendingIntent = PendingIntent.getActivity(mContext, 0, mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK 
															   		 		    | Intent.FLAG_ACTIVITY_CLEAR_TOP 
															   				    | Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		 
		 updateNotification(mIntent);
	}
	
	public void updateNotification(Intent intent) {
		mNotification = new Notification(R.drawable.notification_icon_phone_call, "전화 왔습니다.", System.currentTimeMillis());
		mNotification.flags = Notification.FLAG_ONGOING_EVENT;
		
		mIntent = intent;
		mPendingIntent = PendingIntent.getActivity(mContext, 0, mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK 
																	 		   | Intent.FLAG_ACTIVITY_CLEAR_TOP 
															   				   | Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		
		
		mNotification.setLatestEventInfo(mContext, "통화 중", "통화 중입니다.", mPendingIntent);
		mNotificationManager.notify(Constants.APP_NUMBER, mNotification);
	}
	
	public void removeNotification() {
		if(mNotificationManager != null) {
			mNotificationManager.cancel(Constants.APP_NUMBER);
		}
	}
}
