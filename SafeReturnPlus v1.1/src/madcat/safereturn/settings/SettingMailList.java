package madcat.safereturn.settings;

import java.util.ArrayList;

import madcat.safereturn.pro.R;
import madcat.safereturn.adapter.EMailAdapter;
import madcat.safereturn.adapter.EMailAddress;
import madcat.safereturn.adapter.EMailDelAdapter;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.SettingEmailInputDialog;
import madcat.safereturn.utils.Utils;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class SettingMailList extends ListActivity implements OnClickListener {

	private final String TAG											=	"SettingMailList";
	private final boolean DEVELOPE_MODE									=	Constants.DEVELOPE_MODE;
	
	public static final int MODIFY_EMAIL								=	1;
	public static final int ADD_EMAIL									=	2;
	
	private Context mContext;
	
	private LinearLayout mDeleteMenuLayout;
	private Button mEMailDeleteBtnOk;
	private Button mEMailDeleteBtnCancel;
	
	private ArrayList<EMailAddress> mItems;
	
	private EMailAdapter mAdapter;
	private EMailDelAdapter mDelAdapter;
	
	private int mBackStateFlag;
	private int mSelectedItemIndex;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settings_mail_list);
		
		this.mContext = this;
		this.mBackStateFlag = 1;
		
		// Resource Id
		mDeleteMenuLayout = (LinearLayout)findViewById(R.id.mail_delete_menu_area);
		mEMailDeleteBtnOk = (Button)findViewById(R.id.mail_btn_delete);
		mEMailDeleteBtnCancel = (Button)findViewById(R.id.mail_btn_cancel);
		
		// Event Listener 설정
		mEMailDeleteBtnOk.setOnClickListener(this);
		mEMailDeleteBtnCancel.setOnClickListener(this);
		
		// DB 에서 메일 리스트 불러오기
		mItems = Utils.getEMailList();
		
		if(mItems.isEmpty()) {
			Toast.makeText(mContext, "긴급 메일을 전송할 상대를 등록해 주세요.", Toast.LENGTH_SHORT).show();
		}
		
		// 어댑터 설정
		mAdapter = new EMailAdapter(mContext, R.layout.settings_listview_row, mItems);
		setListAdapter(mAdapter);
		
		registerForContextMenu(getListView());
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		mSelectedItemIndex = ((AdapterView.AdapterContextMenuInfo)menuInfo).position;

		String[] items = {"메일 수정", "메일 삭제"};
		
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setItems(items, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch(which) {
				case 0:		//	메일 수정
					final SettingEmailInputDialog settingEmailDialog = new SettingEmailInputDialog(mContext, MODIFY_EMAIL);
					settingEmailDialog.setLoadName(mItems.get(mSelectedItemIndex).getName());
					settingEmailDialog.setLoadEmail(mItems.get(mSelectedItemIndex).getEMail());
					
					settingEmailDialog.show();
					
					settingEmailDialog.setOnDismissListener(new OnDismissListener() {
						public void onDismiss(DialogInterface dialog) {
							if(settingEmailDialog.getModifyFlag()) {
								refreshEmailList();
							}
						}
					});
					
					break;
				case 1:		//	메일 삭제
					AlertDialog.Builder delBuilder = new AlertDialog.Builder(mContext);
					delBuilder.setMessage("메일을 삭제하시겠습니까?");
					delBuilder.setPositiveButton(mContext.getString(R.string.dialog_btn_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Utils.deleteDBEmail(mItems.get(mSelectedItemIndex).getName(), mItems.get(mSelectedItemIndex).getEMail());
							dialog.dismiss();
							
							refreshEmailList();
						}
					});
					delBuilder.setNegativeButton(mContext.getString(R.string.dialog_btn_cancel), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
					
					delBuilder.show();
					
					break;
				}
			}
		});
		
		builder.show();
	}
	
	private void refreshEmailList() {
		mAdapter.clear();
		mItems = Utils.getEMailList();
		mAdapter = new EMailAdapter(mContext, R.layout.settings_listview_row, mItems);
		setListAdapter(mAdapter);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.mail_btn_delete:
				if(mDelAdapter.getCheckedList().size() != 0) {
					for(int i=0; i < mDelAdapter.getCheckedList().size(); i++) {
						Utils.deleteDBEmail(mDelAdapter.getCheckedList().get(i).getName(),
								mDelAdapter.getCheckedList().get(i).getEMail());
					}

					AlertDialog.Builder delBuilder = new AlertDialog.Builder(mContext);
					delBuilder.setMessage("메일을 삭제하시겠습니까?");
					delBuilder.setPositiveButton(mContext.getString(R.string.dialog_btn_ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Utils.deleteDBEmail(mItems.get(mSelectedItemIndex).getName(), mItems.get(mSelectedItemIndex).getEMail());
							dialog.dismiss();
							
							refreshEmailList();
							
							mDeleteMenuLayout.setVisibility(LinearLayout.INVISIBLE);
							mEMailDeleteBtnOk.setClickable(false);
							mEMailDeleteBtnCancel.setClickable(false);
							
							mBackStateFlag = 1;
						}
					});
					delBuilder.setNegativeButton(mContext.getString(R.string.dialog_btn_cancel), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
					
					delBuilder.show();
					
				} else {
					Toast.makeText(getApplicationContext(), "삭제할 메일을 선택해 주세요.", Toast.LENGTH_SHORT).show();
				}
				
				break;
			case R.id.mail_btn_cancel:
				
				mDeleteMenuLayout.setVisibility(LinearLayout.INVISIBLE);
				mEMailDeleteBtnOk.setClickable(false);
				mEMailDeleteBtnCancel.setClickable(false);
				
				setListAdapter(mAdapter);
				
				mBackStateFlag = 1;
				
				
				break;
		}
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean result = super.onPrepareOptionsMenu(menu);
		
		if(mBackStateFlag == 1) {
			menu.removeItem(R.id.menu_add_email);
			menu.removeItem(R.id.menu_del_email);
			
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.email_list_menu, menu);
		} else {
			result = false;
		}
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
		switch(item.getItemId()) {
			case R.id.menu_add_email:
				final SettingEmailInputDialog settingEmailDialog = new SettingEmailInputDialog(mContext, ADD_EMAIL);
				settingEmailDialog.show();
				
				settingEmailDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(settingEmailDialog.getModifyFlag()) {
							refreshEmailList();
						}
					}
				});
				
				break;
			case R.id.menu_del_email:
				if(mItems.size() != 0) {
					mBackStateFlag = 2;
					mDelAdapter = new EMailDelAdapter(mContext, R.layout.settings_listview_row_del, mItems);
					setListAdapter(mDelAdapter);
					
					mDeleteMenuLayout.setVisibility(LinearLayout.VISIBLE);
					mEMailDeleteBtnOk.setClickable(true);
					mEMailDeleteBtnCancel.setClickable(true);
				} else {
					Toast.makeText(getApplicationContext(), "메뉴 버튼을 눌러 적어도 한명 이상의 이메일을 추가해 주세요.", Toast.LENGTH_SHORT).show();
				}
				break;
			default:
				
				break;
		}
		
		return result;
	}
	
	@Override
	public void onBackPressed() {
		if(mBackStateFlag == 1) {
			super.onBackPressed();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
		}
	}
	
}
