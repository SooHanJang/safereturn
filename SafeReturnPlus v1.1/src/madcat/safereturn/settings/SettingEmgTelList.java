package madcat.safereturn.settings;

import java.util.ArrayList;

import madcat.safereturn.pro.R;
import madcat.safereturn.adapter.EmergencyAddress;
import madcat.safereturn.adapter.EmergencyAddressAdapter;
import madcat.safereturn.adapter.EmergencyAddressDelAdapter;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.AddressMgtDialog;
import madcat.safereturn.dialog.EmergencyAddressDialog;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class SettingEmgTelList extends ListActivity {
	
//	private static final String TAG = "EmergencyFriendsList";
	private static final int MODIFY_FRIENDS = 1;
	private static final int ADD_FRIENDS = 2;
	
	private ArrayList<EmergencyAddress> mOrders;
	private EmergencyAddressAdapter mAdapter;
	private EmergencyAddressDelAdapter mDelAdapter;
	
	private int mFlag;
	
	private LinearLayout mDeleteMenuLayout;
	private Button mAddressDeleteOkBtn;
	private Button mAddressDeleteCancelBtn;
	
	private Context mContext;
	
	private AddressMgtDialog mAddressMgtDialog;
	private int mSelectedItemIndex;
	
	private SharedPreferences mConfigPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settings_emg_tel_list);
		
		mConfigPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mDeleteMenuLayout = (LinearLayout)findViewById(R.id.address_delete_menu_area);
		mAddressDeleteOkBtn = (Button)findViewById(R.id.address_btn_delete);
		mAddressDeleteCancelBtn = (Button)findViewById(R.id.address_btn_cancel);
		
		mAddressDeleteOkBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mDelAdapter.getCheckedList().size() != 0) {
					if(mDelAdapter.getCheckedList().size() == mDelAdapter.getCount()) {
						Toast.makeText(mContext, "반드시 한 개 이상의 연락처가 등록되어 있어야 합니다.", Toast.LENGTH_SHORT).show();
					} else {
						for(int i=0; i < mDelAdapter.getCheckedList().size(); i++) {
							Utils.deleteDBFriends(mDelAdapter.getCheckedList().get(i).getFriendsName(),
									mDelAdapter.getCheckedList().get(i).getFriendsTel());
						}
						
						refreshFriendsList();
						
						mDeleteMenuLayout.setVisibility(LinearLayout.INVISIBLE);
						mAddressDeleteOkBtn.setClickable(false);
						mAddressDeleteCancelBtn.setClickable(false);
						
						mFlag = 1;
					}
				} else {
					Toast.makeText(getApplicationContext(), "삭제할 연락처를 선택해 주세요.", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		mAddressDeleteCancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mDeleteMenuLayout.setVisibility(LinearLayout.INVISIBLE);
				mAddressDeleteOkBtn.setClickable(false);
				mAddressDeleteCancelBtn.setClickable(false);
				
				setListAdapter(mAdapter);
				
				mFlag = 1;
			}
		});
		
		mOrders = Utils.getEmergencyFriendsList();
		
		mFlag = 1;
		mContext = this;
		
		if(mOrders.size() == 0) {
			Toast.makeText(getApplicationContext(), "메뉴 버튼을 눌러 연락처를 추가해주세요.", Toast.LENGTH_SHORT).show();
		}
		
		mAdapter = new EmergencyAddressAdapter(getApplicationContext(), R.layout.settings_listview_row, mOrders);
		setListAdapter(mAdapter);

		registerForContextMenu(getListView());
		
		mAddressMgtDialog = new AddressMgtDialog(mContext, R.style.PopupDialog);
		mAddressMgtDialog.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				switch(mAddressMgtDialog.getDialogStateFlag()) {
				case 1:
					showFriendsDialog(mContext, MODIFY_FRIENDS);
					break;
				case 2:
					if(mOrders.size() != 1) {
						Utils.deleteDBFriends(mAdapter.getItem(mSelectedItemIndex).getFriendsName(), mAdapter.getItem(mSelectedItemIndex).getFriendsTel());
						refreshFriendsList();
					} else {
						Toast.makeText(mContext, "마지막 남은 연락처는 삭제할 수 없습니다.", Toast.LENGTH_SHORT).show();
					}
					break;
				case 3:
					break;
				default:
					break;
				}
			}
		});
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		mSelectedItemIndex = ((AdapterView.AdapterContextMenuInfo)menuInfo).position;
		mAddressMgtDialog.show();
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
//		Log.d(TAG, "onPrepare 호출");
		
		boolean result = super.onPrepareOptionsMenu(menu);
		
		if(mFlag == 1) {
			menu.removeItem(R.id.menu_add_friends);
			menu.removeItem(R.id.menu_del_friends);
			
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.emergency_address_list_menu, menu);
		} else {
			result = false;
		}
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
		switch(item.getItemId()) {
			case R.id.menu_add_friends:
				showFriendsDialog(mContext, ADD_FRIENDS);
				break;
			case R.id.menu_del_friends:
				if(mOrders.size() != 0) {
					mFlag = 2;
					mDelAdapter = new EmergencyAddressDelAdapter(getApplicationContext(), R.layout.settings_listview_row_del, mOrders);
					setListAdapter(mDelAdapter);
					
					mDeleteMenuLayout.setVisibility(LinearLayout.VISIBLE);
					mAddressDeleteOkBtn.setClickable(true);
					mAddressDeleteCancelBtn.setClickable(true);
				} else {
					Toast.makeText(getApplicationContext(), "메뉴 버튼을 눌러 적어도 한명 이상의 연락처를 추가해 주세요.", Toast.LENGTH_SHORT).show();
				}
				break;
			default:
				
				break;
		}
		
		return result;
	}
	
	private void refreshFriendsList() {
		
// 이 주석 부분 삭제 금지
//		mOrders = Util.getFriendsList();
//		mAdapter.add(mOrders.get(mOrders.size()-1));
//		mAdapter.notifyDataSetChanged();
		
//		Log.d(TAG, "친구 어댑터 재호출");
		
		mAdapter.clear();
		mOrders = Utils.getEmergencyFriendsList();
		mAdapter = new EmergencyAddressAdapter(getApplicationContext(), R.layout.settings_listview_row, mOrders);
		setListAdapter(mAdapter);
		
		if(mOrders.size() == 0) {
			Toast.makeText(getApplicationContext(), "메뉴 버튼을 눌러 적어도 한명 이상의 연락처를 추가해 주세요.", Toast.LENGTH_SHORT).show();
		}
	}

	private void showFriendsDialog(Context context, int flag) {
		if(flag == ADD_FRIENDS) {
			final EmergencyAddressDialog emergencyAddressDialog = new EmergencyAddressDialog(mContext, R.style.PopupDialog, EmergencyAddressDialog.SHOW_ADD_DIALOG);
			emergencyAddressDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					if (emergencyAddressDialog.getConfirmFlag()) {
						Utils.insertDBFriends(emergencyAddressDialog.getAddressName(), emergencyAddressDialog.getAddressNumber());
						refreshFriendsList();
					}
				}
			});
			emergencyAddressDialog.show();
		} else if(flag == MODIFY_FRIENDS) {
			final EmergencyAddressDialog emergencyAddressDialog = new EmergencyAddressDialog(mContext, R.style.PopupDialog, EmergencyAddressDialog.SHOW_MOD_DIALOG);
			emergencyAddressDialog.setOriginalAddressData(mAdapter.getItem(mSelectedItemIndex).getFriendsName().toString(), mAdapter.getItem(mSelectedItemIndex).getFriendsTel().toString());
			emergencyAddressDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					if (emergencyAddressDialog.getConfirmFlag()) {
						Utils.updateDBFriends(mAdapter.getItem(mSelectedItemIndex), emergencyAddressDialog.getAddressName(), emergencyAddressDialog.getAddressNumber());
						Toast.makeText(mContext, "연락처 정보가 변경되었습니다.", Toast.LENGTH_SHORT).show();
						refreshFriendsList();
					}
				}
			});
			emergencyAddressDialog.show();
		}
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		if(mFlag == 1) {
			SharedPreferences.Editor editor = mConfigPref.edit();
			editor.putString(Constants.SETTING_EMERGENCY_TEL, mOrders.get(0).getFriendsTel());
			editor.commit();
			
			super.onBackPressed();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
		}
	}
}
