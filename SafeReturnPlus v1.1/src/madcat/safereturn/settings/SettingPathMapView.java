package madcat.safereturn.settings;

import java.util.ArrayList;
import java.util.List;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.geopoint.PathPaint;
import madcat.safereturn.geopoint.PathPoint;
import madcat.safereturn.geopoint.SavePathGeoPoint;
import madcat.safereturn.maps.DestPinOverlay;
import madcat.safereturn.maps.MyPathOverlay;
import madcat.safereturn.maps.StartPinOverlay;
import madcat.safereturn.utils.Utils;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class SettingPathMapView extends MapActivity {
	
	private final String TAG										=	"Settings_PathMapView";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;	
	
	private Context mContext;
	private MapView mMapView;
	private MapController mMapController;
	private List<Overlay> mListOverlay;
	
	private StartPinOverlay mStartPinOverlay = null;
	private DestPinOverlay mDestPinOverlay = null;
	
	private GeoPoint mStartGeoPoint, mDestGeoPoint;
	private double mStartLat, mStartLon, mDestLat, mDestLon;
	private byte[] mPathBytes;
	
	private TextView mTextDistance, mTextTime;
	private int mTimeHour, mTimeMin;
	private String mStartAddr, mDestAddr;
	
	private Paint mPaint;
	private PathPoint mP1, mP2;
	
	private ArrayList<SavePathGeoPoint> mArraySavePathGP;
	
	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    setContentView(R.layout.settings_path_view);
	    
	    mContext = this;
	    getIntenter();

	    // Layout 관련 초기화
	    mTextDistance = (TextView)findViewById(R.id.path_view_distance);
	    mTextTime = (TextView)findViewById(R.id.path_view_time);
	    
	    // Layout 관련 설정
	    float distance = Utils.distanceBetween(mStartLat, mStartLon, mDestLat, mDestLon);
	    mTextDistance.setText("이동 거리 : " + "총 " + Utils.distanceKMFormat(distance) + "Km");
	    mTextTime.setText("도착 시간 : 약 " + mTimeHour + "시 " + mTimeMin + "분");
	    
	    // Map 관련 초기화
        mMapView = (MapView)findViewById(R.id.path_map_view);
        mMapView.setSatellite(false);													// 위성으로 표시 안하도록 설정
		mMapController = mMapView.getController();										// 맵 컨트롤러를 가져온다.
	    mMapController.setZoom(17);														// 초기 맵 확대는 17정도로 설정
        mListOverlay = mMapView.getOverlays();
        
        // MapView 의 ZoomLayout 을 설정하고, 미리 생성한 맵 컨트롤러를 가져와서 설정한 레이아웃에 적용하기 위한 설정 부분
	    LinearLayout zoomLayout = (LinearLayout)findViewById(R.id.path_layout_zoom);
	    View zoomView = mMapView.getZoomControls();
        zoomLayout.addView(zoomView, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        mMapView.displayZoomControls(true);
        
        // Path 관련 초기화
        initPathPaint();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		mStartGeoPoint = new GeoPoint((int)(mStartLat*1E6), (int)(mStartLon*1E6));
		mDestGeoPoint = new GeoPoint((int)(mDestLat*1E6), (int)(mDestLon*1E6));
		
//		Log.d(TAG, "복구해야 할 Bytes : " + mPathBytes);
		
		// DB 에 저장된 ArrayList 를 복구하는 과정
		mArraySavePathGP = (ArrayList<SavePathGeoPoint>)Utils.deSerializeObject(mPathBytes);
		
//		Log.d(TAG, "mArraySavePathGP : " + mArraySavePathGP);
		
		
		// StartPin, DestPin, Path 재설정하는 과정
		registeStartPinOverlay(mStartGeoPoint);			// Map 에 출발지점 등록 및 표시
		registeDestPinOverlay(mDestGeoPoint);			// Map 에 도착지점 등록 및 표시
		restorePathOverlay(mArraySavePathGP);			// Map 에 경로 등록 및 표시
		
		mMapView.getController().animateTo(mStartGeoPoint);		// 출발지를 기준으로 지도에 표시
	}
	
	public void registeStartPinOverlay(GeoPoint startGeoPoint) {
//		Log.d(TAG, "registeStartPinOverlay 호출");

		if(mStartPinOverlay == null) {									// startPinOverlay 객체가 생성되어 있지 않다면
			mStartPinOverlay = new StartPinOverlay(mContext, Constants.PIN_PATH_VIEW_FLAG);			// startPin 을 그려주기 위한 객체를 생성해준다.
			mStartPinOverlay.setStartAddr(mStartAddr);
		} else {														// 이미 객체가 생성되어져 있다면
			Utils.removeStartPinOverlay(mMapView, mStartPinOverlay);
		}
		
		mStartPinOverlay.setGeoPoint(startGeoPoint);					// 시작 지점 좌표를 설정해주고
		mListOverlay.add(mStartPinOverlay);								// MapView 의 overlayList 에 추가한 다음
		mMapView.invalidate();											// MapView를 update 해준다.
		
//		Log.d(TAG, "registeStartPinOverlay 종료");
	}
	
	public void registeDestPinOverlay(GeoPoint destGeoPoint) {
//		Log.d(TAG, "registeDestPinOverlay 호출");

		if(mDestPinOverlay == null) {									// startPinOverlay 객체가 생성되어 있지 않다면
			mDestPinOverlay = new DestPinOverlay(mContext, Constants.PIN_PATH_VIEW_FLAG);				// startPin 을 그려주기 위한 객체를 생성해준다.
			mDestPinOverlay.setDestAddr(mDestAddr);
		} else {														// 이미 객체가 생성되어져 있다면
			Utils.removeDestPinOverlay(mMapView, mDestPinOverlay);
		}
		
		mDestPinOverlay.setGeoPoint(destGeoPoint);						// 도착 지점 좌표를 설정해주고
		mListOverlay.add(mDestPinOverlay);								// MapView 의 overlayList 에 추가한 다음
		mMapView.invalidate();											// MapView를 update 해준다.
		
//		Log.d(TAG, "registeDestPinOverlay 종료");
	}
	
	public void restorePathOverlay(ArrayList<SavePathGeoPoint> arraySavePathGP) {
		int saveGPSize = arraySavePathGP.size();
		
		for(int i=0; i < saveGPSize; i++) {
			MyPathOverlay pathOverlay = new MyPathOverlay();
	    	pathOverlay.setMyPathOverlay(arraySavePathGP.get(i), mP1, mP2, mPaint);
	    	mListOverlay.add(pathOverlay);
		}
		
		mMapView.invalidate();
	}
	
	 private void initPathPaint() {
    	mPaint = new PathPaint();
    	mP1 = new PathPoint();
    	mP2 = new PathPoint();
    	
    	mPaint.setDither(true);
		mPaint.setColor(Color.RED);
		mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		mPaint.setAntiAlias(true);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(4);
		mPaint.setAlpha(90);
    }
	
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		mStartLat = intent.getExtras().getDouble("VIEW_START_LAT");
		mStartLon = intent.getExtras().getDouble("VIEW_START_LON");
		mStartAddr = intent.getExtras().getString("VIEW_START_ADDR");
		mDestLat = intent.getExtras().getDouble("VIEW_DEST_LAT");
		mDestLon = intent.getExtras().getDouble("VIEW_DEST_LON");
		mDestAddr = intent.getExtras().getString("VIEW_DEST_ADDR");
		mPathBytes = intent.getExtras().getByteArray("VIEW_PATH_BYTES");
		mTimeHour = intent.getExtras().getInt("VIEW_TIME_HOUR");
		mTimeMin = intent.getExtras().getInt("VIEW_TIME_MIN");
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
