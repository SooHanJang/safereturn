package madcat.safereturn.settings;

import java.util.ArrayList;

import madcat.safereturn.pro.R;
import madcat.safereturn.adapter.PathAdapter;
import madcat.safereturn.adapter.PathDelAdapter;
import madcat.safereturn.adapter.PathLocation;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.PathMgtDialog;
import madcat.safereturn.utils.Utils;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class SettingPathList extends ListActivity implements OnItemLongClickListener {
	
	private final String TAG										=	"Settings_PathList";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private ArrayList<PathLocation> mOrders;
	private PathAdapter mAdapter;
	private PathDelAdapter mDelAdapter;
	
	private int mFlag;
	
	private LinearLayout mDeleteMenuLayout;
	private Button mPathDeleteOkBtn;
	private Button mPathDeleteCancelBtn;
	
	private PathMgtDialog mPathMgtDialog;
	private int mSelectedItemIndex;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.settings_path_list);
		
		mDeleteMenuLayout = (LinearLayout)findViewById(R.id.path_delete_menu_area);
		mPathDeleteOkBtn = (Button)findViewById(R.id.path_btn_delete);
		mPathDeleteCancelBtn = (Button)findViewById(R.id.path_btn_cancel);
		
		this.getListView().setOnItemLongClickListener(this);
		
		mPathDeleteOkBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mDelAdapter.getCheckedList().size() != 0) {
					for(int i=0; i < mDelAdapter.getCheckedList().size(); i++) {
						Utils.deleteDBPath(mDelAdapter.getCheckedList().get(i).getStartName(),
								mDelAdapter.getCheckedList().get(i).getDestName());
					}
					
					refreshPathList();
					
					mDeleteMenuLayout.setVisibility(LinearLayout.INVISIBLE);
					mPathDeleteOkBtn.setClickable(false);
					mPathDeleteCancelBtn.setClickable(false);
					
					mFlag = 1;
				} else {
					Toast.makeText(getApplicationContext(), "삭제할 경로를 선택해 주세요.", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		mPathDeleteCancelBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mDeleteMenuLayout.setVisibility(LinearLayout.INVISIBLE);
				mPathDeleteOkBtn.setClickable(false);
				mPathDeleteCancelBtn.setClickable(false);
				
				setListAdapter(mAdapter);
				
				mFlag = 1;
			}
		});
		
		mOrders = Utils.getPathList();
		
		mFlag = 1;
		
		if(mOrders.size() == 0) {
			Toast.makeText(getApplicationContext(), "안전 귀가를 이용하여 경로를 설정해 주세요.", Toast.LENGTH_SHORT).show();
		}
		
		mAdapter = new PathAdapter(getApplicationContext(), R.layout.settings_listview_row, mOrders, Constants.FLAG_SETTINGS_SELECT_PATH_LIST);
		setListAdapter(mAdapter);
		
//		registerForContextMenu(getListView());
		
		mPathMgtDialog = new PathMgtDialog(this, R.style.PopupDialog);
		mPathMgtDialog.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				// TODO Auto-generated method stub
				switch(mPathMgtDialog.getDialogStateFlag()) {
					case 1:
						if(DEVELOPE_MODE) {
							Log.d(TAG, "선택된 position : " + mSelectedItemIndex);
						}
						
						Intent intent = new Intent(SettingPathList.this, SettingPathMapView.class);
						intent.putExtra("VIEW_START_LAT", mAdapter.getItem(mSelectedItemIndex).getStartLat());
						intent.putExtra("VIEW_START_LON", mAdapter.getItem(mSelectedItemIndex).getStartLon());
						intent.putExtra("VIEW_START_ADDR", mAdapter.getItem(mSelectedItemIndex).getStartAddr());
						intent.putExtra("VIEW_DEST_LAT", mAdapter.getItem(mSelectedItemIndex).getDestLat());
						intent.putExtra("VIEW_DEST_LON", mAdapter.getItem(mSelectedItemIndex).getDestLon());
						intent.putExtra("VIEW_DEST_ADDR", mAdapter.getItem(mSelectedItemIndex).getDestAddr());
						intent.putExtra("VIEW_PATH_BYTES", mAdapter.getItem(mSelectedItemIndex).getPathBytes());
						intent.putExtra("VIEW_TIME_HOUR", mAdapter.getItem(mSelectedItemIndex).getDestHour());
						intent.putExtra("VIEW_TIME_MIN", mAdapter.getItem(mSelectedItemIndex).getDestMin());
						startActivity(intent);
						overridePendingTransition(R.anim.fade, R.anim.hold);
						break;
					case 2:
						Utils.deleteDBPath(mAdapter.getItem(mSelectedItemIndex).getStartName(), mAdapter.getItem(mSelectedItemIndex).getDestName());
						refreshPathList();
						break;
					default:
						break;
				}
			}
		});
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
//		Log.d(TAG, "onPrepare 호출");
		
		boolean result = super.onPrepareOptionsMenu(menu);
		
		if(mFlag == 1) {
			menu.removeItem(R.id.menu_del_path);
			
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.path_list_menu, menu);
		} else {
			result = false;
		}
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
		switch(item.getItemId()) {
			case R.id.menu_del_path:
//				Log.d(TAG, "menu_del_path 호출");
//				Log.d(TAG, "mDelOrders size : " + mOrders.size());
				
				if(mOrders.size() != 0) {
					mFlag = 2;
					mDelAdapter = new PathDelAdapter(getApplicationContext(), R.layout.settings_listview_row_del, mOrders);
					setListAdapter(mDelAdapter);
					
					mDeleteMenuLayout.setVisibility(LinearLayout.VISIBLE);
					mPathDeleteOkBtn.setClickable(true);
					mPathDeleteCancelBtn.setClickable(true);
				} else {
					Toast.makeText(getApplicationContext(), "안전 귀가를 이용하여 경로를 설정해 주세요.", Toast.LENGTH_SHORT).show();
				}
				break;
			case 3:
				break;
			default:
				break;
		}
		
		return result;
	}
	
	private void refreshPathList() {
				
//		Log.d(TAG, "경로 어댑터 재호출");
			
		mAdapter.clear();
		mOrders = Utils.getPathList();
		mAdapter = new PathAdapter(getApplicationContext(), R.layout.settings_listview_row, mOrders, Constants.FLAG_SETTINGS_SELECT_PATH_LIST);
		setListAdapter(mAdapter);
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		if(mFlag == 1) {
			super.onBackPressed();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, position + "번째 아이템이 롱 클릭 됨");
		}
		
		mSelectedItemIndex = position;
		mPathMgtDialog.show();
		
		
		return false;
	}
}
