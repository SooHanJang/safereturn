package madcat.safereturn.adapter;

import java.util.ArrayList;

import madcat.safereturn.pro.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EMailDelAdapter extends ArrayAdapter<EMailAddress> {

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private int mResId;
	
	private ArrayList<EMailAddress> mItems;
	private ArrayList<EMailAddress> mDelItems;
	private ArrayList<Integer> mListItems;
	
	
	public EMailDelAdapter(Context context, int resId, ArrayList<EMailAddress> items) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.mDelItems = new ArrayList<EMailAddress>();
		this.mListItems = new ArrayList<Integer>();
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final int checkPosition = position;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.linearLayout = (LinearLayout)convertView.findViewById(R.id.delete_row_layout);
			holder.checkBox = (CheckBox)convertView.findViewById(R.id.delete_row_check);
			holder.textName = (TextView)convertView.findViewById(R.id.delete_row_head);
			holder.textEmail = (TextView)convertView.findViewById(R.id.delete_row_body);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.textName.setText(mItems.get(position).getName());
		holder.textEmail.setText(mItems.get(position).getEMail());
		
		holder.linearLayout.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				holder.checkBox.setChecked(!holder.checkBox.isChecked());
			}
		});
		
		holder.checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == checkPosition) {
							return;
						}
					}
					mListItems.add(checkPosition);
					mDelItems.add(mItems.get(checkPosition));
				} else {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == checkPosition) {
							mListItems.remove(i);
							mDelItems.remove(mItems.get(position));
							break;
						}
					}
				}
			}
		});
		
		return convertView;
	}
	
	public ArrayList<EMailAddress> getCheckedList() {
		return mDelItems;
	}
	
	
	class ViewHolder {
		public LinearLayout linearLayout;
		public CheckBox checkBox;
		public TextView textName;
		public TextView textEmail;
	}

}












