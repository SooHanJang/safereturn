package madcat.safereturn.adapter;

public class EMailAddress {
	
	private String mName, mMail;

	public EMailAddress(String name, String mail) {
		this.mName = name;
		this.mMail = mail;
	}

	public String getName() {	return mName;	}
	public void setName(String name) {	this.mName = name;	}

	public String getEMail() {	return mMail;	}
	public void setMail(String mail) {	this.mMail = mail;	}
	

}
