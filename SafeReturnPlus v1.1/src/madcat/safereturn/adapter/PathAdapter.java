package madcat.safereturn.adapter;

import java.util.ArrayList;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.Utils;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;

public class PathAdapter extends ArrayAdapter<PathLocation> {
	
	private ArrayList<PathLocation> mItems;
	private Context mContext;
	private int mFlag;
	private int mResId;
	
	public PathAdapter(Context context, int resId, ArrayList<PathLocation> items, int flag) {
		super(context, resId, items);
		this.mContext = context;
		this.mItems = items;
		this.mFlag = flag;
		this.mResId = resId;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		
		if(v == null) {
			LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(mResId, null);
		} 
		
		PathLocation pathList = mItems.get(position);
		
		if(pathList != null) {
			TextView tt = (TextView)v.findViewById(R.id.display_row_head);			// 학교 -> 집 표시 부분
			TextView bt = (TextView)v.findViewById(R.id.display_row_body);		// 약 1시간 10분 소요 부분

			if(mFlag == Constants.FLAG_SELECT_PATH_LIST) {
				tt.setTextColor(Color.BLACK);
				bt.setTextColor(Color.BLACK);
			} 
			
			if(tt != null) {
				tt.setText(pathList.getStartName() + " ▷ " + pathList.getDestName()); 
			}
			
			if(bt != null) {
				GeoPoint startGeoPoint = new GeoPoint((int)(pathList.getStartLat()*1E6), (int)(pathList.getStartLon()*1E6));
				GeoPoint destGeoPoint = new GeoPoint((int)(pathList.getDestLat()*1E6), (int)(pathList.getDestLon()*1E6));
				
				bt.setText("약 " + Utils.distanceKMFormat(Utils.distanceBetween(startGeoPoint, destGeoPoint)) + "Km " +
						"(" + pathList.getDestHour() + "시간 " + pathList.getDestMin() + "분 소요)");
			}
		}
		
		return v;
	}
}
