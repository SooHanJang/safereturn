package madcat.safereturn.adapter;

import java.util.ArrayList;

import madcat.safereturn.pro.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EmergencyAddressDelAdapter extends ArrayAdapter<EmergencyAddress> {
	
//	private static final String TAG = "FriendsDelAdapter";
	
	private ArrayList<EmergencyAddress> mItems;
	private ArrayList<EmergencyAddress> mDelItems;
	private ArrayList<Integer> mListItems;
	private Context mContext;
	

	public EmergencyAddressDelAdapter(Context context, int textViewResourceId, ArrayList<EmergencyAddress> items) {
		super(context, textViewResourceId, items);
		this.mItems = items;
		this.mContext = context;
		this.mDelItems = new ArrayList<EmergencyAddress>();
		this.mListItems = new ArrayList<Integer>();
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		final int checkPosition = position;
		
		if(v == null) {
			LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.settings_listview_row_del, null);
		} 
		
		EmergencyAddress friendsList = mItems.get(position);
		
		if(friendsList != null) {
			LinearLayout del_layout = (LinearLayout)v.findViewById(R.id.delete_row_layout);
			TextView del_tt = (TextView)v.findViewById(R.id.delete_row_head);
			TextView del_bt = (TextView)v.findViewById(R.id.delete_row_body);
			final CheckBox del_check = (CheckBox)v.findViewById(R.id.delete_row_check);
			
			if(del_tt != null) {
				del_tt.setText(friendsList.getFriendsName());
			}
			
			if(del_bt != null) {
				del_bt.setText("Tel : " + friendsList.getFriendsTel());
			}
			
			del_layout.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					// TODO Auto-generated method stub
					del_check.setChecked(!del_check.isChecked());
				}
			});
			
			del_check.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked) {
						for(int i=0; i < mListItems.size(); i++) {
							if(mListItems.get(i) == checkPosition) {
								return;
							}
						}
//						Log.d(TAG, "checkPosition : " + checkPosition + ", position : " + position);
						mListItems.add(checkPosition);
						mDelItems.add(mItems.get(checkPosition));
//						mDelItems.add(mItems.get(position));
					} else {
						for(int i=0; i < mListItems.size(); i++) {
							if(mListItems.get(i) == checkPosition) {
								mListItems.remove(i);
								mDelItems.remove(mItems.get(position));
								break;
							}
						}
//						mDelItems.remove(mItems.get(position));
					}
				}
			});
			
			boolean reChecked = false;
			for(int i=0; i < mListItems.size(); i++) {
				if(mListItems.get(i) == checkPosition) {
					del_check.setChecked(true);
					reChecked = true;
					break;
				}
			}
			
			if(!reChecked) {
				del_check.setChecked(false);
			}
		}
		
		return v;
	}
	
	public ArrayList<EmergencyAddress> getCheckedList() {
		return mDelItems;
	}
}
