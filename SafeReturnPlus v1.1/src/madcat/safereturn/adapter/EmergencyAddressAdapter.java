package madcat.safereturn.adapter;

import java.util.ArrayList;

import madcat.safereturn.pro.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EmergencyAddressAdapter extends ArrayAdapter<EmergencyAddress> {
	
	private ArrayList<EmergencyAddress> mItems;
	private Context mContext;
	private int mResId;
	
	private boolean touchable = true;
	
	public EmergencyAddressAdapter(Context context, int textViewResourceId, ArrayList<EmergencyAddress> items) {
		super(context, textViewResourceId, items);
		this.mContext = context;
		this.mResId = textViewResourceId;
		this.mItems = items;
	}
	
	public void setTouchable(boolean touchable) {
		this.touchable = touchable;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		
		if(v == null) {
			LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(mResId, null);
		} 
		
		EmergencyAddress addressList = mItems.get(position);
		
		if(addressList != null) {
			TextView tt = (TextView)v.findViewById(R.id.display_row_head);			// Frineds Name TextView
			TextView bt = (TextView)v.findViewById(R.id.display_row_body);		// Friends Tel TextView
			
			if(tt != null) {
				tt.setText(addressList.getFriendsName());
			}
			
			if(bt != null) {
				bt.setText("Tel : " + addressList.getFriendsTel());
			}
				
		}
		
		return v;
	}
}
