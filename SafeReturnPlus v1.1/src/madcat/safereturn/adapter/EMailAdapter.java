package madcat.safereturn.adapter;

import java.util.ArrayList;

import madcat.safereturn.pro.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class EMailAdapter extends ArrayAdapter<EMailAddress> {

	private ArrayList<EMailAddress> mItems;
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private int mResId;
	
	
	public EMailAdapter(Context context, int resId, ArrayList<EMailAddress> items) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.textName = (TextView)convertView.findViewById(R.id.display_row_head);
			holder.textEmail = (TextView)convertView.findViewById(R.id.display_row_body);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.textName.setText(mItems.get(position).getName());
		holder.textEmail.setText(mItems.get(position).getEMail());
		
		return convertView;
	}
	
	
	class ViewHolder {
		public TextView textName;
		public TextView textEmail;
	}

}












