package madcat.safereturn.adapter;

public class EmergencyAddress {
	
	private String mFriendsName, mFriendsTel;
	
	public EmergencyAddress(String name, String tel) {
		mFriendsName = name;
		mFriendsTel = tel;
	}
	
	public String getFriendsName() { return mFriendsName; }
	public String getFriendsTel() { return mFriendsTel; }
}
