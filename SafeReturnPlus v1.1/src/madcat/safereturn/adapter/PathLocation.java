package madcat.safereturn.adapter;

public class PathLocation {
	
	private String mStartName, mStartAddr, mDestName, mDestAddr;
	private double mStartLat, mStartLon, mDestLat, mDestLon;
	private int mDestHour, mDestMin;
	private byte[] mPathBytes;
	
	public PathLocation(String startName, double startLat, double startLon, String startAddr, 
			String destName, double destLat, double destLon, String destAddr, int destHour, int destMin, byte[] pathBytes) {
		this.mStartName = startName;
		this.mStartLat = startLat;
		this.mStartLon = startLon;
		this.mStartAddr = startAddr;
		this.mDestName = destName;
		this.mDestLat = destLat;
		this.mDestLon = destLon;
		this.mDestAddr = destAddr;
		this.mDestHour = destHour;
		this.mDestMin = destMin;
		this.mPathBytes = pathBytes;
	}
	
	public String getStartName() { return mStartName; }
	public String getStartAddr() { return mStartAddr; }
	public double getStartLat() { return mStartLat;	}
	public double getStartLon() { return mStartLon;	}
	
	public String getDestName() { return mDestName; }
	public String getDestAddr() { return mDestAddr;	}
	public double getDestLat() { return mDestLat; }
	public double getDestLon() { return mDestLon; }
	
	public int getDestHour() { return mDestHour; }
	public int getDestMin() { return mDestMin;	}
	
	public byte[] getPathBytes() { return mPathBytes; }
}
