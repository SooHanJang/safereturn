package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.settings.SettingMailList;
import madcat.safereturn.utils.Utils;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class SettingEmailInputDialog extends Dialog implements OnClickListener {

	private final String TAG										=	"EmailDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	
	private Button mBtnOk, mBtnCancel;
	private EditText mEditName, mEditEmail;
	
	private int mState;
	
	private String mLoadName, mLoadEmail;
	
	private boolean mModifyFlag										=	false;
	

	public SettingEmailInputDialog(Context context, int flag) {
		super(context);
		this.mContext = context;
		this.mState = flag;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_email);
		setTitle(mContext.getString(R.string.dialog_email_modify_title));
		
		mBtnOk = (Button)findViewById(R.id.dialog_email_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_email_btn_cancel);
		
		mEditName = (EditText)findViewById(R.id.dialog_email_name_input);
		mEditEmail = (EditText)findViewById(R.id.dialog_email_email_input);

		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		if(mState == SettingMailList.MODIFY_EMAIL) {
			// 값을 읽어와서 화면에 미리 띄워주기
			
			mEditName.setText(mLoadName);
			mEditEmail.setText(mLoadEmail);
		}
	}

	@Override
	public void onClick(View v) {
		
		switch(v.getId()) {
			case R.id.dialog_email_btn_ok:

				String name = mEditName.getText().toString().trim();
				String email = mEditEmail.getText().toString().trim();
				
				switch(mState) {
					case SettingMailList.ADD_EMAIL:
						
						if(name.length() != 0 && email.length() != 0) {
							Utils.insertDbEmail(name, email);
							mModifyFlag = true;
							
							dismiss();
						} else {
							Toast.makeText(mContext, mContext.getString(R.string.toast_title_caution_email), Toast.LENGTH_SHORT).show();
						}
						
						break;
						
					case SettingMailList.MODIFY_EMAIL:
						
						if(name.length() != 0 && email.length() != 0) {
							Utils.updateDBEmail(mLoadName, mLoadEmail, name, email);
							mModifyFlag = true;
							dismiss();
						} else {
							Toast.makeText(mContext, mContext.getString(R.string.toast_title_caution_email), Toast.LENGTH_SHORT).show();
						}
						
						break;
				}
				
				break;
			case R.id.dialog_email_btn_cancel:
				
				dismiss();
				break;
		}
	}
	
	public void setLoadName(String name) {	this.mLoadName = name;	}
	public void setLoadEmail(String email) {	this.mLoadEmail = email;	}
	
	public boolean getModifyFlag() {	return mModifyFlag;	}
	

}
