package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class AddressModifyDialog extends Dialog {
	
//	private static final String TAG = "AddressModifyDialog";
	
	private ImageButton mBtnOk, mBtnCancel;
	private ImageView mImageTitle;
	private EditText mEditAddr;
	private int mDialogFlag = 0;
	private String mAddrText;
	private Context mContext;
 
	public AddressModifyDialog(Context context, int styleId, int dialogFlag, String addrText) {
		super(context, styleId);
		
//		Log.d(TAG, "생성자 생성");
		
		mContext = context;
		this.mAddrText = addrText;
		this.mDialogFlag = dialogFlag;
	}
	
	public String getAddrText() {
		return mAddrText;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_address_input);

		mBtnOk = (ImageButton)findViewById(R.id.dialog_address_btn_ok);
		mBtnCancel = (ImageButton)findViewById(R.id.dialog_address_btn_cancel);
		mImageTitle = (ImageView)findViewById(R.id.dialog_address_input_title);
		mEditAddr = (EditText)findViewById(R.id.dialog_address_input);
		
		mEditAddr.setText(mAddrText);		// EditText 에 주소 텍스트 설정
		
		if(mDialogFlag == Constants.DIALOG_FLAG_MOIDFY_START_ADDR) {
			mImageTitle.setBackgroundResource(R.drawable.dialog_address_input_start_title);
		} else {
			mImageTitle.setBackgroundResource(R.drawable.dialog_address_input_dest_title);
		}
	
		mBtnOk.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(mEditAddr.getText().toString().length() != 0) {
					mAddrText = mEditAddr.getText().toString();
					
//					Log.d(TAG, "입력받은 주소 값 : " + mAddrText);
					dismiss();
				} else {
					Toast.makeText(mContext, "주소를 입력해 주세요", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		mBtnCancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dismiss();
			}
		});
	}
	
	@Override
	public void onBackPressed() {}
}









