package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class MapPopUpDialog extends Dialog {
	
//	private final String TAG = "PopUpDialog";
	
	private ImageButton mBtnDest, mBtnFavorite, mBtnInput;
	private int mDialogStateflag = 0;

	public int getDialogStateFlag() { return mDialogStateflag; }

	public MapPopUpDialog(Context context, int style) {
		super(context, style);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_map_select);
		
		mBtnDest = (ImageButton)findViewById(R.id.select_dest_map);
		mBtnFavorite = (ImageButton)findViewById(R.id.select_dest_favorite);
		mBtnInput = (ImageButton)findViewById(R.id.select_dest_input);
		
		mBtnInput.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "도착지 주소 입력 클릭");
				dismiss();
				mDialogStateflag = 1;
			}
		});
		
		mBtnDest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "지도에서 도착지 선택 클릭");
				dismiss();
				mDialogStateflag = 2;
			}
		});
		
		mBtnFavorite.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "등록한 장소에서 선택 클릭");
				dismiss();
				mDialogStateflag = 3;
			}
		});
	}
}
