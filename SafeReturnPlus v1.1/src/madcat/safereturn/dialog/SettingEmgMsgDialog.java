package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingEmgMsgDialog extends Dialog {
	private Context mContext;

	private EditText mMessageField;
	private Button mBtnOk, mBtnCancel;
	
	private SharedPreferences mPref;
	
	public SettingEmgMsgDialog(Context context) {
		super(context);
		mContext = context;
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.dialog_set_message);
		setTitle(mContext.getString(R.string.dialog_emg_msg_title));
		
		mPref = mContext.getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
	
		mMessageField = (EditText)findViewById(R.id.dialog_msg_input);
		mBtnOk = (Button)findViewById(R.id.dialog_msg_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_msg_btn_cancel);
			
		mMessageField.setText(mPref.getString(Constants.SETTING_EMERGENCY_MESSAGE, Constants.DEFAULT_EMERGENCY_MESSAGE));
		
		mBtnOk.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String emergencyMessage = mMessageField.getText().toString();
				
				if (!emergencyMessage.trim().equals("")) {
					SharedPreferences.Editor prefEditor = mPref.edit();
					prefEditor.putString(Constants.SETTING_EMERGENCY_MESSAGE, emergencyMessage);
					prefEditor.commit();
					dismiss();
				} else {
					Toast.makeText(mContext, "긴급 메시지를 설정해 주세요.", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		mBtnCancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dismiss();
			}
		});
	}
	
	@Override
	public void onBackPressed() {}
}