package madcat.safereturn.dialog;

import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

public class WidgetSendSmsDialog extends Activity {
	
	private final String TAG										=	"WidgetSendSmsDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		SharedPreferences configPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		String helpMsg = configPref.getString(Constants.SETTING_EMERGENCY_MESSAGE, Constants.DEFAULT_EMERGENCY_MESSAGE);
		
		Utils.sendMMS(this, helpMsg, null);
		Toast.makeText(this, "위급 메시지를 전송하였습니다.", Toast.LENGTH_SHORT).show();
		
		finish();
	}
}
