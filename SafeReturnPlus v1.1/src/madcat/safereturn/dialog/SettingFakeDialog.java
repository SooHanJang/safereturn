package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

public class SettingFakeDialog extends Dialog implements OnClickListener, OnCheckedChangeListener, OnItemSelectedListener {
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private Spinner mSpinTimer;
	private CheckBox mCheckFake;
	private LinearLayout mLinearCheckLayout, mLinearSenderLayout;
	private Button mBtnOk, mBtnCancel;
	
	private EditText mEditName, mEditPhoneNumber;
	
	private int mFakeTime, mTempFakeTime;
	private boolean mFakeCustomFlag, mFakeTempCustomFlag;
	
	public SettingFakeDialog(Context context) {
		super(context);
		this.mContext = context;
		this.mConfigPref = mContext.getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_set_fake);
		setTitle(mContext.getString(R.string.dialog_fake_title));
		
		// Resource Id 설정
		mSpinTimer = (Spinner)findViewById(R.id.setting_fake_timer);
		mCheckFake = (CheckBox)findViewById(R.id.setting_fake_sender_check);
		mLinearCheckLayout = (LinearLayout)findViewById(R.id.setting_fake_check_layout);
		mLinearSenderLayout = (LinearLayout)findViewById(R.id.setting_fake_sender_layout);
		mBtnOk = (Button)findViewById(R.id.dialog_fake_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_fake_btn_cancel);
		
		mEditName = (EditText)findViewById(R.id.setting_fake_sender_name);
		mEditPhoneNumber = (EditText)findViewById(R.id.setting_fake_sender_number);
		
		// Event Listener 설정
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		mCheckFake.setOnCheckedChangeListener(this);
		mSpinTimer.setOnItemSelectedListener(this);
		
		initSpinner();
		initFakeCustomCheck();
		initFakeEditText();
	}
	
	private void initSpinner() {
		// Spinner 설정
		ArrayAdapter timerAdapter = ArrayAdapter.createFromResource(mContext, R.array.setting_fake_timer, android.R.layout.simple_spinner_item);
		timerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinTimer.setAdapter(timerAdapter);
		
		mFakeTime = mConfigPref.getInt(Constants.SETTING_FAKE_TIME, 5);
		mTempFakeTime = mFakeTime;
		
		mSpinTimer.setSelection(timeToIndex(mFakeTime));
	}
	
	private void initFakeCustomCheck() {
		mFakeCustomFlag = mConfigPref.getBoolean(Constants.SETTING_FAKE_CUSTOM_CHECK, false);
		mFakeTempCustomFlag = mFakeCustomFlag;
		mCheckFake.setChecked(mFakeCustomFlag);
		
		fakefCustomCheckLayout(mFakeCustomFlag);
	}
	
	private void initFakeEditText() {
		mEditName.setText(mConfigPref.getString(Constants.SETTING_FAKE_CUSTOM_NAME, ""));
		mEditPhoneNumber.setText(mConfigPref.getString(Constants.SETTING_FAKE_CUSTOM_PHONE_NUMBER, ""));
	}
	
	private void fakefCustomCheckLayout(boolean flag) {
		if(flag) {
			mLinearSenderLayout.setVisibility(View.VISIBLE);
		} else {
			mLinearSenderLayout.setVisibility(View.GONE);
		}
		
		mFakeTempCustomFlag = flag;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_fake_btn_ok:
				
				mFakeTime = mTempFakeTime;
				mFakeCustomFlag = mFakeTempCustomFlag;
				
				SharedPreferences.Editor configEditor = mConfigPref.edit();
				configEditor.putInt(Constants.SETTING_FAKE_TIME, mFakeTime);
				configEditor.putBoolean(Constants.SETTING_FAKE_CUSTOM_CHECK, mFakeCustomFlag);
				
				if(mFakeCustomFlag) {
					if(mEditName.getText().length() != 0 && mEditPhoneNumber.getText().length() != 0) {
						configEditor.putString(Constants.SETTING_FAKE_CUSTOM_NAME, mEditName.getText().toString());
						configEditor.putString(Constants.SETTING_FAKE_CUSTOM_PHONE_NUMBER, mEditPhoneNumber.getText().toString());
						configEditor.commit();
						
						dismiss();
					} else {
						Toast.makeText(mContext, "발신자 이름과 전화번호를 설정해주세요.", Toast.LENGTH_SHORT).show();
					}
				} else {
					configEditor.commit();
					dismiss();
				}
				break;
			case R.id.dialog_fake_btn_cancel:
				dismiss();
				break;
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		fakefCustomCheckLayout(isChecked);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		mTempFakeTime = indexToTime(position);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {}
	
	private int timeToIndex(int time) {
		int result = 0;
		
		switch(time) {
			case 5:
				result = 0;
				break;
			case 15:
				result = 1;
				break;
			case 30:
				result = 2;
				break;
			case 60:
				result = 3;
				break;
		}
		
		return result;
	}
	
	private int indexToTime(int index) {
		int result = 5;
		
		switch(index) {
			case 0:		//	5초
				result = 5;
				break;
			case 1:		//	15초
				result = 15;
				break;
			case 2:		//	30초
				result = 30;
				break;
			case 3:		//	1분
				result = 60;
				break;
		}
		
		return result;
	}
}





