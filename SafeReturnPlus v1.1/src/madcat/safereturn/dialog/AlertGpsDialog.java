package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class AlertGpsDialog extends Dialog {
	
	private ImageButton mBtnGPS, mBtnBack;
	private int mDialogStateFlag = 0;
	
	public int getDialogStateFlag() { return mDialogStateFlag; }

	public AlertGpsDialog(final Context context, int style) {
		super(context, style);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_alert_gps);
		
		mBtnGPS = (ImageButton)findViewById(R.id.dialog_alert_btn_gps);
		mBtnBack = (ImageButton)findViewById(R.id.dialog_alert_btn_back);
		
		mBtnGPS.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mDialogStateFlag = 1;
				dismiss();
			}
		});
		
		mBtnBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mDialogStateFlag = 2;
				dismiss();
			}
		});
		
	}
	
	@Override
	public void onBackPressed() {}
}
