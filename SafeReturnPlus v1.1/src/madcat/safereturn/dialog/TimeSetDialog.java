package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class TimeSetDialog extends Dialog implements android.view.View.OnClickListener {
	
//	private static final String TAG = "TimeSetDialog";
	
	private int mLayoutFlag = 0;
	private int mMin, mHour;
	
	private Button[] mBtnMin;
	private Button[] mBtnHour;

	public TimeSetDialog(Context context, int style, int layoutflag) {
		super(context, style);
		
		mLayoutFlag = layoutflag;
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		if(mLayoutFlag == 1) {		// 분을 설정하는 다이얼로그
			setContentView(R.layout.dialog_time_min);
			
			mBtnMin = new Button[12];
			
			for(int i=0; i < 12; i++) {
				int btnResId = context.getResources().getIdentifier("min_" + i*5, "id", context.getPackageName());
				mBtnMin[i] = (Button)findViewById(btnResId);
				mBtnMin[i].setOnClickListener(this);
			}
		} else {			// 시간을 설정하는 다이얼로그
			setContentView(R.layout.dialog_time_hour);
			
			mBtnHour = new Button[12];
			
			for(int i=0; i < 12; i++) {
				int btnResId = context.getResources().getIdentifier("hour_" + i, "id", context.getPackageName());
				mBtnHour[i] = (Button)findViewById(btnResId);
				mBtnHour[i].setOnClickListener(this);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.min_0 :
			mMin = 0;
			break;
		case R.id.min_5 :
			mMin = 5;
			break;
		case R.id.min_10 :
			mMin = 10;
			break;
		case R.id.min_15 :
			mMin = 15;
			break;
		case R.id.min_20 :
			mMin = 20;
			break;
		case R.id.min_25 :
			mMin = 25;
			break;
		case R.id.min_30 :
			mMin = 30;
			break;
		case R.id.min_35 :
			mMin = 35;
			break;
		case R.id.min_40 :
			mMin = 40;
			break;
		case R.id.min_45 :
			mMin = 45;
			break;
		case R.id.min_50 :
			mMin = 50;
			break;
		case R.id.min_55 :
			mMin = 55;
			break;
		case R.id.hour_0 :
			mHour = 0;
			break;
		case R.id.hour_1 :
			mHour = 1;
			break;
		case R.id.hour_2 :
			mHour = 2;
			break;
		case R.id.hour_3 :
			mHour = 3;
			break;
		case R.id.hour_4 :
			mHour = 4;
			break;
		case R.id.hour_5 :
			mHour = 5;
			break;
		case R.id.hour_6 :
			mHour = 6;
			break;
		case R.id.hour_7 :
			mHour = 7;
			break;
		case R.id.hour_8 :
			mHour = 8;
			break;
		case R.id.hour_9 :
			mHour = 9;
			break;
		case R.id.hour_10 :
			mHour = 10;
			break;
		case R.id.hour_11 :
			mHour = 11;
			break;
		default:
			break;
		}
		
		dismiss();
	}
	
	public int getMin() {
		if(mLayoutFlag == 1) {
			return mMin;
		}
		
		return 0;
	}
	
	public int getHour() {
		if(mLayoutFlag == 2) {
			return mHour;
		}
		
		return 0;
	}
}
