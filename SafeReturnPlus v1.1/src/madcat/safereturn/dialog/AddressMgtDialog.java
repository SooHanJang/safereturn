package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class AddressMgtDialog extends Dialog {
	
//	private final String TAG = "AddressMgtDialog";
	
	private ImageButton mBtnModify, mBtnDelete;
	private int mDialogStateflag = 0;

	public int getDialogStateFlag() { return mDialogStateflag; }

	public AddressMgtDialog(Context context, int style) {
		super(context, style);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_address_mgt);
		
		mBtnModify = (ImageButton)findViewById(R.id.address_mgt_modify_btn);
		mBtnDelete = (ImageButton)findViewById(R.id.address_mgt_delete_btn);
		
		mBtnModify.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "연락처 수정 선택");
				dismiss();
				mDialogStateflag = 1;
			}
		});
		
		mBtnDelete.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "연락처 삭제 선택");
				dismiss();
				mDialogStateflag = 2;
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		dismiss();
		mDialogStateflag = 3;
	}
}
