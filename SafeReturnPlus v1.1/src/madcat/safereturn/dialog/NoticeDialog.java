package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;

public class NoticeDialog extends Dialog implements OnCheckedChangeListener, OnClickListener {
	
	private final String TAG											=	"NoticeDialog";
	private final boolean DEVELOPE_MODE									=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private CheckBox mCheckBox;
	private SharedPreferences mPref;
	
	private ImageButton mBtnNext, mBtnPrev;
	private ImageView mImageNotice, mImagePageNumber;
	
	private int mPageCount												=	1;
	
	public NoticeDialog(Context context, int styleId) {
		super(context, styleId);
		mContext = context;
		mPref = mContext.getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_notice);
		
		mCheckBox = (CheckBox)findViewById(R.id.notice_check);
		
		mBtnPrev = (ImageButton)findViewById(R.id.notice_prev);
		mBtnNext = (ImageButton)findViewById(R.id.notice_next);
		mImageNotice = (ImageView)findViewById(R.id.notice_text);
		mImagePageNumber = (ImageView)findViewById(R.id.notice_page_number_image);
		
		mCheckBox.setOnCheckedChangeListener(this);
		mBtnPrev.setOnClickListener(this);
		mBtnNext.setOnClickListener(this);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if(isChecked) {
			SharedPreferences.Editor editor = mPref.edit();
			editor.putBoolean(Constants.APP_UPDATE_2, true);
			editor.putBoolean(Constants.APP_UPDATE_NOTICE, true);
			editor.commit();
			
			dismiss();
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.notice_next:
			if(mPageCount < 3) {
				mPageCount += 1;
			}
			
			switch(mPageCount) {
				case 1:
					mImageNotice.setBackgroundResource(R.drawable.notice_update2_first);
					mImagePageNumber.setBackgroundResource(R.drawable.notice_1st_page_number);
					mBtnPrev.setVisibility(View.INVISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 2:
					mImageNotice.setBackgroundResource(R.drawable.notice_update2_second);
					mImagePageNumber.setBackgroundResource(R.drawable.notice_2nd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 3:
					mImageNotice.setBackgroundResource(R.drawable.notice_update2_third);
					mImagePageNumber.setBackgroundResource(R.drawable.notice_3rd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.INVISIBLE);
					break;
				default:
			}
			
			
			break;
		case R.id.notice_prev:
			if(mPageCount > 0) {
				mPageCount--;
			}
			
			switch(mPageCount) {
				case 1:
					mImageNotice.setBackgroundResource(R.drawable.notice_update2_first);
					mImagePageNumber.setBackgroundResource(R.drawable.notice_1st_page_number);
					mBtnPrev.setVisibility(View.INVISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 2:
					mImageNotice.setBackgroundResource(R.drawable.notice_update2_second);
					mImagePageNumber.setBackgroundResource(R.drawable.notice_2nd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 3:
					mImageNotice.setBackgroundResource(R.drawable.notice_update2_third);
					mImagePageNumber.setBackgroundResource(R.drawable.notice_3rd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.INVISIBLE);
					break;
				default:
			}
			
			break;
		}
	}
}
