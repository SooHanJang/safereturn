package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.Utils;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class AppExitPopUpDialog extends Dialog {
	
//	private static final String TAG = "AppExitPopUpDialog";
	
	private int mStartFlag = 0;
	private int mAppFlag = 0;
	private int mStopFlag = 0;
	private int mEmergencyFlag = 0;
	private ImageButton mBtnOk, mBtnCancel, mBtnRegistePath;
	
	private MessagePool mMessagePool;
	
	public int getStopFlag() {
		return mStopFlag;
	}
	
	public int getFlag() {
		return mStartFlag;
	}
	
	public int getAppFlag() {
		return mAppFlag;
	}
	
	public int getEmergencyFlag() {
		return mEmergencyFlag;
	}
	
	public AppExitPopUpDialog(Context context, int resId, int layoutFlag) {
		super(context, resId);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		mMessagePool = (MessagePool)context.getApplicationContext();
		
		if(layoutFlag == Constants.MAP_EXIT_DIALOG) {
			setContentView(R.layout.dialog_start_exit);
			
			mBtnOk = (ImageButton)findViewById(R.id.start_exit_btn_ok);
			mBtnCancel = (ImageButton)findViewById(R.id.start_exit_btn_cancel);
			
			mBtnOk.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismiss();
					mStartFlag = 1;
				}
			});
			
			mBtnCancel.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismiss();
					mStartFlag = 2;
				}
			});
		} else if(layoutFlag == Constants.APP_EXIT_DIALOG) {
			setContentView(R.layout.dialog_app_exit);
			
			mBtnOk = (ImageButton)findViewById(R.id.app_exit_btn_ok);
			mBtnCancel = (ImageButton)findViewById(R.id.app_exit_btn_cancel);

			mBtnOk.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismiss();
					mAppFlag = 1;
				}
			});
			
			mBtnCancel.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismiss();
					mAppFlag = 2;
				}
			});
			
		} else if(layoutFlag == Constants.START_STOP_DIALOG) {
			setContentView(R.layout.dialog_start_stop);
			
			LinearLayout popupLinear = (LinearLayout)findViewById(R.id.app_exit_popup_linear);
			mBtnOk = (ImageButton)findViewById(R.id.start_stop_btn_ok);
			mBtnCancel = (ImageButton)findViewById(R.id.start_stop_btn_cancel);
			mBtnRegistePath = (ImageButton)findViewById(R.id.start_stop_registe_path);

			if(mMessagePool.getFavoriteFlag() != false) {
				popupLinear.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, Utils.dipFromPixel(context, 180)));
				mBtnRegistePath.setVisibility(View.GONE);
			}
			
			mBtnOk.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismiss();
					mStopFlag = 1;
				}
			});
			
			mBtnRegistePath.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismiss();
					mStopFlag = 2;
				}
			});
			
			mBtnCancel.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismiss();
					mStopFlag = 3;
				}
			});
		} else if(layoutFlag == Constants.EMERGENCY_EXIT_DIALOG) {
			setContentView(R.layout.dialog_emergency_mode_exit);
			
			mBtnOk = (ImageButton)findViewById(R.id.emergency_mode_exit_btn_ok);
			mBtnCancel = (ImageButton)findViewById(R.id.emergency_mode_exit_btn_cancel);
			
			mBtnOk.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismiss();
					mEmergencyFlag = 1;
				}
			});
			
			mBtnCancel.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					dismiss();
					mEmergencyFlag = 2;
				}
			});
		}
	}

	@Override
	public void onBackPressed() {}
}
