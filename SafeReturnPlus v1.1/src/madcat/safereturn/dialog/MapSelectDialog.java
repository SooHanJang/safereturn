package madcat.safereturn.dialog;

import java.util.ArrayList;

import madcat.safereturn.pro.R;
import madcat.safereturn.adapter.PathAdapter;
import madcat.safereturn.adapter.PathLocation;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.geopoint.SavePathGeoPoint;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.Utils;
import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;

public class MapSelectDialog extends Dialog {
	
//	private final String TAG										=	"MapSelectDialog";
	
	private Context mContext;
	private MessagePool mMessagePool;
	private Location mCurrentLocation;
	
	private ListView mSelectListView;
	
	private ArrayList<PathLocation> mPathLocation;
	private PathAdapter mAdapter;
	
	private GeoPoint mStartGeoPoint, mDestGeoPoint;
	private ArrayList<SavePathGeoPoint> mArraySavePathGP;
	private String mDestAddressStr = "";
	private int mDestHour = 0, mDestMin = 0;
	
	private boolean mSelected = false;
	
	public MapSelectDialog(Context context, int style, Location currentLocation) {
		// TODO Auto-generated constructor stub
		super(context, style);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_address_select);
		
		mContext = context;
		mMessagePool = (MessagePool) mContext.getApplicationContext();
		mCurrentLocation = currentLocation;
		
		mSelectListView = (ListView)findViewById(R.id.dialog_select_list);
		
		mPathLocation = Utils.getPathList();
		
//		Log.d(TAG, "mPathLocation size : " + mPathLocation.size());
		
		mAdapter = new PathAdapter(mContext, R.layout.user_listview_row, mPathLocation, Constants.FLAG_SETTINGS_SELECT_PATH_LIST);
		mSelectListView.setAdapter(mAdapter);
		
		mSelectListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				GeoPoint myGeoPoint = new GeoPoint((int)(mCurrentLocation.getLatitude()*1E6), (int)(mCurrentLocation.getLongitude()*1E6));
				mStartGeoPoint = new GeoPoint((int)(mPathLocation.get(position).getStartLat()*1E6), 
						(int)(mPathLocation.get(position).getStartLon()*1E6));

				if(Utils.distanceBetween(mStartGeoPoint, myGeoPoint) <= Constants.FAVORITE_LOCATION_RANGE) {
					mMessagePool.setFavoriteFlag(true);		// 즐겨 찾는 장소를 선택하였기에 true 로 전환
					mDestGeoPoint = new GeoPoint((int)(mPathLocation.get(position).getDestLat()*1E6), 
							(int)(mPathLocation.get(position).getDestLon()*1E6));

					// DB 에 저장된 ArrayList 를 복구하는 과정
//					Log.d(TAG, "Path Array 복구 시작");
//					Log.d(TAG, "저장된 값 : " + mPathLocation.get(position).getPathBytes());
					
					mArraySavePathGP = (ArrayList<SavePathGeoPoint>)Utils.deSerializeObject(mPathLocation.get(position).getPathBytes());
//					Log.d(TAG, "Path Array 좌표 복구 종료 (생성된 path : " + mArraySavePathGP);
					
					mDestAddressStr = mPathLocation.get(position).getDestAddr();
					mDestHour = mPathLocation.get(position).getDestHour();
					mDestMin = mPathLocation.get(position).getDestMin();
					
					mSelected = true;
					dismiss();
				} else {
					mMessagePool.setFavoriteFlag(false);
					Toast.makeText(mContext, "즐겨 찾는 시작 위치와 현재 위치가 10m 이상 차이가 나면 시작할 수 없습니다.", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		dismiss();
		mSelected = false;
	}
	
	public GeoPoint getStartGeoPoint() { return mStartGeoPoint; }
	public GeoPoint getDestGeoPoint() { return mDestGeoPoint; }
	public ArrayList<SavePathGeoPoint> getArraySavePathGP() { return mArraySavePathGP; }
	public String getDestAddress() { return mDestAddressStr; }
	public int getDestHour() { return mDestHour; }
	public int getDestMin() { return mDestMin; }
	public boolean getSelectedFlag() { return mSelected; }
}