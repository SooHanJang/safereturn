package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.DesAlgorithm;
import madcat.safereturn.utils.EmailClient;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class SettingEmailSetDialog extends Dialog implements OnClickListener, OnCheckedChangeListener{
	
	private final String TAG										=	"SettingEmailSetDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private LinearLayout mLinearInputLayout;
	private Button mBtnOk, mBtnCancel, mBtnTest;
	private EditText mEditMail, mEditPwd;
	private CheckBox mCheckEmail;
	
	private TextWatcher mWatcherMail, mWatcherPwd;
	
	private boolean mCheckFlag;
	
	private String mAuthenMail, mAuthenPwd;
	
	public SettingEmailSetDialog(Context context) {
		super(context);
		this.mContext = context;
		this.mConfigPref = mContext.getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_set_email);
		setTitle(mContext.getString(R.string.setting_bbox_mail));
		
		// Resource Id 설정
		mLinearInputLayout = (LinearLayout)findViewById(R.id.dialog_email_set_layout);
		
		mBtnOk = (Button)findViewById(R.id.dialog_email_set_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_email_set_btn_cancel);
		mBtnTest = (Button)findViewById(R.id.dialog_email_set_btn_test);
		mEditMail = (EditText)findViewById(R.id.dialog_email_set_addr);
		mEditPwd = (EditText)findViewById(R.id.dialog_email_set_pw);
		mCheckEmail = (CheckBox)findViewById(R.id.dialog_email_set_checkbox);
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		mBtnTest.setOnClickListener(this);
		mCheckEmail.setOnCheckedChangeListener(this);
		
		// TextWatcher 설정
		setMailWathcerListener();
		
		// 설정 값 로드
		mCheckFlag = mConfigPref.getBoolean(Constants.SETTING_BBOX_MAIL_ENABLE, false);
		mCheckEmail.setChecked(mCheckFlag);
		
		if(mCheckFlag && mConfigPref.getBoolean(Constants.SETTING_BBOX_MAIL_AUTHENTIC, false)) {
				
			mAuthenMail = mConfigPref.getString(Constants.SETTING_BBOX_MAIL_ADDR, "");
			mAuthenPwd = mConfigPref.getString(Constants.SETTING_BBOX_MAIL_PWD, "");
			
			try {
				DesAlgorithm.getInstance().setKey(Utils.getPhoneNumber(mContext));
				String plainPwd = DesAlgorithm.getInstance().decrypt(mAuthenPwd);
				
				mEditMail.setText(mAuthenMail);
				mEditPwd.setText(plainPwd);
				
				setMailAuthentic(true);
				setCheckLayout(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			setCheckLayout(false);
		}
				
				
	}
	
	private void setCheckLayout(boolean flag) {
		if(flag) {
			mLinearInputLayout.setVisibility(View.VISIBLE);
		} else {
			mLinearInputLayout.setVisibility(View.GONE);
		}
	}
	
	private void initUserInfo() {
		SharedPreferences.Editor editor = mConfigPref.edit();
		editor.putBoolean(Constants.SETTING_BBOX_MAIL_ENABLE, false);
		editor.putBoolean(Constants.SETTING_BBOX_MAIL_AUTHENTIC, false);
		editor.putString(Constants.SETTING_BBOX_MAIL_ADDR, "");
		editor.putString(Constants.SETTING_BBOX_MAIL_PWD, "");
		editor.commit();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_email_set_btn_ok:
				if(mCheckFlag) {

					String mailAddr = mEditMail.getText().toString().trim();
					String mailPwd = mEditPwd.getText().toString().trim();

					if(mailAddr.length() != 0 && mailPwd.length() != 0) {

						// 인증이 완료된 사용자의 메일 주소와 암호를 저장합니다.
						if(mConfigPref.getBoolean(Constants.SETTING_BBOX_MAIL_AUTHENTIC, false)) {
							
							// 사용자 암호를 변환
							try {
								DesAlgorithm.getInstance().setKey(Utils.getPhoneNumber(mContext));
								String cipherPwd = DesAlgorithm.getInstance().encrypt(mailPwd);
								
								if(DEVELOPE_MODE) {
									Log.d(TAG, "암호화한 값 : " + cipherPwd);
									Log.d(TAG, "복호화한 값 : " + DesAlgorithm.getInstance().decrypt(cipherPwd));
								}
								
								SharedPreferences.Editor editor = mConfigPref.edit();
								editor.putBoolean(Constants.SETTING_BBOX_MAIL_ENABLE, mCheckFlag);
								editor.putString(Constants.SETTING_BBOX_MAIL_ADDR, mailAddr);
								editor.putString(Constants.SETTING_BBOX_MAIL_PWD, cipherPwd);
								editor.commit();
								
								dismiss();
							} catch (Exception e) {
								e.printStackTrace();
							} 
						} else {
							Toast.makeText(mContext, mContext.getString(R.string.toast_title_email_authentic), Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(mContext, mContext.getString(R.string.toast_title_caution_email_set), Toast.LENGTH_SHORT).show();
					}
				} else {
					initUserInfo();
					
					dismiss();
				}
				
				break;
				
			case R.id.dialog_email_set_btn_cancel:
				
				// 인증이 안되었다면, 초기화한다.
				if(!mConfigPref.getBoolean(Constants.SETTING_BBOX_MAIL_AUTHENTIC, false)) {
					initUserInfo();
				}
				
				dismiss();
				
				break;
				
			case R.id.dialog_email_set_btn_test:
				
				if(mCheckFlag) {

					String mailAddr = mEditMail.getText().toString().trim();
					String mailPwd = mEditPwd.getText().toString().trim();
					
					if(DEVELOPE_MODE) {
						Log.d(TAG, "사용자 메일 주소 : " + mailAddr);
						Log.d(TAG, "사용자 메일 암호 : " + mailPwd);
						
					}

					if(mailAddr.length() != 0 && mailPwd.length() != 0) {
						
						SendEMailTestAsync sendEmailTestAsync = new SendEMailTestAsync(mailAddr, mailPwd);
						sendEmailTestAsync.execute();
						
						
					} else {
						Toast.makeText(mContext, mContext.getString(R.string.toast_title_caution_email_set), Toast.LENGTH_SHORT).show();
					}
				}
				
				break;
		}
	}
	
	class SendEMailTestAsync extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog progressDialog;
		
		String mailAddr, mailPwd;
		boolean succeedFlag = false;
		
		public SendEMailTestAsync(String mailAddr, String mailPwd) {
			this.mailAddr = mailAddr;
			this.mailPwd = mailPwd;
		}
		
		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(mContext, "", mContext.getString(R.string.progress_test_email));
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// 이메일 테스트 전송 기능
			
			try {
				EmailClient testEmail = new EmailClient(mailAddr, mailPwd);
				testEmail.sendMail(mContext.getString(R.string.etc_test_email_subject), 
						mContext.getString(R.string.etc_test_email_body), mailAddr, mailAddr);
				
				succeedFlag = true;
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "메일 전송 성공");
				}
				
			} catch(Exception e){ 
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "메일 전송 실패 : " + e.toString());
				}
				
				succeedFlag = false;
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "성공 여부 확인 : " + succeedFlag);
			}
			
			if(succeedFlag) {
				Toast.makeText(mContext, mContext.getString(R.string.toast_title_email_send_test), Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(mContext, mContext.getString(R.string.toast_title_email_send_fail), Toast.LENGTH_SHORT).show();
			}
			
			setMailAuthentic(succeedFlag);
		}
		
	}
	
	private void setMailAuthentic(boolean flag) {
		SharedPreferences.Editor editor = mConfigPref.edit();
		
		if(flag) {
			editor.putBoolean(Constants.SETTING_BBOX_MAIL_AUTHENTIC, true);
			mBtnTest.setEnabled(false);
		} else {
			editor.putBoolean(Constants.SETTING_BBOX_MAIL_AUTHENTIC, false);
			mBtnTest.setEnabled(true);
		}
		
		editor.commit();
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if(isChecked) {
			Toast.makeText(mContext, mContext.getString(R.string.toast_title_caution_email_3g), Toast.LENGTH_SHORT).show();
		}
		
		
		mCheckFlag = isChecked;
		setCheckLayout(mCheckFlag);
	}
	
	private void setMailWathcerListener() {
		mWatcherMail = new TextWatcher() {
			
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void afterTextChanged(Editable s) {
				setMailAuthentic(false);
			}
		};
		
		mWatcherPwd = new TextWatcher() {
			
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void afterTextChanged(Editable s) {
				setMailAuthentic(false);
			}
		};
		
		mEditMail.addTextChangedListener(mWatcherMail);
		mEditPwd.addTextChangedListener(mWatcherPwd);
		
	}

}

	
	
	
	
	
	
	
	