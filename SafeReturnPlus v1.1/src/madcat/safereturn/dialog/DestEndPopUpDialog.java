package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

public class DestEndPopUpDialog extends Dialog {
	
//	private final String TAG = "DestEndPopUpDialog";
	
	private ImageButton btnOk, btnRegiste;
	private int flag = 0;

	public int getFlag() {
		return flag;
	}

	public DestEndPopUpDialog(Context context, int style, boolean dialogStateFlag) {
		super(context, style);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_end_dest);
		
		btnOk = (ImageButton)findViewById(R.id.map_end_dest_btn_ok);
		btnRegiste = (ImageButton)findViewById(R.id.map_end_dest_btn_registe);
		
		if(dialogStateFlag != false) {
			btnRegiste.setVisibility(View.GONE);
		}
		
		btnOk.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "안전귀가 종료 확인");
				dismiss();
				flag = 1;
			}
		});
		
		btnRegiste.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "안전귀가 위치 등록");
				dismiss();
				flag = 2;
			}
		});
	}
	
	@Override
	public void onBackPressed() {}
}
