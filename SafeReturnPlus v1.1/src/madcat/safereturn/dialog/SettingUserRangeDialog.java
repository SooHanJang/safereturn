package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class SettingUserRangeDialog extends Dialog implements OnClickListener, OnSeekBarChangeListener{
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private SeekBar mSeekRange;
	private TextView mTextRange;
	private Button mBtnOk, mBtnCancel;
	
	private int mRangeValue, mTempRangeValue;
	
	public SettingUserRangeDialog(Context context) {
		super(context);
		this.mContext = context;
		this.mConfigPref = mContext.getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_set_user_range);
		setTitle(mContext.getString(R.string.dialog_range_title));
		
		mSeekRange = (SeekBar)findViewById(R.id.dialog_range_seekbar);
		mTextRange = (TextView)findViewById(R.id.dialog_range_text);
		mBtnOk = (Button)findViewById(R.id.dialog_range_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_range_btn_cancel);
		
		mSeekRange.setOnSeekBarChangeListener(this);
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		initSeekRange();
	}
	
	private void initSeekRange() {
		mRangeValue = mConfigPref.getInt(Constants.SETTING_RANGE_VALUE, Constants.DEFAULT_USER_RANGE);
		mTempRangeValue = mRangeValue;
		mSeekRange.setMax(4);
		mSeekRange.setProgress((mRangeValue/100)-1);
		mTextRange.setText(String.valueOf(mRangeValue) + "m");
	}
	
	private void setChangeRange() {
		mRangeValue = mTempRangeValue;
		
		SharedPreferences.Editor editor = mConfigPref.edit();
		editor.putInt(Constants.SETTING_RANGE_VALUE, mRangeValue);
		editor.commit();
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		mTempRangeValue = (progress + 1) * 100;
		mTextRange.setText(String.valueOf(mTempRangeValue) + "m");
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_range_btn_ok:
				setChangeRange();
				dismiss();
				break;
			case R.id.dialog_range_btn_cancel:
				dismiss();
				break;
		}
	}
	
	@Override
	public void onBackPressed() {}
}
