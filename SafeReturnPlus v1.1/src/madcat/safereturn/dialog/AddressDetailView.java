package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import android.app.Dialog;
import android.content.Context;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class AddressDetailView extends Dialog {
	
	private TextView mTextAddress;
	
	public AddressDetailView(Context context, int style, String addr) {
		super(context, style);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_address_detail_view);
		
		mTextAddress = (TextView)findViewById(R.id.dialog_address_detail_view);
		mTextAddress.setText(addr);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		dismiss();
		return super.onTouchEvent(event);
	}
}
