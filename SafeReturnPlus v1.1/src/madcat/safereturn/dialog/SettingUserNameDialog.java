package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingUserNameDialog extends Dialog {
	private Context mContext;
	private EditText mModifyNameField;
	private Button mBtnOk, mBtnCancel;
	
	private SharedPreferences mPref;
	
	public SettingUserNameDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_set_name);
		setTitle(mContext.getString(R.string.dialog_name_modify_title));
		
		mModifyNameField = (EditText)findViewById(R.id.dialog_name_input);
		mBtnOk = (Button)findViewById(R.id.dialog_name_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_name_btn_cancel);
		
		mPref = mContext.getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mModifyNameField.setText(mPref.getString(Constants.SETTING_USER_NAME, ""));
		
		mBtnOk.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String userName = mModifyNameField.getText().toString();
				
				if (userName.trim().equals("") || userName == null) {
					Toast.makeText(mContext, "이름을 입력해주세요.", Toast.LENGTH_SHORT).show();
				} else {
					SharedPreferences.Editor prefEditor = mPref.edit();
					prefEditor.putString(Constants.SETTING_USER_NAME, userName);		// 사용자가 설정한 반경을 ShaerdPreference 에 저장한다.
					prefEditor.commit();
					dismiss();
				}
			}
		});
		
		mBtnCancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dismiss();
			}
		});
		
	}
	
	@Override
	public void onBackPressed() {}
}
