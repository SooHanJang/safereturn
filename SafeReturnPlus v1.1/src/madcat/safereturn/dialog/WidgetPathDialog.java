package madcat.safereturn.dialog;

import java.util.ArrayList;

import madcat.safereturn.pro.R;
import madcat.safereturn.adapter.PathAdapter;
import madcat.safereturn.adapter.PathLocation;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.menu.StartReturn;
import madcat.safereturn.utils.Utils;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class WidgetPathDialog extends ListActivity implements LocationListener {
	
	private final String TAG										=	"WidgetPathDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	private final float ACTIVIT_SIZE								=	0.8f;

	private Context mContext;

	private ArrayList<PathLocation> mItems;
	private PathAdapter mAdapter;
	
	private LocationManager mLocM;
	private Location mCurrentLocation;
	private String mBestProvider;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		overridePendingTransition(R.anim.fade, R.anim.hold);
		
		super.onCreate(savedInstanceState);
		this.mContext = this;
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_widget_select_path);
		
		
		
		mItems = Utils.getPathList();
		mAdapter = new PathAdapter(mContext, R.layout.settings_listview_row, mItems, Constants.FLAG_SETTINGS_SELECT_PATH_LIST);
		
		setListAdapter(mAdapter);
		
		mLocM = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		mBestProvider = mLocM.getBestProvider(Utils.setCriteria(), true);
		
		if(mItems.isEmpty()) {
			Toast.makeText(mContext, "즐겨 찾는 장소가 등록되어 있지 않습니다. 등록 후 이용해 주세요", Toast.LENGTH_SHORT).show();
			finish();
		}
		
		if(mBestProvider != null) {
			mCurrentLocation = mLocM.getLastKnownLocation(mBestProvider);
			mLocM.requestLocationUpdates(mBestProvider, 0, 0, this);
		} else {
			Toast.makeText(mContext, "GPS와 네트워크 사용을 활성화 하신 뒤 이용해 주세요.", Toast.LENGTH_SHORT).show();
			finish();
		}
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "현재 위치 : " + mCurrentLocation);
		}
		
		if(mCurrentLocation != null) {

			float distance = Utils.distanceBetween(mItems.get(position).getStartLat(), mItems.get(position).getStartLon(),
					mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "거리 차이 : " + distance);
			}
			
			if(distance <= Constants.FAVORITE_LOCATION_RANGE) {
				Intent returnIntent = new Intent(WidgetPathDialog.this, StartReturn.class);
				returnIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK 
								    | Intent.FLAG_ACTIVITY_CLEAR_TOP 
				   				    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				returnIntent.setData(Uri.parse(Constants.CALL_BY_WIDGET_PATH));
				returnIntent.putExtra(Constants.ATTRIBUTE_MYLOCATION_START_NAME, mItems.get(position).getStartName());
				returnIntent.putExtra(Constants.ATTRIBUTE_MYLOCATION_START_LAT, mItems.get(position).getStartLat());
				returnIntent.putExtra(Constants.ATTRIBUTE_MYLOCATION_START_LON, mItems.get(position).getStartLon());
				returnIntent.putExtra(Constants.ATTRIBUTE_MYLOCATION_DEST_NAME, mItems.get(position).getDestName());
				returnIntent.putExtra(Constants.ATTRIBUTE_MYLOCATION_DEST_LAT, mItems.get(position).getDestLat());
				returnIntent.putExtra(Constants.ATTRIBUTE_MYLOCATION_DEST_LON, mItems.get(position).getDestLon());
				returnIntent.putExtra(Constants.ATTRIBUTE_MYLOCATION_DEST_HOUR, mItems.get(position).getDestHour());
				returnIntent.putExtra(Constants.ATTRIBUTE_MYLOCATION_DEST_MIN, mItems.get(position).getDestMin());
				returnIntent.putExtra(Constants.ATTRIBUTE_MYLOCATION_DEST_ADDR, mItems.get(position).getDestAddr());
				returnIntent.putExtra(Constants.ATTRIBUTE_MYLOCATION_PATH_BYTES, mItems.get(position).getPathBytes());
				
				startActivity(returnIntent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			} else {
				Toast.makeText(mContext, "즐겨 찾는 시작 위치와 현재 위치가 10m 이상 차이가 나면 시작할 수 없습니다.", Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(mContext, "현재 위치를 찾을 수 없습니다. GPS를 켜고 잠시 뒤에 이용해 주세요", Toast.LENGTH_SHORT).show();
		}
		
		finish();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		mLocM.removeUpdates(this);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
		
		finish();
	}

	@Override
	public void onLocationChanged(Location location) {
		mCurrentLocation = location;
	}

	public void onProviderDisabled(String provider) {}
	public void onProviderEnabled(String provider) {}
	public void onStatusChanged(String provider, int status, Bundle extras) {}
}
