package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.ShakeCallSensor;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

public class SettingSensorDialog extends Dialog {
	
	private final String TAG										=	"EmergencySensorDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private MessagePool mMessagePool;
	
	//SETTING DIALOG.
	private SeekBar mSensorSeekBar;
	private TextView mSensorViewer;
	private Button mBtnTest, mBtnOk, mBtnCancel;
	
	//TESTING DIALOG.
	private TextView mTestingViewer;
	private Button mBtnBack, mBtnReset;
	
	private ShakeCallSensor mShakeSensor;

	//THRESHOLD.
	private SharedPreferences mPref;
	private int mThresHold, mTempThresHold;
	
	//FALG.
	private boolean switchFlag = true;
	private static final int viewFlag = 1;
	
	public SettingSensorDialog(Context context) {
		super(context);
		mContext = context;
		mMessagePool = (MessagePool)mContext.getApplicationContext();
		
	}
		
	@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			switchingDialog();
		}
	
	private void switchingDialog() {
		if (switchFlag) {
			showSettingDialog();
		} else {
			showTestingDialog(viewFlag);
		}
		
		switchFlag = !switchFlag;
	}
	
	private void showSettingDialog() {
		setContentView(R.layout.dialog_set_sensor);
		setTitle(mContext.getString(R.string.dialog_sensor_title));
		
		mSensorSeekBar = (SeekBar)findViewById(R.id.dialog_sensor_seekbar);
		mSensorViewer = (TextView)findViewById(R.id.dialog_sensor_viewer);
		mBtnTest = (Button)findViewById(R.id.dialog_sensor_btn_test);
		mBtnOk = (Button)findViewById(R.id.dialog_sensor_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_sensor_btn_cancel);
		mPref = mContext.getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mThresHold = mPref.getInt(Constants.SETTING_USER_THRESHOLD, Constants.DEFAULT_USER_THRESHOLD);
		mTempThresHold = mThresHold;
		
		mSensorSeekBar.setMax(30);
		mSensorSeekBar.setProgress((mThresHold-1500)/100 * 2);
		mSensorViewer.setText(String.valueOf(mTempThresHold));
		
		mSensorSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			public void onStopTrackingTouch(SeekBar seekBar) {}
			public void onStartTrackingTouch(SeekBar seekBar) {}
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				mTempThresHold = (progress*50)+1500;
				mSensorViewer.setText(String.valueOf(mTempThresHold));
			}
		});
		
		mBtnTest.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mThresHold = mTempThresHold;
				SharedPreferences.Editor prefEditor = mPref.edit();
				prefEditor.putInt(Constants.SETTING_USER_THRESHOLD, mThresHold);		// 사용자가 설정한 반경을 ShaerdPreference 에 저장한다.
				prefEditor.commit();
				
				switchingDialog();
			}
		});
		
		mBtnOk.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mThresHold = mTempThresHold;
				SharedPreferences.Editor prefEditor = mPref.edit();
				prefEditor.putInt(Constants.SETTING_USER_THRESHOLD, mThresHold);		// 사용자가 설정한 반경을 ShaerdPreference 에 저장한다.
				prefEditor.commit();
				
				dismiss();
			}
		});
		
		mBtnCancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mSensorSeekBar.setProgress((mThresHold-1500)/100 * 2);
				mSensorViewer.setText(String.valueOf(mThresHold));
				
				dismiss();
			}
		});
	}
	
	public void showTestingDialog(final int flag) {
		setContentView(R.layout.dialog_set_test_sensor);
		setTitle(R.string.dialog_sensor_test_title);
		
		mTestingViewer = (TextView)findViewById(R.id.dialog_test_viewer);
		mBtnBack = (Button)findViewById(R.id.dialog_test_btn_back);
		mBtnReset = (Button)findViewById(R.id.dialog_test_btn_reset);
		
		mShakeSensor = new ShakeCallSensor(mContext);
		mShakeSensor.setTestFlag(true);		// Test 를 위해 센서를 사용하기 위해 true 로 전환
		mMessagePool.setShakeTestTextView(mTestingViewer);
		
		mTestingViewer.setText(mShakeSensor.getShakeCount() + "회 알림 성공");
		
		mShakeSensor.registerSensor();
		
		mShakeSensor.setThresHold(mThresHold);
		
		mBtnBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mShakeSensor.unregisterSensor();
				mShakeSensor.setTestFlag(false);		// Test 를 종료한다고 sensor 에 알림
				
				if(flag == viewFlag) {
					switchingDialog();
				} else {
					dismiss();
				}
			}
		});
		
		mBtnReset.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mShakeSensor.setShakeCount(0);		// Count 를 초기화
				mTestingViewer.setText(mShakeSensor.getShakeCount() + "회 알림 성공");
			}
		});
	}
	
	public void setThresHold(int thresHold) { mThresHold = thresHold; }
	
	@Override
	public void onBackPressed() {}
}