package madcat.safereturn.dialog;

import java.io.IOException;
import java.util.List;

import madcat.safereturn.pro.R;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class MapInputDialog extends Dialog {
	
//	private static final String TAG = "MapInputDialog";

	private ImageButton mBtnOk, mBtnCancel;
	private EditText mInputAddr;
	private Geocoder mGeoCoder;
	private Context mContext;
	private List<Address> mListAddress;
	private MapInputDialog mapInputDialog;
	private int mDialogStateFlag = 0;

	private double mLat, mLon;
	private String myAddr;
	
	public double getMyLat() { return mLat; }
	public double getMyLon() { return mLon; }
	public String getMyAddr() { return myAddr; }
	public int getDialogStateFlag() { return mDialogStateFlag; }
	
	public MapInputDialog(Context context, int style) {
		super(context, style);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_address_input);
		
		mContext = context;
		mapInputDialog = this;
		mGeoCoder = new Geocoder(mContext);
		
		mBtnOk = (ImageButton)findViewById(R.id.dialog_address_btn_ok);
		mBtnCancel = (ImageButton)findViewById(R.id.dialog_address_btn_cancel);
		mInputAddr = (EditText)findViewById(R.id.dialog_address_input);
		
		mBtnOk.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(mInputAddr.getText().toString().length() != 0 && mapInputDialog != null) {
					AsyncGetLocation asyncGetLocation = new AsyncGetLocation(mapInputDialog);
					asyncGetLocation.execute();
				}
			}
		});
		
		mBtnCancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dismiss();
				mDialogStateFlag = 2;
			}
		});
	}
	
	class AsyncGetLocation extends AsyncTask<Void, Void, Void> {
		MapInputDialog controlDialog;
		ProgressDialog progressDialog;
		
		public AsyncGetLocation(MapInputDialog dialog) {
			controlDialog = dialog;
		}
		
		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(mContext, null, "도착지 주소 설정 중...");
		}
		
		protected Void doInBackground(Void... params) {
			try {
				mListAddress = mGeoCoder.getFromLocationName(mInputAddr.getText().toString(), 5);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			if(mListAddress != null) {
				if(mListAddress.size() != 0) {
					mLat = mListAddress.get(0).getLatitude();
					mLon = mListAddress.get(0).getLongitude();
					myAddr = mInputAddr.getText().toString();
					mDialogStateFlag = 1;
					controlDialog.dismiss();
				} else {
					Toast.makeText(mContext, "도착지 주소를 정확히 입력해주세요.", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(mContext, "3G 나 WIFI 를 이용할 수 있는 곳으로 이동해주세요.", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	@Override
	public void onBackPressed() {}
}
