package madcat.safereturn.dialog;

import madcat.safereturn.pro.R;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class EmergencyAddressDialog extends Dialog {
	public static final int SHOW_ADD_DIALOG = 0;
	public static final int SHOW_MOD_DIALOG = 1;
	
	private ImageView mTitleImage;
	private EditText mEditName, mEditTel;
	private ImageButton mBtnOk, mBtnCancel;
	
	private Context mContext;
	
	private String mOriginAddressName, mOriginAddressNumber;
	
	private boolean mConfirmFlag = false;
	public boolean getConfirmFlag() { return mConfirmFlag; }

	public EmergencyAddressDialog(Context context, int resId, int showFlag) {
		super(context, resId);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mContext = context;
		
		setContentView(R.layout.dialog_emergency_address);
		
		mTitleImage = (ImageView)findViewById(R.id.dialog_emg_title_image);
		mEditName = (EditText)findViewById(R.id.dialog_emg_input_name);
		mEditTel = (EditText)findViewById(R.id.dialog_emg_input_tel);
		mBtnOk = (ImageButton)findViewById(R.id.dialog_emg_address_btn_ok);
		mBtnCancel = (ImageButton)findViewById(R.id.dialog_emg_address_btn_cancel);
		
		switch (showFlag) {
		case SHOW_ADD_DIALOG:
			mTitleImage.setBackgroundResource(R.drawable.dialog_add_address_title);
			mBtnOk.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					if (!mEditName.getText().toString().trim().equals("") && !mEditTel.getText().toString().trim().equals("")) {
						mConfirmFlag = true;
						dismiss();
					} else {
						if (mEditName.getText().toString().trim().equals("")) {
							Toast.makeText(mContext, "이름을 입력하세요.", Toast.LENGTH_SHORT).show();
						} else if (mEditTel.getText().toString().trim().equals("")) {
							Toast.makeText(mContext, "연락처를 입력하세요.", Toast.LENGTH_SHORT).show();
						}
					}
				}
			});
			
			mBtnCancel.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					mEditName.setText("");
					mEditTel.setText("");
					mConfirmFlag = false;
					dismiss();
				}
			});
			break;
		case SHOW_MOD_DIALOG:
			mTitleImage.setBackgroundResource(R.drawable.dialog_modify_address_title);
			mBtnOk.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					if (!mEditName.getText().toString().trim().equals("") && !mEditTel.getText().toString().trim().equals("")) {
						if (!mOriginAddressName.equals(mEditName.getText().toString()) || !mOriginAddressNumber.equals(mEditTel.getText().toString())) {
							mConfirmFlag = true;
							dismiss();
						} else {
							mConfirmFlag = false;
							dismiss();
						}
					} else {
						if (mEditName.getText().toString().trim().equals("")) {
							Toast.makeText(mContext, "이름을 입력하세요.", Toast.LENGTH_SHORT).show();
						} else if (mEditTel.getText().toString().trim().equals("")) {
							Toast.makeText(mContext, "연락처를 입력하세요.", Toast.LENGTH_SHORT).show();
						}
					}
				}
			});
			
			mBtnCancel.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					mConfirmFlag = false;
					dismiss();
				}
			});	
			break;
		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {}

	/**
	 * ModifyDialog를 호출하기 전에, 기존 주소 정보를 넣어 초기화하는 작업.
	 * 이 작업을 하지 않으면 MOD_DIALOG가 호출되지 않는다.
	 */
	public void setOriginalAddressData(String originAddressName, String originPhoneNumber) {
		mOriginAddressName = originAddressName;
		mOriginAddressNumber = originPhoneNumber;
		mEditName.setText(mOriginAddressName);
		mEditTel.setText(mOriginAddressNumber);
	}
	
	public String getAddressName() {
		return mEditName.getText().toString();
	}
	
	public String getAddressNumber() {
		return mEditTel.getText().toString();
	}
}
