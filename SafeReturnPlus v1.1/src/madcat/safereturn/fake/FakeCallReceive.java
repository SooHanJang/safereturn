package madcat.safereturn.fake;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.service.NotificationFakeCall;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

public class FakeCallReceive extends Activity implements OnClickListener {

	private final String TAG										=	"FakeCallReceive";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	
	private ImageButton mBtnCallEnd;
	private TextView mTextCallTimer, mTextCallName, mTextCallTel;
	
	private Thread mCallTimerThread;
	private boolean mCallTimerFlag									=	true;
	private int mCallTimerCount;
	
	// 화면 표시 관련 변수
	private String mCallName, mCallTel;
	
	private Handler mTimerHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			
             int min = mCallTimerCount / 60;
             int sec = mCallTimerCount % 60;
             String strTime = String.format("%02d:%02d", min, sec);
 
             mTextCallTimer.setText(strTime);
             mTextCallTimer.invalidate();
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.fake_call_receive);
		
		mContext = this;
		mCallTimerCount = 0;
		
		this.getIntenter();
		
		mBtnCallEnd = (ImageButton)findViewById(R.id.fake_receive_end_call);
		mTextCallTimer = (TextView)findViewById(R.id.fake_receive_time);
		mTextCallName = (TextView)findViewById(R.id.fake_receive_tel_name);
		mTextCallTel = (TextView)findViewById(R.id.fake_receive_tel_number);
		
		// 발신자 이름과 전화번호 설정
		if(mCallName != null) {
			mTextCallName.setText(mCallName);
		}
		
		if(mCallTel != null) {
			mTextCallTel.setText(mCallTel);
		}
		
		mBtnCallEnd.setOnClickListener(this);
		
		// Timer Thread 설정
		mCallTimerThread = new Thread(new Runnable() {
			public void run() {
				while(mCallTimerFlag) {
					mTimerHandler.sendMessage(mTimerHandler.obtainMessage());
					SystemClock.sleep(1000);
					mCallTimerCount++;
				}
			}
		});
		
		mCallTimerThread.start();
		
		NotificationFakeCall fakeCallNotification = NotificationFakeCall.getInstance();
		fakeCallNotification.setContext(mContext);
		if(fakeCallNotification != null) {
			Intent intent = new Intent(mContext, FakeCallReceive.class);
			fakeCallNotification.updateNotification(intent);
		}
	}
	
	@Override
	public void onBackPressed() {}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.fake_receive_end_call:
				mCallTimerFlag = false;
				
				Intent endCallIntent = new Intent(FakeCallReceive.this, FakeCallEnd.class);
				endCallIntent.putExtra(Constants.PUT_FAKE_CALL_NAME, mCallName);
				endCallIntent.putExtra(Constants.PUT_FAKE_CALL_TEL, mCallTel);
				startActivity(endCallIntent);
				overridePendingTransition(0, 0);
				
				finish();
			break;
		}
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mCallName = intent.getExtras().getString(Constants.PUT_FAKE_CALL_NAME);
			mCallTel = intent.getExtras().getString(Constants.PUT_FAKE_CALL_TEL);
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "받은 값 : " + mCallName + ", " + mCallTel);
			}
		}
	}
}
