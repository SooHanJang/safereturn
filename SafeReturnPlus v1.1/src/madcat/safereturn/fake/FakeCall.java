package madcat.safereturn.fake;

import java.io.IOException;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.service.NotificationFakeCall;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

public class FakeCall extends Activity implements OnClickListener {
	
	private final String TAG										=	"FakeCall";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	private final int VIBRATOR_DELAY								=	1500;
	
	private Context mContext;

	private ImageButton mBtnGetCall, mBtnEndCall;
	private TextView mTextCallName, mTextCallTel;
	
	// 벨소리 관련 변수
	private MediaPlayer mMediaPlayer;
	
	// 진동 관련 변수
	private Thread mVibratorThread;
	private boolean mVibratorFlag									=	true;
	
	// 화면 표시 관련 변수
	private String mCallName, mCallTel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.fake_call_incoming);
		
		mContext = this;
		
		this.getIntenter();
		
		// Resource Id 설정
		mTextCallName = (TextView)findViewById(R.id.fake_incoming_tel_name);
		mTextCallTel = (TextView)findViewById(R.id.fake_incoming_tel_number);
		mBtnGetCall = (ImageButton)findViewById(R.id.fake_incoming_get_call);
		mBtnEndCall = (ImageButton)findViewById(R.id.fake_incoming_end_call);
		
		// 발신자 이름과 전화번호 설정
		if(mCallName != null) {
			mTextCallName.setText(mCallName);
		}
		
		if(mCallTel != null) {
			mTextCallTel.setText(mCallTel);
		}
		
		// Event Listener 설정
		mBtnGetCall.setOnClickListener(this);
		mBtnEndCall.setOnClickListener(this);
		
		// 벨소리 설정
		Uri ringtoneUri = RingtoneManager.getActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_RINGTONE);
        String ringtonePath = null;
        
        try {
        	mMediaPlayer = new MediaPlayer();
        	
        	if(ringtoneUri != null) {
             	Cursor c = getContentResolver().query(ringtoneUri, null, null, null, null);
             	c.moveToFirst();
             	ringtonePath = c.getString(1);
             	
             	mMediaPlayer.setDataSource(ringtonePath);
             	
             	mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
    			mMediaPlayer.setLooping(true);
    			mMediaPlayer.prepare();
            } 
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(ringtoneUri != null) {
			if(mMediaPlayer != null) {
				mMediaPlayer.start();
			}
		}
		
		// 진동 설정
		mVibratorThread = new Thread(new Runnable() {
			public void run() {
				Vibrator vi = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

				while(mVibratorFlag) {
					vi.vibrate(VIBRATOR_DELAY);
					SystemClock.sleep(VIBRATOR_DELAY + 1000);
				}
			}
		});
		
		mVibratorThread.start();
		
		NotificationFakeCall fakeCallNotification = NotificationFakeCall.getInstance();
		fakeCallNotification.setContext(mContext);
		fakeCallNotification.registNotification();
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.fake_incoming_get_call:
				if(mMediaPlayer != null && mMediaPlayer.isPlaying()) {
		    		mMediaPlayer.stop();
		    	}
		    	
		    	if(mVibratorThread != null) {
		    		mVibratorFlag = false;
		    	}
		    	
		    	finish();

		    	if(DEVELOPE_MODE) {
		    		Log.d(TAG, "전달 값 : " + mCallName + ", " + mCallTel);
		    	}
		    	
				Intent receiveCallIntent = new Intent(FakeCall.this, FakeCallReceive.class);
				receiveCallIntent.putExtra(Constants.PUT_FAKE_CALL_NAME, mCallName);
				receiveCallIntent.putExtra(Constants.PUT_FAKE_CALL_TEL, mCallTel);
				startActivity(receiveCallIntent);
				overridePendingTransition(0, 0);
				break;
			case R.id.fake_incoming_end_call:
				if(mMediaPlayer != null && mMediaPlayer.isPlaying()) {
		    		mMediaPlayer.stop();
		    	}
		    	
		    	if(mVibratorThread != null) {
		    		mVibratorFlag = false;
		    	}
		    	
		    	finish();
		    	
		    	if(DEVELOPE_MODE) {
		    		Log.d(TAG, "전달 값 : " + mCallName + ", " + mCallTel);
		    	}
		    	
		    	Intent endCallIntent = new Intent(FakeCall.this, FakeCallEnd.class);
		    	endCallIntent.putExtra(Constants.PUT_FAKE_CALL_NAME, mCallName);
		    	endCallIntent.putExtra(Constants.PUT_FAKE_CALL_TEL, mCallTel);
				startActivity(endCallIntent);
				overridePendingTransition(0, 0);
				
				break;
		}
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mCallName = intent.getExtras().getString(Constants.PUT_FAKE_CALL_NAME);
			mCallTel = intent.getExtras().getString(Constants.PUT_FAKE_CALL_TEL);
		}
	}
	
	@Override
	public void onBackPressed() {}
}