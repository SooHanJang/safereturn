package madcat.safereturn.fake;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.service.NotificationFakeCall;
import madcat.safereturn.utils.MessagePool;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FakeCallEnd extends Activity {
	
	private final String TAG										=	"FakeCallEnd";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	
	private RelativeLayout mRelativeBg, mRelativeCall;
	private TextView mTextCallEnd, mTextCallName, mTextCallTel;
	private MessagePool mMessagePool;
	
	// 화면 표시 관련 변수
	private String mCallName, mCallTel;
	
	private Handler mHandlerEnd = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			
			mMessagePool.setFakeCallFlag(false);
			finish();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
			
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.fake_call_incoming);
		
		mContext = this;
		mMessagePool = (MessagePool)getApplicationContext();
		
		this.getIntenter();

		mRelativeBg = (RelativeLayout)findViewById(R.id.fake_incoming_root_bg);
		mRelativeCall = (RelativeLayout)findViewById(R.id.fake_incoming_call_layout);
		mTextCallName = (TextView)findViewById(R.id.fake_incoming_tel_name);
		mTextCallTel = (TextView)findViewById(R.id.fake_incoming_tel_number);
		mTextCallEnd = (TextView)findViewById(R.id.fake_incoming_text);
		
		mRelativeBg.setBackgroundResource(R.drawable.fake_call_end_bg);
		mRelativeCall.setVisibility(View.GONE);
		mTextCallEnd.setText("Call ended");
		mTextCallEnd.setTextColor(Color.rgb(255, 93, 115));
		
		// 발신자 이름과 전화번호 설정
		if(mCallName != null) {
			mTextCallName.setText(mCallName);
		}
		
		if(mCallTel != null) {
			mTextCallTel.setText(mCallTel);
		}
		
		Thread endThread = new Thread(new Runnable() {
			public void run() {
				SystemClock.sleep(1000);
				mHandlerEnd.sendMessage(mHandlerEnd.obtainMessage());
			}
		});
		
		endThread.start();
		
		NotificationFakeCall fakeCallNotification = NotificationFakeCall.getInstance();
		fakeCallNotification.setContext(mContext);
		if(fakeCallNotification != null) {
			fakeCallNotification.removeNotification();
		}
		
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mCallName = intent.getExtras().getString(Constants.PUT_FAKE_CALL_NAME);
			mCallTel = intent.getExtras().getString(Constants.PUT_FAKE_CALL_TEL);
		}
	}
	
	@Override
	public void onBackPressed() {}

}
