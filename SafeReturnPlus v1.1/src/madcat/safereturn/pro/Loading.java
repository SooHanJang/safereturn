package madcat.safereturn.pro;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.load.LoadDatabase;
import madcat.safereturn.menu.EmergencyMode;
import madcat.safereturn.menu.StartReturn;
import madcat.safereturn.user.UserDefault0Explain;
import madcat.safereturn.utils.Utils;
import madcat.studio.update.Update;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

public class Loading extends Activity {
	
	private final String TAG											=	"Loading";
	private final boolean DEVELOPE_MODE									=	Constants.DEVELOPE_MODE;
	
	private boolean mFlag = false;
	private AsyncDBLoading mAsyncDbLoading;
	private Context mContext;
	private SharedPreferences mPref;
	
    public void onCreate(Bundle savedInstanceState) {
    	if(DEVELOPE_MODE) {
    		Log.d(TAG, "onCreate 호출");
    	}
    	
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.loading);
        mContext = this;
        
        mPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
    	if (mPref.getBoolean(Constants.APP_FIRST, false) == false) {
			mFlag = false;
		} else {
			mFlag = true;
		}
    	
    	
		mAsyncDbLoading = new AsyncDBLoading();		
    	mAsyncDbLoading.execute();										// DB 초기화 및 메인메뉴 Activity 호출
    }
    
    @Override
    protected void onStop() {
    	super.onStop();

    	// Async 동장 죽 onStop 이 호출되면, Async 를 중단
    	if(mAsyncDbLoading != null) {
//    		Log.d(TAG, "AsyncState : " + mAsyncDbLoading.getStatus());
    		if(mAsyncDbLoading.getStatus().toString().equals("RUNNING")) {
    			mAsyncDbLoading.cancel(true);
//				Log.d(TAG, "Async 중지");
    		}
    	}
    }

    /*
     * AsyncDBLoading 클래스는 AsyncTask 를 상속 받아서, Background 로 DB 초기화를 실행하며
     * 초기화가 완료되면 자동으로 메인 메뉴 Acitvity 를 호출하는 클래스입니다.
     * 
     */
    
    private class AsyncDBLoading extends AsyncTask<Void, Void, Void> {

    	ProgressDialog progressDialog;
    	
    	@Override
    	protected void onPreExecute() {
    		if(mFlag == false) {		// 처음 실행하는 것이라면, DB 생성 할 동안 로딩 화면을 보여준다.
    			progressDialog = ProgressDialog.show(mContext, null, "데이터베이스 생성 중입니다...");
    		}
    	}

		@Override
		protected Void doInBackground(Void... params) {
//			mFlag = false;		// 테스트를 위한 코드. 정식 출시 시에는 삭제
			
			if(mFlag == false) {		// 어플을 처음 실행하는 거라면
				if (LoadDatabase.loadDataBase(getResources().getAssets())) {
					if(DEVELOPE_MODE) {
						Log.d(TAG, "DB 생성 성공");
					}
				} else {
					if(DEVELOPE_MODE) {
						Log.d(TAG, "DB 생성 실패");
					}
				}
			} else {					//	어플을 처음 실행하는 것이 아니라면
				if(!mPref.getBoolean(Constants.APP_UPDATE_1, false)) {			//	업데이트를 수행하지 않았다면
					// 업데이트를 수행
					Update update = new Update(mContext);
					update.execUpdate_1();
					
					// 업데이트가 완료 되었음을 저장
					SharedPreferences.Editor updateEditor = mPref.edit();
					updateEditor.putBoolean(Constants.APP_UPDATE_1, true);
					updateEditor.commit();
				}
			}
			
			try {
				Thread.sleep(Constants.APP_LOADDING_TIME);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			
			if(progressDialog != null && progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			
			if(mFlag == false) {
				SharedPreferences tempPref = getSharedPreferences(Constants.TEMP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
				SharedPreferences.Editor tempPrefEditor = tempPref.edit();
				tempPrefEditor.putString("TEMP_NAME", "");
				tempPrefEditor.putInt("TEMP_THRESHOLD", Constants.DEFAULT_USER_THRESHOLD);
				tempPrefEditor.putString("TEMP_EMG_MSG", Constants.DEFAULT_EMERGENCY_MESSAGE);
				tempPrefEditor.commit();
				
				Intent intent = new Intent(Loading.this, UserDefault0Explain.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
		    	finish();
			} else {
				Intent intent = new Intent(Loading.this, MainMenu.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
		    	finish();
			}
		}
    }
    
    @Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}

    @Override
    public void onBackPressed() {}
}