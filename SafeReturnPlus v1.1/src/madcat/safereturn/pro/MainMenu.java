package madcat.safereturn.pro;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.AppExitPopUpDialog;
import madcat.safereturn.dialog.NoticeDialog;
import madcat.safereturn.menu.Settings;
import madcat.safereturn.menu.EmergencyMode;
import madcat.safereturn.menu.Help;
import madcat.safereturn.menu.StartReturn;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Toast;
public class MainMenu extends Activity {
	
	private final String TAG										=	"MainMenu";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private ImageButton mBtnStartReturn, mBtnSettings, mBtnHelp, mBtnEmergencyMode;
	private Context mContext;
	private SharedPreferences mPref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main_menu);

		// 기본 설정
		mContext = this;
		mPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		// Button 객체 생성 및 초기화
		mBtnStartReturn = (ImageButton)findViewById(R.id.menu_btn_start);				// 안전 귀가 메뉴 버튼
		mBtnSettings = (ImageButton)findViewById(R.id.menu_btn_settings);				// 환경 설정 메뉴 버튼
		mBtnHelp = (ImageButton)findViewById(R.id.menu_btn_help);						// 도움말 메뉴 버튼
		mBtnEmergencyMode = (ImageButton)findViewById(R.id.menu_btn_emergency_mode);	// 빠른 안전 모드 버튼
		
		onSetClickListener();		// Button 이벤트 처리를 설정하는 메소드
		
		// 업데이트 갱신
		if(!mPref.getBoolean(Constants.APP_UPDATE_2, false)) {
			SharedPreferences.Editor editor = mPref.edit();
			editor.putBoolean(Constants.APP_UPDATE_NOTICE, false);
			editor.commit();
		}
		
		// 공지사항 출력
		if(!mPref.getBoolean(Constants.APP_UPDATE_NOTICE, false)) {
			NoticeDialog noticeDialog = new NoticeDialog(mContext, R.style.PopupDialog);
			noticeDialog.show();
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onStart 호출");
		}
		
	}

	private void onSetClickListener() {
		mBtnStartReturn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(Utils.getEmergencyFriendsList().size() != 0) {
					Intent intent = new Intent(MainMenu.this, StartReturn.class);
					startActivity(intent);
			    	overridePendingTransition(R.anim.fade, R.anim.hold);
				} else {
					Toast.makeText(mContext, "안전 귀가를 시작하기 위해서는 적어도 한명 이상의 긴급 메시지를 받을 수신자를 추가해주세요.", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		mBtnSettings.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainMenu.this, Settings.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
		
		mBtnHelp.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainMenu.this, Help.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
		
		mBtnEmergencyMode.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainMenu.this, EmergencyMode.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		final AppExitPopUpDialog appExitPopUpDialog = new AppExitPopUpDialog(mContext, R.style.PopupDialog, Constants.APP_EXIT_DIALOG);
		appExitPopUpDialog.show();
		
		appExitPopUpDialog.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				if(appExitPopUpDialog.getAppFlag() == 1) {
					finish();
					android.os.Process.killProcess(android.os.Process.myPid());
				} else if(appExitPopUpDialog.getAppFlag() == 2) {
					// Dialog Cancel
				}
				
			}
		});
		
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
