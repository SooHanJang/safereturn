package madcat.safereturn.blackbox;

import madcat.safereturn.constants.Constants;
import madcat.safereturn.service.NotificationSafeReturn;
import madcat.safereturn.utils.MessagePool;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

public class BCtrlService extends Service {
	
	private final String TAG										=	"BCtrlService";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;

	public final static int RECORDING_STOP							=	0;
	public final static int RECORDING_START							=	1;

	private Context mContext;
	private MessagePool mMessagePool;
	private SharedPreferences mConfigPref;
	
	private boolean mRecordingFlag									=	false;
	
	private Thread mCountThread;
	private int mCount;
	
	private Thread mEmergencyCheckThread;
	private int mSendCheckCount										=	1;
	
	private String mFileDirPath;
	
	private Intent mPreviewIntent;
	
	
	private Handler mCountHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			mMessagePool.setBCurrentCount(mCount);
			startBService(mFileDirPath, mCount);
		}
	};
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}

		this.mContext = this;
		this.mMessagePool = (MessagePool)getApplicationContext();
		this.mConfigPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);

		if(intent != null) {
			mFileDirPath = intent.getExtras().getString("FILE_DIR_PATH");
			mMessagePool.setBCurrentFilePath(mFileDirPath);
		}
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onStartCommand 호출 (fileDirPaht : " + mFileDirPath + ")");
		}
		
		startCountThread();
		
		return START_NOT_STICKY;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onDestroy 호출");
		}

		stopBService();
		
		mRecordingFlag = false;
		mCount = 0;
		
	}
	
	private void startCountThread() {
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "startCountThread 호출");
		}
		
		mRecordingFlag = true;
		
		// 블랙 박스 쓰레드 설정
		mCountThread = new Thread(new Runnable() {
			public void run() {
				Looper.prepare();
				
				while(mRecordingFlag) {
					mCount += 1;
					mCountHandler.sendMessage(mCountHandler.obtainMessage());
					
					if(DEVELOPE_MODE) {
						Log.d(TAG, "설정된 타임 컷 시간 : " + mConfigPref.getInt(Constants.SETTING_BBOX_TIME_CUT, 20));
					}
					
					SystemClock.sleep(mConfigPref.getInt(Constants.SETTING_BBOX_TIME_CUT, 20) * 60 * 1000);
					
					stopBService();
					SystemClock.sleep(1 * 1000);
					
				}
				
				stopCountThread();
				
				Looper.loop();
			}
		});
		
		mCountThread.start();
		
		
		// 저장된 블랙 박스 파일 메일 전송 설정
		if(DEVELOPE_MODE) {
			Log.d(TAG, "메일 전송 유무 상태 : " + mConfigPref.getBoolean(Constants.SETTING_BBOX_MAIL_ENABLE, false));
		}
		
		if(mConfigPref.getBoolean(Constants.SETTING_BBOX_MAIL_ENABLE, false)) {
			mEmergencyCheckThread = new Thread(new Runnable() {
				public void run() {
					Looper.prepare();
					
					while(true) {
						if(DEVELOPE_MODE) {
							Log.d(TAG, "현재 위급 상황 상태 : " + mMessagePool.getEmergencyStateFlag());
						}
						
						
						if(mMessagePool.getEmergencyStateFlag()) {
							if(DEVELOPE_MODE) {
								Log.d(TAG, "현재 저장하는 파일 카운트 : " + mCount + ", 전송할 파일 카운트 : " + mSendCheckCount);
							}
							
							if(mCount != mSendCheckCount) {
								
								if(DEVELOPE_MODE) {
									Log.d(TAG, mSendCheckCount + "파일을 전송합니다.");
								}
								
								// 메일 서비스 호출 (메일이 등록되어 있고, 인증이 되었다면 작동)
								if(DEVELOPE_MODE) {
									Log.d(TAG, "현재 메일로 보내기 체크 상태 : " + mConfigPref.getBoolean(Constants.SETTING_BBOX_MAIL_ENABLE, false));
									Log.d(TAG, "현재 메일 인증 상태 : " + mConfigPref.getBoolean(Constants.SETTING_BBOX_MAIL_AUTHENTIC, false));
								}
								
								if(mConfigPref.getBoolean(Constants.SETTING_BBOX_MAIL_ENABLE, false) &&
										mConfigPref.getBoolean(Constants.SETTING_BBOX_MAIL_AUTHENTIC, false)) {
								
									Intent sendBBoxToEmailIntent = new Intent(BCtrlService.this, BBoxSendEmail.class);
									sendBBoxToEmailIntent.putExtra(Constants.PUT_SEND_BBOX_COUNT, mSendCheckCount);
									startService(sendBBoxToEmailIntent);
									
									mSendCheckCount = mCount;
								}
							}
						}
						
						SystemClock.sleep(Constants.SEND_CHECK_DURATION_TIME * 1000);
					}
				}
			});
			
			
			mEmergencyCheckThread.start();
		}
		
		startForeground(Constants.APP_NUMBER, NotificationSafeReturn.getInstance().getNotification());
	}
	
	private void stopCountThread() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "stopCountThread 호출");
		}

		NotificationSafeReturn.getInstance().removeNotification();
		
		stopForeground(true);
	}
	
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	private void startBService(String filePath, int count) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "블랙박스 시작");
		}
		
		mPreviewIntent = new Intent(BCtrlService.this, BPreviewActivity.class);
		mPreviewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		mPreviewIntent.putExtra("filePath", filePath);
		mPreviewIntent.putExtra("count", count);
		mPreviewIntent.putExtra("flag", RECORDING_START);
		startActivity(mPreviewIntent);
		
		Toast.makeText(mContext, "블랙 박스를 시작합니다.", Toast.LENGTH_SHORT).show();
	}
	
	private void stopBService() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "블랙 박스 중지");
		}
		
		
		mPreviewIntent = new Intent(BCtrlService.this, BPreviewActivity.class);
		mPreviewIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		mPreviewIntent.putExtra("flag", RECORDING_STOP);
		startActivity(mPreviewIntent);
		
		Toast.makeText(mContext, "블랙 박스를 중지합니다.", Toast.LENGTH_SHORT).show();
	}

}
