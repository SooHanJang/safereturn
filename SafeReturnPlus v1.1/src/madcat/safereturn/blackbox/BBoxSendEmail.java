package madcat.safereturn.blackbox;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import madcat.safereturn.constants.Constants;
import madcat.safereturn.service.WifiLockService;
import madcat.safereturn.utils.DesAlgorithm;
import madcat.safereturn.utils.EmailClient;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

public class BBoxSendEmail extends Service {
	
	private final String TAG										=	"ServiceSendBBoxToEmail";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private MessagePool mMessagePool;
	private SharedPreferences mConfigPref;
	private int mCount;

	@Override
	public void onCreate() {
		super.onCreate();
		this.mContext = this;
		this.mMessagePool = (MessagePool)getApplicationContext();
		this.mConfigPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		// WifiLock 설정
		WifiLockService.registeWifiLock(mContext);
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onStartCommand 호출");
		}

		if(intent != null) {
			mCount = intent.getExtras().getInt(Constants.PUT_SEND_BBOX_COUNT);
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "메일로 전송할 카운트 : " + mCount);
			}
		}

		SendMailAsync sendMailAsync = new SendMailAsync(mCount);
		sendMailAsync.execute();
		
		return START_NOT_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		// WifiLock 해제
		WifiLockService.releaseWifiLock();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onDestroy 호출");
		}
	}
	
	class SendMailAsync extends AsyncTask<Void, Void, Void> {
		
		int mCount;
		
		public SendMailAsync(int count) {
			this.mCount = count;
		}
		
		@Override
		protected void onPreExecute() {
			if(DEVELOPE_MODE) {
				Log.d(TAG, mCount + " 번째 블랙박스 파일 전송 시작");
			}
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			sendEmail(mCount);
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, mCount + " 번째 블랙박스 파일 전송 완료");
			}
		}
	}
	
	private void sendEmail(int count) {
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "sendEmail 호출");
		}
		
    	try {

    		SharedPreferences mPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
    		String emergencyMsg = mPref.getString(Constants.SETTING_EMERGENCY_MESSAGE, Constants.DEFAULT_EMERGENCY_MESSAGE);
    		String emergencyLoc = "위치 확인 : http://maps.google.co.kr/maps?q=" 
										+ mMessagePool.getCurrentLocation().getLatitude() + "+" 
										+ mMessagePool.getCurrentLocation().getLongitude();
    		
    		
    		String filePath = mMessagePool.getBCurrentFilePath();
    		String totalPath = filePath + "/" + Constants.BBOX_FILE_NAME + count + "." + Constants.BBOX_FILE_EXTENSION;
    		
    		String totalFileName = Constants.BBOX_FILE_NAME + count + "." + Constants.BBOX_FILE_EXTENSION;
    		
    		if(DEVELOPE_MODE) {
    			Log.d(TAG, "msg : " + emergencyMsg + " , loc : " + emergencyLoc + ", totalPath : " + totalPath + ", ' + totalFileName : " + totalFileName);
    		}
    		
    		DesAlgorithm.getInstance().setKey(Utils.getPhoneNumber(mContext));
	    	EmailClient email = new EmailClient(mConfigPref.getString(Constants.SETTING_BBOX_MAIL_ADDR, ""), 
	    			DesAlgorithm.getInstance().decrypt(mConfigPref.getString(Constants.SETTING_BBOX_MAIL_PWD, "")));
	    	
	    	// 등록된 사용자 메일을 불러와서 객체를 생성한다.
	    	InternetAddress[] receiveAddress = Utils.getMailList(mContext);
	    	
	    	if(receiveAddress != null) {
				if(DEVELOPE_MODE) {
					Log.d(TAG, "메일 전송 준비 완료");
					
					for(int i=0; i < receiveAddress.length; i++) {
						Log.d(TAG, "보낼 주소 : " + receiveAddress[i].getAddress());
					}
				}
		    	
		    	// 기본적인 사항들을 설정한 뒤, 메일을 전송한다. 
		    	email.sendMultipleMailWithFile(emergencyMsg, emergencyLoc, mConfigPref.getString(Constants.SETTING_BBOX_MAIL_ADDR, ""), receiveAddress, 
		    			filePath + "/" + Constants.BBOX_FILE_NAME + count + "." + Constants.BBOX_FILE_EXTENSION, 
		    			Constants.BBOX_FILE_NAME + count + "." + Constants.BBOX_FILE_EXTENSION);
				
		    	
		    	if(DEVELOPE_MODE) {
		    		Log.d(TAG, "메일 전송 완료");
		    	}
	    	}
    	} catch(Exception e) {
    		Log.d(TAG, e.toString() + "");
    		Log.d(TAG, e.getMessage() + "");
    	}
    }

}
