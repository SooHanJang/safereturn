package madcat.safereturn.blackbox;

import madcat.safereturn.constants.Constants;
import madcat.safereturn.service.WakeLockService;
import madcat.safereturn.utils.MessagePool;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.IBinder;
import android.util.Log;
import android.view.SurfaceHolder;

public class BRecordService extends Service {

	private final String TAG										=	"RecordService";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	
	private SurfaceHolder mSurfaceHolder;
	private MediaRecorder mMediaRecorder;
	
	private int mCount;
	private String mFilePath;
	
	@Override
	public void onCreate() {
		super.onCreate();
		this.mContext = this;
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}
		
		WakeLockService.getInstance().registeWakeLock(mContext);
		
		mSurfaceHolder = BPreviewActivity.mSurfaceView.getHolder();
		
		
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onStartCommand 호출");
		}
		
		mCount = intent.getExtras().getInt("count");
		mFilePath = intent.getExtras().getString("filePath");
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "받은 카운트 : " + mCount + ", filePath : " + mFilePath);
		}
		
		startRecorder(mCount, mFilePath);
		
		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {

		WakeLockService.getInstance().releaseWakeLock();
		
		releaseRecorder();
		
		super.onDestroy();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onDestroy 호출");
		}
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	private void startRecorder(int count, String filePath) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "startRecorder 호출");
		}
		
    	if(mMediaRecorder == null) {
			mMediaRecorder = new MediaRecorder();
		}

    	mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
    	mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
    	
//    	mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
    	
		mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H263);
    	mMediaRecorder.setVideoFrameRate(30);
    	
    	mMediaRecorder.setPreviewDisplay(mSurfaceHolder.getSurface());
    	mMediaRecorder.setOutputFile(filePath + "/" + Constants.BBOX_FILE_NAME + count + "." + Constants.BBOX_FILE_EXTENSION);

    	try {
    		mMediaRecorder.prepare();
    		mMediaRecorder.start();
		} catch (Exception e) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "Failed to prepare and start video recording", e);
			}
				
				
			mMediaRecorder.release();
			mMediaRecorder = null;
		}
    }
	
	private void releaseRecorder() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "releaseRecorder 호출");
		}
		
    	if(mMediaRecorder == null) {
			return;
		}
		
		// Video 종료 및 초기화
    	mMediaRecorder.stop();
    	mMediaRecorder.reset();
    	mMediaRecorder.release();
		
    	mMediaRecorder = null;
    }
}
