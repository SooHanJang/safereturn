package madcat.safereturn.blackbox;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.LinearLayout;

public class BPreviewActivity extends Activity {
	
	private final String TAG										=	"PreviewActivity";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private LinearLayout mLinearPrivew;
	private int mFlag												=	0;
	
	public static PreviewSurface mSurfaceView;
	public static SurfaceHolder mSurfaceHolder;
	
	private int mCount;
	private String mFilePath;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bb_preview);
		this.getIntenter();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}
		
		
		
		switch(mFlag) {
			case BCtrlService.RECORDING_STOP:
				Intent stopService = new Intent(BPreviewActivity.this, BRecordService.class);
				stopService(stopService);
				
				finish();
				break;
			case BCtrlService.RECORDING_START:
				mSurfaceView = new PreviewSurface(this);
				mLinearPrivew = (LinearLayout)findViewById(R.id.brecord_preview);
				mLinearPrivew.addView(mSurfaceView);
				
				break;
		}
	}
	
	private void getIntenter() {
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "getIntenter 호출");
		}
			
		
		Intent intent = getIntent();
		
		if(intent != null) {
			mFlag = intent.getExtras().getInt("flag");
			
			if(mFlag == BCtrlService.RECORDING_START) {
				mCount = intent.getExtras().getInt("count");
				mFilePath = intent.getExtras().getString("filePath");
			}
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "전달 받은 플래그 : " + mFlag + ", mCount : " + mCount + ", mFilePath : " + mFilePath);
			}
		}
	}

	class PreviewSurface extends SurfaceView implements SurfaceHolder.Callback {
		
		private final String TAG										=	"PreviewSurface";
		
		private SurfaceHolder mHolder;
		
		public PreviewSurface(Context context) {
			super(context);

			if(DEVELOPE_MODE) {
				Log.d(TAG, "PreviewSurface 생성자 호출");
			}
			
			mHolder = getHolder();
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
			mHolder.addCallback(this);
			
			mSurfaceHolder = mHolder;
			
		}

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "surfaceCreated 호출");
				Log.d(TAG, "레코드 서비스 호출");
			}
			
			if(mFlag == BCtrlService.RECORDING_START) {
				
				Intent recordService = new Intent(BPreviewActivity.this, BRecordService.class);
				recordService.putExtra("count", mCount);
				recordService.putExtra("filePath", mFilePath);
				startService(recordService);
				
				finish();
			}
		}
		
		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "surfaceChagned 호출");
			}
		}
		
		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "surfaceDestoryed 호출");
			}
		}
	}
}
