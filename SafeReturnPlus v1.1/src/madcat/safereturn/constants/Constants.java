package madcat.safereturn.constants;

import android.hardware.SensorManager;
import android.location.LocationManager;

public class Constants {

	public static final float DEFAULT_HDIP_DENSITY_SCALE									=	1.5f;
	
	public static final boolean DEVELOPE_MODE												=	false;
	
	public static final int APP_LOADDING_TIME												=	1000;
	
	public static final String APP_PREFERENCE_NAME											=	"APP_PREPERENCE";
	public static final String APP_FIRST													=	"FIRST";
	public static final String APP_NAME														=	"SafeReturn";
	public static final int APP_NUMBER														=	3211;
	
	public static final String APP_UPDATE_1													=	"UPDATE_2011_12_08";
	public static final String APP_UPDATE_2													=	"UPDATE_2012-04-19";
	
	public static final String APP_UPDATE_NOTICE											=	"UPDATE_NOTICE";
	
	public static final String TEMP_PREFERENCE_NAME											=	"TEMP_PREFERENCE";
	
	public static final String ROOT_DIR														=	"/data/data/madcat.safereturn.pro/databases/";
	public static final String FILE_NAME													=	"safereturnDB.db";

	public static final String DATABASE_NAME												=	"safereturnDB.db";
	public static final int DATABASE_VERSION												=	1;
	public static final int DATABASE_FILE_INDEX												=	4;
	
	public static final String SETTING_USER_NAME											=	"USER_NAME";
	public static final String SETTING_USER_THRESHOLD										=	"USER_THRESHOLD";
	public static final String SETTING_EMERGENCY_MESSAGE									=	"EMERGENCY_MESSAGE";
	public static final String SETTING_EMERGENCY_TEL										=	"EMERGENCY_TEL";
	public static final String SETTING_RANGE_VALUE											=	"RANGE_VALUE";
	
	public static final String SETTING_FAKE_TIME											=	"FAKE_TIME";
	public static final String SETTING_FAKE_CUSTOM_CHECK									=	"FAKE_CUSTOM_CHECK";
	public static final String SETTING_FAKE_CUSTOM_NAME										=	"FAKE_CUSTOM_NAME";
	public static final String SETTING_FAKE_CUSTOM_PHONE_NUMBER								=	"FAKE_CUSTOM_PHONE_NUMBER";
	
	public static final String SETTING_CCTV_OVERLAY_STATE									=	"CCTV_OVERLAY_STATE";
	public static final String SETTING_POLICE_OVERLAY_STATE									=	"POLICE_OVERLAY_STATE";
	public static final String SETTING_STORE_OVERLAY_STATE									=	"STORE_OVERLAY_STATE";
	public static final String SETTING_FIRE_OVERLAY_STATE									=	"FIRE_OVERLAY_STATE";
	
	public static final String SETTING_BBOX_ENABLE											=	"BBOX_ENABLE";
	public static final String SETTING_BBOX_MAIL_ENABLE										=	"BBOX_MAIL_ENABLE";
	public static final String SETTING_BBOX_MAIL_ADDR										=	"BBOX_MAIL_ADDR";
	public static final String SETTING_BBOX_MAIL_PWD										=	"BBOX_MAIL_PWD";
	public static final String SETTING_BBOX_MAIL_AUTHENTIC									=	"BBOX_MAIL_AUTHENTIC";
	public static final String SETTING_BBOX_TIME_CUT										=	"BBOX_TIME_CUT";
	public static final String SETTING_BBOX_MAIL											=	"BBOX_MAIL";
	
	public static final int DEFAULT_USER_THRESHOLD											=	2000;
	public static final int DEFAULT_USER_RANGE												=	200;
	public static final int DEFAULT_USER_ADD_PROXIMITY_RANGE								=	20;		// 기본 단위 미터
	public static final String DEFAULT_EMERGENCY_MESSAGE									=	"도와주세요! 위급 상황이 발생했습니다.";
	
	public static final int DATA_X															=	SensorManager.DATA_X;
	public static final int DATA_Y															=	SensorManager.DATA_Y;
	public static final int DATA_Z															=	SensorManager.DATA_Z;
	
	public static final String TABLE_POLICE_TEL												=	"POLICE_TEL";
	public static final String ATTRIBUTE_POLICE_TEL_LAT										=	"_lat";
	public static final String ATTRIBUTE_POLICE_TEL_LON										=	"_lon";
	public static final String ATTRIBUTE_POLICE_TEL_TEL										=	"_tel";
	
	public static final String TABLE_DATA													=	"DATA";
	public static final String ATTRIBUTE_DATA_CATEGORY										=	"_category";
	public static final String ATTRIBUTE_DATA_LAT											=	"_lat";
	public static final String ATTRIBUTE_DATA_LON											=	"_lon";
	public static final String ATTRIBUTE_DATA_ADDR											=	"_addr";
	public static final String ATTRIBUTE_DATA_NAME											=	"_name";
	public static final String ATTRIBUTE_DATA_TEL											=	"_tel";
	
	public static final String TABLE_MYLOCATION												=	"MYLOCATION";
	public static final String ATTRIBUTE_MYLOCATION_START_NAME								=	"_start_name";
	public static final String ATTRIBUTE_MYLOCATION_START_LAT								=	"_start_lat";
	public static final String ATTRIBUTE_MYLOCATION_START_LON								=	"_start_lon";
	public static final String ATTRIBUTE_MYLOCATION_START_ADDR								=	"_start_addr";
	public static final String ATTRIBUTE_MYLOCATION_DEST_NAME								=	"_dest_name";
	public static final String ATTRIBUTE_MYLOCATION_DEST_LAT								=	"_dest_lat";
	public static final String ATTRIBUTE_MYLOCATION_DEST_LON								=	"_dest_lon";
	public static final String ATTRIBUTE_MYLOCATION_DEST_ADDR								=	"_dest_addr";
	public static final String ATTRIBUTE_MYLOCATION_DEST_HOUR								=	"_dest_hour";
	public static final String ATTRIBUTE_MYLOCATION_DEST_MIN								=	"_dest_min";
	public static final String ATTRIBUTE_MYLOCATION_PATH_BYTES								=	"_path_bytes";
	
	public static final String TABLE_FRIENDS												=	"FRIENDS";
	public static final String ATTRIBUTE_FRIENDS_NAME										=	"_name";
	public static final String ATTRIBUTE_FRIENDS_TEL										=	"_phone_number";
	
	public static final String TABLE_EMAIL													=	"EMAIL";
	public static final String ATTRIBUTE_EMAIL_NAME											=	"_name";
	public static final String ATTRIBUTE_EMAIL_EMAIL										=	"_email";
	
	public static final String GPS															=	LocationManager.GPS_PROVIDER;
	public static final String NETWORK														=	LocationManager.NETWORK_PROVIDER;
	public static final String SENT															=	"SMS_SENT";
	
	public static final int FLAG_SELECT_PATH_LIST											=	1;
	public static final int FLAG_SETTINGS_SELECT_PATH_LIST									=	2;
	
	public static final int COMPARE_ACCURACY_DISTANCE										=	50;		// Meter	(default = 50)
	public static final int WARNING_WAIT_TIME												=	120;	// Sec		(default = 120)
	public static final int FAVORITE_LOCATION_RANGE											=	20;		// Meter 	(default = 20)
	public static final int SHAKE_SENSOR_COUNT												=	6;
	public static final int VIRBRATOR_COUNT													=	3;
	public static final long VIRBRATOR_DURING_TIME											=	3000;
	
	public static final int DELAY_GET_POLICE_TEL											=	20000;
	public static final int DELAY_GET_MY_ADDRESS											=	5000;
	public static final int DELAY_BUTTON_PUSH												=	3000;
	
	public static final int CCTV_SCORE														=	1;
	public static final int POLICE_SCORE													=	5;
	public static final int CONVENIENCE_SCORE												=	2;
	public static final int FIRE_SCORE														=	1;
	
	public static final String DEST_BALLON_TEXT												=	"도착지로 설정";
	public static final int MAP_EXIT_DIALOG													=	1;
	public static final int APP_EXIT_DIALOG													=	2;
	public static final int START_STOP_DIALOG												=	3;
	public static final int MAP_END_DEST_DIALOG												=	4;
	public static final int EMERGENCY_EXIT_DIALOG											=	5;

	public static final int EMG_ADD_DIALOG_FLAG												=	1;
	public static final int EMG_MODIFY_DIALOG_FLAG											=	2;
	
	public static final int PIN_MAIN_VIEW_FLAG												=	1;
	public static final int PIN_PATH_VIEW_FLAG												=	2;
	
	public static final float DISTANCE_UPDATE_PERIOD										=	5.0f;
	public static final long TIME_UPDATE_PERIOD												=	10000;
	
	public static final int MAP_ZOOM_LEVEL													=	18;
	
	public static final int DIALOG_FLAG_MOIDFY_START_ADDR									=	1;
	public static final int DIALOG_FLAG_MOIDFY_DEST_ADDR									=	2;
	
	public static final String PUT_FAKE_CALL_NAME											=	"PUT_FAKE_CALL_NAME";
	public static final String PUT_FAKE_CALL_TEL											=	"PUT_FAKE_CALL_TEL";
	public static final String PUT_SEND_BBOX_COUNT											=	"PUT_SEND_BBOX_COUNT";
	
	public static final int SAFE_STATE_INIT													=	0;
	public static final int SAFE_STATE_DANGEROUS											=	1;
	public static final int SAFE_STATE_CAUTION												=	2;
	public static final int SAFE_STATE_NORMAL												=	3;
	public static final int SAFE_STATE_SAFE													=	4;
	
	
	public static final String CALL_BY_WIDGET_PATH											=	"CALL_BY_WIDGET_PATH";
	
	public static final String ACTION_WIDGET_STATE_UPDATE									=	"action.widget.STATE.UPDATE";
	public static final String ACTION_WIDGET_SAFE_STATE_CHANGE								=	"action.widget.SAFE.STATE.CHANGE";
	
	
	// 블랙 박스 설정 관련
	public static final int MIN_TO_MEGA														=	(int) 2.5;
	public static final int SEND_CHECK_DURATION_TIME										=	10;					//	기본 단위 (초)
	public static final String FILE_PATH_DELIMETER											=	"_";
	public static final String BBOX_FILE_NAME												=	"SafeReturn_Video_";
	public static final String BBOX_FILE_EXTENSION											=	"mp4";
	
	
}









