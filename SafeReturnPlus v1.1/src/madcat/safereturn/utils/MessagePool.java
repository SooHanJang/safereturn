package madcat.safereturn.utils;

import java.util.ArrayList;

import madcat.safereturn.geopoint.SavePathGeoPoint;
import madcat.safereturn.maps.CCTVOverlay;
import madcat.safereturn.maps.DestPinOverlay;
import madcat.safereturn.maps.FireOverlay;
import madcat.safereturn.maps.PoliceOverlay;
import madcat.safereturn.maps.StartPinOverlay;
import madcat.safereturn.maps.StoreOverlay;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

public class MessagePool extends Application {
	
	private boolean mLongClickFlag;
	private boolean mSetDestFlag;
	private boolean mReturningFlag;
	private boolean mAddProximityFlag;
	private boolean mServiceFlag;
	private boolean mBServiceFlag;
	private boolean mRunningBService;
	private boolean mEmergencyStateFlag									=	false;
	
	private RelativeLayout mLinearFullLayout;
	private LinearLayout mLinearSummaryLayout/*, mStartReturningLayout*/;
	private GeoPoint mGeoStart, mGeoDest;
	private Context mContext;
	private ArrayList<SavePathGeoPoint> mSavePathGeoPoint;
	private PendingIntent mDestPendingIntent;
	private LocationManager mLocationManager;
	private MapView mMapView;
	private StartPinOverlay mStartPinOverlay;
	private DestPinOverlay mDestPinOverlay;
	private int mDestHour, mDestMin;
	private EditText mEditDest, mEditHourText, mEditMinText;
	private boolean mFavoriteFlag;
	
	private CCTVOverlay mCCTVOverlay;
	private PoliceOverlay mPoliceOverlay;
	private StoreOverlay mStoreOverlay;
	private FireOverlay mFireOverlay;
	
	private boolean mReturningTimerFlag;
	private boolean mWarningTimerFlag;
	private boolean mEmergencyFlag;
	
	private String mClosePoliceTel;
	private Intent mServiceIntent;
	private Intent mBServiceIntent;
	
	private TextView mShakeTestTextView;

	private String mMyAddress;
	
	private LinearLayout mEmergencyLayout;
	
	private int mRange;
	
	private boolean mFakeCallFlag										=	false;
	
	private Location mCurrentLocation;
	
	private String mCurrentSafeState;
	private int mCurrentWidgetState;
	private int mBCurrentCount;
	
	private String mBCurrentFilePath;
	
	private Button mBtnEmergencyCancel;
	
	public String getCurrentSafeState() {	return mCurrentSafeState;	}
	public void setCurrentSafeState(String currentSafeState) {	this.mCurrentSafeState = currentSafeState;	}
	
	public int getCurrentWidgetState() {	return mCurrentWidgetState;	}
	public void setCurrentWidgetState(int currentWidgetState) {	this.mCurrentWidgetState = currentWidgetState;	}
	
	public Location getCurrentLocation() {	return mCurrentLocation;	}
	public void setCurrentLocation(Location currentLocation) {	this.mCurrentLocation = currentLocation;	}
	
	public boolean getFakeCallFlag() {	return mFakeCallFlag;	}
	public void setFakeCallFlag(boolean fakeCallFlag) {	this.mFakeCallFlag = fakeCallFlag;	}
	
	public void setMyAddress(String addr) { mMyAddress = addr; }
	public String getMyAddress() { return mMyAddress; }
	
	public void setServiceFlag(boolean flag) { mServiceFlag = flag; }
	public boolean getServiceFlag() { return mServiceFlag; }
	
	public void setBServiceFlag(boolean flag) { mBServiceFlag = flag; }
	public boolean getBServiceFlag() { return mBServiceFlag; }
	
	public void setRunningBService(boolean flag) { mRunningBService = flag; }
	public boolean isRunningBService() { return mRunningBService; }
	
	public void setShakeTestTextView(TextView textView) { mShakeTestTextView = textView; }
	public TextView getShakeTestTextView() { return mShakeTestTextView; }
	
	public void setServiceIntent(Intent intent) { mServiceIntent = intent; }
	public Intent getServiceIntent() { return mServiceIntent; }
	
	public void setBServiceIntent(Intent intent) { mBServiceIntent = intent; }
	public Intent getBServiceIntent() { return mBServiceIntent; }
	
	public void setClosePoliceTel(String tel) { mClosePoliceTel = tel; }
	public String getClosePoliceTel() { return mClosePoliceTel; }
	
	public void setWarningTimerFlag(boolean flag) { mWarningTimerFlag = flag; }
	public boolean getWarningTimerFlag() { return mWarningTimerFlag; }
	
	public void setReturningTimerFlag(boolean flag) { mReturningTimerFlag = flag; }
	public boolean getReturningTimerFlag() { return mReturningTimerFlag; }
	
	public void setFireOverlay(FireOverlay fireOverlay) { mFireOverlay = fireOverlay; }
	public FireOverlay getFireOverlay() { return mFireOverlay; }
	
	public void setStoreOverlay(StoreOverlay storeOverlay) { mStoreOverlay = storeOverlay; }
	public StoreOverlay getStoreOverlay() { return mStoreOverlay; }
	
	public void setPoliceOverlay(PoliceOverlay policeOverlay) { mPoliceOverlay = policeOverlay; }
	public PoliceOverlay getPoliceOverlay() { return mPoliceOverlay; }
	
	public void setCCTVOverlay(CCTVOverlay cctvOverlay) { mCCTVOverlay = cctvOverlay; }
	public CCTVOverlay getCCTVOverlay() { return mCCTVOverlay; }
	
	public void setFavoriteFlag(boolean flag) { mFavoriteFlag = flag; }
	public boolean getFavoriteFlag() { return mFavoriteFlag; }
	
	public void setEditDest(EditText editDest) { mEditDest = editDest; }
	public EditText getEditDest() { return mEditDest; }
	
	public void setDestHour(int destHour) { mDestHour = destHour; }
	public int getDestHour() { return mDestHour; }
	
	public void setDestMin(int destMin) { mDestMin = destMin; }
	public int getDestMin() { return mDestMin; }
	
	public void setStartPinOverlay(StartPinOverlay startPin) { mStartPinOverlay = startPin; }
	public StartPinOverlay getStartPinOverlay() { return mStartPinOverlay; }
	
	public void setDestPinOverlay(DestPinOverlay destPin) { mDestPinOverlay = destPin; }
	public DestPinOverlay getDestPinOverlay() { return mDestPinOverlay; }
	
	public void setMapView(MapView mapView) { mMapView = mapView; }
	public MapView getMapView() { return mMapView; }
	
	public void setLocationManger(LocationManager locM) { mLocationManager = locM; }
	public LocationManager getLocationManager() { return mLocationManager; }
	
	public void setDestPendingIntent(PendingIntent pending) { mDestPendingIntent = pending; }
	public PendingIntent getDestPendingIntent() { return mDestPendingIntent; }
	
	public void setAddProximityFlag(boolean flag) { mAddProximityFlag = flag; }
	public boolean getAddProximityFlag() { return mAddProximityFlag; }
	
	public void setArraySavePathGeoPoint(ArrayList<SavePathGeoPoint> saveGP) { mSavePathGeoPoint = saveGP; }
	public ArrayList<SavePathGeoPoint> getArraySavePathGeoPoint() { return mSavePathGeoPoint; }
	
	public void setLongClickFlag(boolean flag) { mLongClickFlag = flag; }
	public boolean getLongClickFlag() { return mLongClickFlag; }
	
	public void setDestFlag(boolean flag) { mSetDestFlag = flag; }
	public boolean getDestFlag() { return mSetDestFlag; }
	
	public void setFullLayout(RelativeLayout fullLayout) { mLinearFullLayout = fullLayout; }
	public RelativeLayout getFullLayout() { return mLinearFullLayout; }
	
	public void setSummaryLayout(LinearLayout summaryLayout) { mLinearSummaryLayout = summaryLayout; }
	public LinearLayout getSummaryLayout() { return mLinearSummaryLayout; }
	
	public void setEmergencyLayout(LinearLayout emergencyLayout) { mEmergencyLayout = emergencyLayout; }
	public LinearLayout getEmergencyLayout() { return mEmergencyLayout; }
	
	public void setGeoDest(GeoPoint geoDest) { mGeoDest = geoDest; }
	public GeoPoint getGeoDest() { return mGeoDest; }
	
	public void setGeoStart(GeoPoint geoStart) { mGeoStart = geoStart; }
	public GeoPoint getGeoStart() { return mGeoStart; }
	
	public void setReturningFlag(boolean returnFlag) { mReturningFlag = returnFlag; }
	public boolean getReturningFlag() { return mReturningFlag; }
	
	public void setContext(Context context) { mContext = context; }
	public Context getContext() { return mContext; }
	
	public void setEmergencyFlag(boolean emergencyFlag) { mEmergencyFlag = emergencyFlag; }
	public boolean getEmergencyFlag() { return mEmergencyFlag; }
	
	public void setEditHourText(EditText hourEditText) {	this.mEditHourText = hourEditText;	}
	public EditText getEditHourText() {	return mEditHourText;	}
	
	public void setEditMinText(EditText minEditText) {	this.mEditMinText = minEditText;	}
	public EditText getEditMinText() {	return mEditMinText;	}
	
	public void setRange(int range)	{	this.mRange = range;	}
	public int getRange()	{	return mRange;	}
	
	public void setBCurrentCount(int count) {	this.mBCurrentCount = count;	}
	public int getBCurrentCount()	{	return mBCurrentCount;	}
	
	public void setBCurrentFilePath(String filePath)	{	this.mBCurrentFilePath = filePath;	}
	public String getBCurrentFilePath()	{	return mBCurrentFilePath;	}
	
	public void setEmergencyStateFlag(boolean flag)	{	this.mEmergencyStateFlag = flag;	}
	public boolean getEmergencyStateFlag()	{	return mEmergencyStateFlag;	}
	
	public void setBtnEmergencyCancel(Button button) {	this.mBtnEmergencyCancel = button;	}
	public Button getBtnEmergencyCancel() {	return mBtnEmergencyCancel;	}
}
