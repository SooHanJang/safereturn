package madcat.safereturn.utils;

import madcat.safereturn.constants.Constants;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

public class ShakeCallSensor implements SensorEventListener {

//	private static final String TAG = "ShakeSensor";
	
	private float x, y, z, speed, lastX, lastY, lastZ;
	private long lastTime;
	
	private SharedPreferences mPref;
	private SensorManager mSensorManager;
	private Sensor mAccelerometerSensor;
	private Context mContext;
	private MessagePool mMessagePool;
	
	private boolean mTestFlag = false;
	
	private String mEmergencyTel;
	private int mCount = 0;
	private int mThresHold;
	private int mTestSendingMessageCount = 0;
	private String mEmergencyMessage;
	
	public ShakeCallSensor(Context context) {
		mContext = context;
		mMessagePool = (MessagePool)mContext.getApplicationContext();
		mPref = context.getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
		mAccelerometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mThresHold = mPref.getInt(Constants.SETTING_USER_THRESHOLD, Constants.DEFAULT_USER_THRESHOLD);
		mEmergencyMessage = mPref.getString(Constants.SETTING_EMERGENCY_MESSAGE, Constants.DEFAULT_EMERGENCY_MESSAGE);
	}
	
	public void registerSensor() {
		if(mAccelerometerSensor != null) {
			mSensorManager.registerListener(this, mAccelerometerSensor, SensorManager.SENSOR_DELAY_GAME);
			this.setThresHold(mThresHold);
		}
	}
	
	public void unregisterSensor() {
		if(mSensorManager != null) {
			mSensorManager.unregisterListener(this);
		}
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            long currentTime = System.currentTimeMillis();
            long gabOfTime = (currentTime - lastTime);
   
            if (gabOfTime > 100) {
                lastTime = currentTime;
   
                x = event.values[SensorManager.DATA_X];
                y = event.values[SensorManager.DATA_Y];
                z = event.values[SensorManager.DATA_Z];
   
                speed = Math.abs(x + y + z - lastX - lastY - lastZ) /
                        gabOfTime * 10000;

                /* 만약 속도가 getThresHold 이상 발생하면 mCount 를 증가시키고
                 * 특정 미만의 속도가 포착되면 mCount 를 0으로 초기화 시키든, 감소 시키든 하도록 구현한다.
                 */
                
            	if(mTestFlag != false) {		// 테스트 할 때 표시하는 부분
            		if (speed > getThresHold()) {
                    	mCount++;
            		} else if(speed < getThresHold() - 1200) {
            			mCount = 0;
            		}
            		
            		if(mCount >= Constants.SHAKE_SENSOR_COUNT) {
            			mTestSendingMessageCount++;
	            		TextView tv = mMessagePool.getShakeTestTextView();
	            		tv.setText(mTestSendingMessageCount + " 회 알림 성공");
            		}
            	} else {
            		if(speed > getThresHold()) {
            			mCount++;
            		} else if(speed < getThresHold() - 1200) {
            			mCount = 0;
            		}
            		
//            		Log.d(TAG, "현재 count : " + mCount);
                	
                	if(mCount >= Constants.SHAKE_SENSOR_COUNT) {
                		mCount = 0;
//                		Log.d(TAG, "현재 전화번호 : " + getPoliceTel());
                		
                		// 현재 상황을 위험 상황으로 변화, 긴급 상황 취소 버튼 활성화
                		mMessagePool.setEmergencyStateFlag(true);
                		mMessagePool.getBtnEmergencyCancel().setVisibility(View.VISIBLE);
                		
                		Intent emergencyCallIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+getEmergencyTel()));
                		emergencyCallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                		mContext.startActivity(emergencyCallIntent);
                		
                		String helpMsg = mEmergencyMessage + "(위치 : " + mMessagePool.getMyAddress() + ")";
                		String locMsg = null;
                		
                		if(mMessagePool.getCurrentLocation() != null) {
                			locMsg = "(위치 확인 : http://maps.google.co.kr/maps?q=" 
                				+ mMessagePool.getCurrentLocation().getLatitude() + "+" 
                				+ mMessagePool.getCurrentLocation().getLongitude() + ")"; 
                			
	                		Utils.sendMMS(mContext, helpMsg, locMsg);
	                				
                		}
                		
                		Utils.sendMMS(mContext, helpMsg, locMsg);
							
//                		Log.d(TAG, "핸드폰 흔들어서 위급 메시지 전송 완료 (" + mEmergencyMessage + ")");
                	}
            	}
                lastX = event.values[Constants.DATA_X];
                lastY = event.values[Constants.DATA_Y];
                lastZ = event.values[Constants.DATA_Z];
            }
        }
	}
	
//	public void sendSMS(String phoneNumber, String message) {
//		SmsManager smsManager = SmsManager.getDefault();
//		smsManager.sendTextMessage(phoneNumber, null, message, null, null);
//	}
	
	public void setEmergencyTel(String emergencyTel, int count) {
    	mEmergencyTel = emergencyTel;
    	mCount = count;
    }
    
    public String getEmergencyTel() { 	return mEmergencyTel;  }

    public int getShakeCount() { return mCount;  }
    public void setShakeCount(int count) { 	mCount = count;  }
    
    public void setThresHold(int threshold) { mThresHold = threshold; }
    public int getThresHold() { return mThresHold;  }
    public void setTestFlag(boolean flag) {	mTestFlag = flag;  }
}
