package madcat.safereturn.utils;

import java.io.IOException;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;

public class EmergencySiren {

	private static EmergencySiren mSirenObject					=	new EmergencySiren();
	private boolean mPlayFlag									=	false;
	private int STREAM_TYPE										=	AudioManager.STREAM_RING;
	
	private Context mContext;
	private MediaPlayer mMediaPlayer;
	private AudioManager mAudioManager;
	private int mCurrVol, mMaxVol;
	
	private EmergencySiren() {}
	
	public void setContext(Context context) {
		this.mContext = context;
	}
	
	public boolean isPlaySiren() {
		return mPlayFlag;
	}
	
	public void initMediaPlayer() {
		try {
			// ���� ����
			mAudioManager = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);
			mCurrVol = mAudioManager.getStreamVolume(STREAM_TYPE);
			mMaxVol = mAudioManager.getStreamMaxVolume(STREAM_TYPE);

			AssetFileDescriptor afd = mContext.getAssets().openFd("siren/emergency_siren.wav");
			
			mMediaPlayer = new MediaPlayer();
			mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
	     	
	     	mMediaPlayer.setAudioStreamType(STREAM_TYPE);
			mMediaPlayer.setLooping(true);
			
			mMediaPlayer.prepare();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void startMediaPlayer() {
		if(mMediaPlayer != null) {
			mAudioManager.setStreamVolume(STREAM_TYPE, mMaxVol, AudioManager.FLAG_PLAY_SOUND);
			mMediaPlayer.start();
			mPlayFlag = true;
		}
	}
	
	public void stopMediaPlayer() {
		if(mMediaPlayer != null) {
			mMediaPlayer.stop();
			mAudioManager.setStreamVolume(STREAM_TYPE, mCurrVol, AudioManager.FLAG_PLAY_SOUND);
			mPlayFlag = false;
		}
	}
	
	public static EmergencySiren getInstance() {
		return mSirenObject;
	}
}
