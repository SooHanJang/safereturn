package madcat.safereturn.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import madcat.safereturn.adapter.EMailAddress;
import madcat.safereturn.adapter.EmergencyAddress;
import madcat.safereturn.adapter.PathLocation;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.maps.CCTVOverlay;
import madcat.safereturn.maps.DestPinOverlay;
import madcat.safereturn.maps.FireOverlay;
import madcat.safereturn.maps.MyOverlay;
import madcat.safereturn.maps.PoliceOverlay;
import madcat.safereturn.maps.StartPinOverlay;
import madcat.safereturn.maps.StoreOverlay;
import madcat.safereturn.pro.R;
import madcat.safereturn.widget.WidgetSafeReturn;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;

public class Utils {
	
	private static final String TAG										=	"Utils";
	private static final boolean DEVELOPE_MODE							=	Constants.DEVELOPE_MODE;
		
	public static Criteria setCriteria() {
		
    	Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);
        
        return criteria;
	}
	
	public static String getLocAddress(double lat, double lon, Context context) {
    	StringBuilder geoString = new StringBuilder();
    	String myAddress = null;
    	
    	try {
    		Geocoder geoCoder = new Geocoder(context, Locale.getDefault());
    		Address adr = geoCoder.getFromLocation(lat, lon, 1).get(0);
    		myAddress = adr.getAddressLine(0);
    		
    		if(adr.getLocality() != null) geoString.append(adr.getLocality()).append(" ");
    		if(adr.getThoroughfare() != null) geoString.append(adr.getThoroughfare());
    		if(!"".equals(geoString.toString())) geoString.append("\n\n");
    	} catch(Exception e) {}
    	
    	return myAddress;
	}
	
	public static MapView.LayoutParams getMapLayoutParams(int width, int height, GeoPoint geo) {
		MapView.LayoutParams params = new MapView.LayoutParams(width, height, 
				geo, 0, 0, MapView.LayoutParams.BOTTOM_CENTER);
		
		return params;
	}
	
	public static void insertDbPath(String startName, double startLat, double startLon, String startAddr,
									String destName, double destLat, double destLon, String destAddr,
									int destHour, int destMin, byte[] pathBytes) {
		
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues values = new ContentValues();
		values.put(Constants.ATTRIBUTE_MYLOCATION_START_NAME, startName);
		values.put(Constants.ATTRIBUTE_MYLOCATION_START_LAT, startLat);
		values.put(Constants.ATTRIBUTE_MYLOCATION_START_LON, startLon);
		values.put(Constants.ATTRIBUTE_MYLOCATION_START_ADDR, startAddr);
		values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_NAME, destName);
		values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_LAT, destLat);
		values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_LON, destLon);
		values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_ADDR, destAddr);
		values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_HOUR, destHour);
		values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_MIN, destMin);
		values.put(Constants.ATTRIBUTE_MYLOCATION_PATH_BYTES, pathBytes);
	
		sdb.insert(Constants.TABLE_MYLOCATION, null, values);
		sdb.close();
	}
	
	public static void insertDbEmail(String name, String email) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues values = new ContentValues();
		values.put(Constants.ATTRIBUTE_EMAIL_NAME, name);
		values.put(Constants.ATTRIBUTE_EMAIL_EMAIL, email);
	
		sdb.insert(Constants.TABLE_EMAIL, null, values);
		sdb.close();
	}
	
	public static ArrayList<PathLocation> getPathList() {
		
		PathLocation pathLocation;
		ArrayList<PathLocation> pathNameOrders = new ArrayList<PathLocation>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String loadDbPath = "select * from " + Constants.TABLE_MYLOCATION;
		Cursor cursor = sdb.rawQuery(loadDbPath, null);
		
		while(cursor.moveToNext()) {
			pathLocation = new PathLocation(cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3), cursor.getString(4),
					cursor.getString(5), cursor.getDouble(6), cursor.getDouble(7), cursor.getString(8), cursor.getInt(9), cursor.getInt(10),
					cursor.getBlob(11));
			pathNameOrders.add(pathLocation);
		}
		
		cursor.close();
		sdb.close();
		
		return pathNameOrders;
	}
	
	public static InternetAddress[] getMailList(Context context) {
		try {
			
			SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			
			String mailListSql = "SELECT * FROM " + Constants.TABLE_EMAIL;
			Cursor cursor = sdb.rawQuery(mailListSql, null);

			cursor.moveToFirst();
	
			InternetAddress[] items = new InternetAddress[cursor.getCount()];
			
			for(int i=0; i < items.length; i++) {
				items[i] = new InternetAddress(cursor.getString(2));
				cursor.moveToNext();
			}
			
			cursor.close();
			sdb.close();
			
			return items;
		} catch (AddressException e) {
			e.printStackTrace();
			
			return null;
		}
		
	}
	
	public static ArrayList<EMailAddress> getEMailList() {

		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String mailListSql = "SELECT * FROM " + Constants.TABLE_EMAIL;
		Cursor cursor = sdb.rawQuery(mailListSql, null);
		
		ArrayList<EMailAddress> items = new ArrayList<EMailAddress>();
		
		while(cursor.moveToNext()) {
			EMailAddress mailAddress = new EMailAddress(cursor.getString(1), cursor.getString(2));
			items.add(mailAddress);
		}
		
		cursor.close();
		sdb.close();
		
		return items;
		
	}
	
	public static byte[] serializeObject(Object object) {
//		Log.d(TAG, "직렬화 시작");
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(object);
			oos.close();
			
			byte[] buf = bos.toByteArray();
			
//			Log.d(TAG, "직렬화 종료");
			
			return buf;
		} catch (IOException e) {
			e.printStackTrace();
			
			return null;
		}
	}
	
	public static Object deSerializeObject(byte[] b) {
//		Log.d(TAG, "직렬화 읽기 시작 : " + b);
		
		try {
			ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(b));
			Object object = ois.readObject();
			
			ois.close();
			
//			Log.d(TAG, "직렬화 읽기 종료");
			
			return object;
		} catch(IOException ie) {
//			Log.d(TAG, "io error : " + ie);
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void removeStartPinOverlay(MapView mapView, StartPinOverlay startPinOverlay) {
//		Log.d(TAG, "removeStartPinOverlay 호출");
		
		int mapViewSize = mapView.getOverlays().size();
		
		for(int i=0; i < mapViewSize; i++) {
			if(mapView.getOverlays().get(i) instanceof StartPinOverlay) {
				mapView.getOverlays().remove(i);
			}
		}
		
		if(startPinOverlay != null) {
			startPinOverlay.clearOverlay(mapView);
		}
	}
	
	public static void removeDestPinOverlay(MapView mapView, DestPinOverlay destPinOverlay) {
//		Log.d(TAG, "removeDestPinOverlay 호출");
		
		int mapViewSize = mapView.getOverlays().size();
		
		for(int i=0; i < mapViewSize; i++) {
			if(mapView.getOverlays().get(i) instanceof DestPinOverlay) {
				mapView.getOverlays().remove(i);
			}
		}

		if(destPinOverlay != null) {
			destPinOverlay.clearOverlay(mapView);
		}
	}
	
	public static float distanceBetween(GeoPoint startGeoPoint, GeoPoint endGeoPoint) {
		float[] results = new float[1];
		
		Location.distanceBetween(startGeoPoint.getLatitudeE6()/1E6, startGeoPoint.getLongitudeE6()/1E6, 
				endGeoPoint.getLatitudeE6()/1E6, endGeoPoint.getLongitudeE6()/1E6, results);
		
		return results[0];
	}
	
	public static float distanceBetween(double startLat, double startLon, double destLat, double destLon) {
		float[] results = new float[1];
		
		Location.distanceBetween(startLat, startLon, destLat, destLon, results);
		return results[0];
	}
	
	
	public static int metersToRadius(float meters, MapView mapView, double latitude) {
		return (int)(mapView.getProjection().metersToEquatorPixels(meters) * (1 / Math.cos(Math.toRadians(latitude))));
	}
	
	public static String distanceKMFormat(float distance) {
		DecimalFormat df = new DecimalFormat(",##0.00");

		return df.format(distance / 1000);
	}
	
	public static int timeToSecond(int hour, int min) {
		int totalSecond = (hour * 3600) + (min * 60);
		
		return totalSecond;
	}
	
	
	/*
	 * Cursor 는 반드시 한번 동작을 시킨 후. moveToFirst 속도가 느리므로 count로 초기화 한 후 return 할 것. 그렇지 않으면 동작안함.
	 */
	
	public static Cursor getClosePoliceTel(Location currentLocation) {
//		Log.d(TAG, "getClosePoliceTel 실행");
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String closePoliceTelSql = 
			"select " + Constants.ATTRIBUTE_POLICE_TEL_TEL + 
			", round(cast(abs(" + Constants.ATTRIBUTE_POLICE_TEL_LAT + " - " + currentLocation.getLatitude() + ")*111000 AS INTEGER), 0) + " +
			"round(cast(abs(" + Constants.ATTRIBUTE_POLICE_TEL_LON + " - " + currentLocation.getLongitude() + ")*88800 AS INTEGER),0) sum_dis" +
			" from " + Constants.TABLE_POLICE_TEL +
			" order by sum_dis" + 
			" limit 2";
		
		Cursor cursor = sdb.rawQuery(closePoliceTelSql, null);
		cursor.getCount();
		
//		Log.d(TAG, "getClosePoliceTel size : " + cursor.getCount());
		
		sdb.close();
		
//		Log.d(TAG, "getClosePoliceTel 종료");
		return cursor;
	}
	
	public static HashMap<String, Overlay> getBetweenOverlay(Location currentLocation, int saveRange, 
			CCTVOverlay cctvOverlay, PoliceOverlay policeOverlay, StoreOverlay storeOverlay, FireOverlay fireOverlay) {
//		long startTime = System.currentTimeMillis();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		HashMap<String, Overlay> hashData = new HashMap<String, Overlay>();
		
		double PlusLat = currentLocation.getLatitude() + ((saveRange*1E6) / (111000 * 1E6));
		double PlusLon = currentLocation.getLongitude() + ((saveRange*1E6) / (88800 * 1E6));
		double MinusLat = currentLocation.getLatitude() - ((saveRange*1E6) / (111000 * 1E6));
		double MinusLon = currentLocation.getLongitude() - ((saveRange*1E6) / (88800 * 1E6));
		
		String dataSql = "select * from " + Constants.TABLE_DATA + 
		   				 " where " + Constants.ATTRIBUTE_DATA_LAT + " between " + MinusLat + " and " + PlusLat +
		   				 " and " + Constants.ATTRIBUTE_DATA_LON + " between " + MinusLon + " and " + PlusLon;
		
		Cursor dataCursor = sdb.rawQuery(dataSql, null);
		
		while(dataCursor.moveToNext()) {
    		if(dataCursor.getString(1).equals("CCTV")) {
    			if(cctvOverlay != null) {
	    			OverlayItem cctvOverlayItem = new OverlayItem(
	    					new GeoPoint((int)(dataCursor.getDouble(2)*1E6), (int)(dataCursor.getDouble(3)*1E6)), 
	    							dataCursor.getString(5), dataCursor.getString(6));
	    			cctvOverlay.addOverlay(cctvOverlayItem);
    			}
    		} else if(dataCursor.getString(1).equals("POLICE")) {
    			if(policeOverlay != null) {
	    			OverlayItem policeOverlayItem = new OverlayItem(
	    					new GeoPoint((int)(dataCursor.getDouble(2)*1E6), (int)(dataCursor.getDouble(3)*1E6)), 
	    							dataCursor.getString(5), dataCursor.getString(6));
	    			policeOverlay.addOverlay(policeOverlayItem);
    			}
    		} else if(dataCursor.getString(1).equals("STORE")) {
    			if(storeOverlay != null) {
	    			OverlayItem storeOverlayItem = new OverlayItem(
	    					new GeoPoint((int)(dataCursor.getDouble(2)*1E6), (int)(dataCursor.getDouble(3)*1E6)), 
	    							dataCursor.getString(5), dataCursor.getString(6));
	    			storeOverlay.addOverlay(storeOverlayItem);
    			}
    		} else if(dataCursor.getString(1).equals("FIRE")) {
    			if(fireOverlay != null) {
	    			OverlayItem fireOverlayItem = new OverlayItem(
	    					new GeoPoint((int)(dataCursor.getDouble(2)*1E6), (int)(dataCursor.getDouble(3)*1E6)), 
	    							dataCursor.getString(5), dataCursor.getString(6));
	    			fireOverlay.addOverlay(fireOverlayItem);
    			}
    		}
    	}
		
		dataCursor.close();
		sdb.close();
		
		if(cctvOverlay != null) {
			hashData.put("CCTV", cctvOverlay);
		}
		
		if(policeOverlay != null) {
			hashData.put("POLICE", policeOverlay);
		}
		
		if(storeOverlay != null) {
			hashData.put("STORE", storeOverlay);
		}
		
		if(fireOverlay != null) {
			hashData.put("FIRE", fireOverlay);
		}
		
//		long endTime = System.currentTimeMillis();
		
//		Log.d(TAG, "수행 속도 : " + (endTime - startTime) / 1000.0);
		
		return hashData;
		
	}
	
	public static ArrayList<EmergencyAddress> getEmergencyFriendsList() {
		EmergencyAddress emergencyFriends;
		ArrayList<EmergencyAddress> mEmergencyFriendsOrders = new ArrayList<EmergencyAddress>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String friendsList = "select * from " + Constants.TABLE_FRIENDS;
		Cursor cursor = sdb.rawQuery(friendsList, null);
		
		while(cursor.moveToNext()) {
			emergencyFriends = new EmergencyAddress(cursor.getString(1), cursor.getString(2));
			mEmergencyFriendsOrders.add(emergencyFriends);
		}
		
		cursor.close();
		sdb.close();
		
		return mEmergencyFriendsOrders;
	}
	
	public static void deleteDBFriends(String name, String tel) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		sdb.delete(Constants.TABLE_FRIENDS, Constants.ATTRIBUTE_FRIENDS_NAME + "='" + name + "' and " +
				   Constants.ATTRIBUTE_FRIENDS_TEL + "='" + tel + "'", null);
		
		sdb.close();
	}
	
	public static void deleteDBPath(String start_name, String dest_name) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		sdb.delete(Constants.TABLE_MYLOCATION, Constants.ATTRIBUTE_MYLOCATION_START_NAME + "='" + start_name + "' and " +
				   Constants.ATTRIBUTE_MYLOCATION_DEST_NAME + "='" + dest_name + "'", null);
		
		sdb.close();
	}
	
	public static void deleteDBEmail(String name, String email) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		sdb.delete(Constants.TABLE_EMAIL, Constants.ATTRIBUTE_EMAIL_NAME + "='" + name + "' and " +
				   Constants.ATTRIBUTE_EMAIL_EMAIL + "='" + email + "'", null);
		
		sdb.close();
	}
	
	public static void insertDBFriends(String name, String tel) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues values = new ContentValues();
		
		values.put(Constants.ATTRIBUTE_FRIENDS_NAME, name);
		values.put(Constants.ATTRIBUTE_FRIENDS_TEL, tel);
		
		sdb.insert(Constants.TABLE_FRIENDS, null, values);
		sdb.close();
	}
	
	public static void updateDBFriends(EmergencyAddress emergencyFriends, String name, String tel) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues values = new ContentValues();
		
		values.put(Constants.ATTRIBUTE_FRIENDS_NAME, name);
		values.put(Constants.ATTRIBUTE_FRIENDS_TEL, tel);
		
		sdb.update(Constants.TABLE_FRIENDS, values, Constants.ATTRIBUTE_FRIENDS_NAME + "='" + emergencyFriends.getFriendsName() + "' and " +
													Constants.ATTRIBUTE_FRIENDS_TEL + "='" + emergencyFriends.getFriendsTel() + "'", null);
		
		sdb.close();
	}
	
	public static void updateDBEmail(String beforeName, String beforeEmail, String changeName, String changeEmail) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues values = new ContentValues();
		
		values.put(Constants.ATTRIBUTE_EMAIL_NAME, changeName);
		values.put(Constants.ATTRIBUTE_EMAIL_EMAIL, changeEmail);
		
		sdb.update(Constants.TABLE_EMAIL, values, Constants.ATTRIBUTE_EMAIL_NAME + "='" + beforeName + "' and " +
													Constants.ATTRIBUTE_EMAIL_EMAIL + "='" + beforeEmail + "'", null);
		
		sdb.close();
		
	}
	
	public static String checkSafePercent(int cctvNumber, int convenienceNumber, int policeNumber,  int fireNumber) {
		int totalScore = (cctvNumber * Constants.CCTV_SCORE) + (convenienceNumber * Constants.CONVENIENCE_SCORE) +
						 (policeNumber * Constants.POLICE_SCORE) + (fireNumber * Constants.FIRE_SCORE);
		
		int totalPercent = (totalScore * 10) / 3;
		
		if(totalPercent >= 50) {
			return "안전 지역";
		} else if(totalPercent < 50 && totalPercent >= 35) {
			return "일반 지역";
		} else if(totalPercent < 35 && totalPercent >= 20) {
			return "주의 지역";
		} else {
			return "위험 지역";
		}
	}

	public static void sendMMS(Context context, String helpMsg, String locMsg) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "sendSMS 시작");
		}

		try {
			SharedPreferences pref = context.getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
			
			ArrayList<EmergencyAddress> friendsOrders = Utils.getEmergencyFriendsList();
			android.telephony.SmsManager smsManager = android.telephony.SmsManager.getDefault();
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "SmsManager 객체 상태 : " + smsManager);
				Log.d(TAG, "전달 된 메시지(1) : " + helpMsg);
				Log.d(TAG, "전달 된 메시지(2) : " + locMsg);
				Log.d(TAG, "현재 친구 목록 크기 : " + friendsOrders.size());
			}
			
			ArrayList<String> msgParts = smsManager.divideMessage(helpMsg);
			
			int firendsSize = friendsOrders.size();
			for(int i=0; i < firendsSize; i++) {
				smsManager.sendMultipartTextMessage(friendsOrders.get(i).getFriendsTel(), null, msgParts, null, null);
				
				if(locMsg != null) {
					smsManager.sendTextMessage(friendsOrders.get(i).getFriendsTel(), null, locMsg, null, null);
				}
			}
		} catch(Exception e) {
			Toast.makeText(context, context.getString(R.string.toast_title_caution_send_sms_fail), Toast.LENGTH_SHORT).show();
		}
	}
	
	public static String getAddress(Context context, double lat, double lon) {
//    	Log.d(TAG, "getAddress 시작");
    	String myAddress = null;
    	try {
    		Geocoder geoCoder = new Geocoder(context.getApplicationContext(), Locale.getDefault());
    		Address adr = geoCoder.getFromLocation(lat, lon, 1).get(0);
    		myAddress = adr.getAddressLine(0);
    	} catch(Exception e) {}
    	return myAddress;
    }
	
	/**
	 * 부모 View를 기준으로 자식 View를 재귀 호출하면서 할당된 이미지를 해제한다.
	 * @param root
	 */
	
	public static void recursiveRecycle(View root) {
		if(root == null) {
			return;
		}
		
		if(root instanceof ViewGroup) {
			ViewGroup group = (ViewGroup)root;
			int count = group.getChildCount();
			
			for(int i=0; i < count; i++) {
				recursiveRecycle(group.getChildAt(i));
			}
			
			if(!(root instanceof AdapterView)) {
				group.removeAllViews();
			}
		}
		
		if(root instanceof ImageView) {
			((ImageView)root).setImageDrawable(null);
		}
		
		root.setBackgroundDrawable(null);
		
		root = null;
		
		return;
	}
	
	public static int validationDestTime(String hour, String min) {
		int temp_hour = Integer.parseInt(hour);
		int temp_min = Integer.parseInt(min);
		
		if(temp_hour > 12) {
			return 1;			// 시간 에러
		}
		
		if(temp_min >= 60) {
			return 2;			// 분 에러
		}
		
		if(temp_hour == 0 && temp_min == 0) {
			return 3;			// 0 시 0분 에러
		}
		
		else return 4;			// 에러 없음
	}
	
	public static int dipFromPixel(Context context, int pixel) {
	    float scale = context.getResources().getDisplayMetrics().density;
	    
	    return (int)(pixel / Constants.DEFAULT_HDIP_DENSITY_SCALE * scale);
	}
	
	public static int pixelFromDip(Context context, int dip) {
		float scale = context.getResources().getDisplayMetrics().density;
	    
	    return (int)(dip / scale * Constants.DEFAULT_HDIP_DENSITY_SCALE);
	}
	
	// 현재 내 위치를 실시간으로 표시해주기 위한 메소드
    public static void setMyLocationOverlay(final MapView mapView, Context context, int range) {
    	final MyOverlay myLocationOverlay = new MyOverlay(context, mapView, range);
    	myLocationOverlay.enableMyLocation();
         
    	myLocationOverlay.runOnFirstFix(new Runnable() {
    		public void run() {
 				mapView.getController().animateTo(myLocationOverlay.getMyLocation());
 			}
 		});
         
        mapView.getOverlays().add(myLocationOverlay);
        mapView.invalidate();
    }

    /**
	 * 정해진 Action 을 BroadCasting 한 뒤, MessagePool 에 해당 액션이 발생되었음을 기록한다.
	 * 
	 * @param context
	 * @param action
	 */
	
	public static void widgetStateUpdate(Context context, int state) {
		// 현재 위젯 상태를 저장
		MessagePool messagePool = (MessagePool)context.getApplicationContext();
		messagePool.setCurrentWidgetState(state);
		
		if(state == WidgetSafeReturn.WIDGET_STATE_DEFAULT) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "안전도 상태를 초기화 합니다.");
			}
			
			messagePool.setCurrentSafeState("init");
		}
		
		// 위젯 업데이트 호출
		Intent intent = new Intent(Constants.ACTION_WIDGET_STATE_UPDATE);
		context.sendBroadcast(intent);
	}
	
	public static void widgetSafeUpdate(Context context, String safe) {
		// 현재 안전도 상태를 저장
		MessagePool messagePool = (MessagePool)context.getApplicationContext();
		messagePool.setCurrentSafeState(safe);
		
		// 위젯 업데이트 호출
		Intent intent = new Intent(Constants.ACTION_WIDGET_SAFE_STATE_CHANGE);
		context.sendBroadcast(intent);
			
	}
	
	/**
	 * 핸드폰 화면의 가로 크기를 dip 단위로 가져옵니다.
	 * 
	 * @param context
	 * @return int형의 핸드폰 화면의 가로 크기 dip
	 */
	
	public static int getScreenDipWidthSize(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		
		int dipWidth = (int) (displayMetrics.widthPixels / displayMetrics.density);
		
		return dipWidth;
	}

	/**
	 * 핸드폰 화면의 세로 크기를 dip 단위로 가져옵니다.
	 * 
	 * @param context
	 * @return int형의 핸드폰 화면의 세로 크기 dip
	 */
	
	public static int getScreenDipHeightSize(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		
		int dipHeight = (int) (displayMetrics.heightPixels / displayMetrics.density);
		
		return dipHeight;
	}
	
	
	/**
	 * 사용자가 핸드폰에 등록한 기본 메일 주소를 가져옵니다.
	 * 
	 * @param context
	 * @return
	 */
	
	public static String getAccountInfo(Context context) {
    	AccountManager am = (AccountManager)context.getSystemService(Context.ACCOUNT_SERVICE);
    	Account[] accounts = am.getAccountsByType("com.google");
    	
    	if(accounts.length > 0) {
    		return accounts[0].name;
    	} else {
    		return null;
    	}
    }
	
	/**
	 *	3G 사용여부 체크 
	 * @param context
	 * @return
	 */
	
	public static boolean is3gNetwork(Context context) {
		ConnectivityManager manager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		boolean isRunning = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
		
		return isRunning;
	}
	
	
	/**
	 * Wifi 사용여부 체크 
	 * @param context
	 * @return
	 */
	
	public static boolean isWifiNetwork(Context context) {
		ConnectivityManager manager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		boolean isRunning = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
		
		return isRunning;
	}
	
	/**
	 * 사용자의 핸드폰 번호를 String 타입으로 반환합니다.
	 * 
	 * @param context
	 * @return
	 */
	
	public static String getPhoneNumber(Context context) {
		TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getLine1Number();
	}
}










