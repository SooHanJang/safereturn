package madcat.safereturn.utils;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import madcat.safereturn.constants.Constants;

import android.util.Log;

public class EmailClient {
	
	private final String TAG										=	"EmailClient";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private String mMailHost										=	"smtp.gmail.com";

	private final String TYPE_TEXT									=	"text/html;charset=EUC-KR";
	private final String TYPE_VIDEO									=	"video/mp4;charset=UTF-8";
	
	private String type												=	TYPE_VIDEO;
	private Session mSession;
	

	public EmailClient(String user, String pwd)
	{
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", mMailHost);
		mSession = Session
			.getInstance(props, new EmailAuthenticator(user, pwd));
	} // constructor

	public void sendMail(String subject, String body, String sender, String recipients)
	{
		try {
			Message msg = new MimeMessage(mSession);
			msg.setFrom(new InternetAddress(sender));
			msg.setSubject(subject);
			msg.setContent(body, type);
			msg.setSentDate(new Date());
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
			Transport.send(msg);
		} catch (Exception e) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "Exception occured : ");
				Log.d(TAG, e.toString());
				Log.d(TAG, e.getMessage());
			}
		} // try-catch
	} // vodi sendMail
	
	public void sendMultipleMail(String subject, String body, String sender, InternetAddress[] recipientsList) {
		try {
			Message msg = new MimeMessage(mSession);
			msg.setFrom(new InternetAddress(sender));
			msg.setSubject(subject);
			msg.setContent(body, type);
			msg.setSentDate(new Date());
			msg.setRecipients(Message.RecipientType.TO, recipientsList);
			
			Transport.send(msg);
		} catch (Exception e) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "Exception occured : ");
				Log.d(TAG, e.toString());
				Log.d(TAG, e.getMessage());
			}
		} // try-catch
	}

	public void sendMailWithFile(String subject, String body, String sender,
			String recipients, String filePath, String fileName)
	{
		try {
			Message msg = new MimeMessage(mSession);
			msg.setFrom(new InternetAddress(sender));
			msg.setSubject(subject);
			msg.setContent(body, type);
			msg.setSentDate(new Date());
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(
					recipients));

			MimeBodyPart attachPart = new MimeBodyPart();
			attachPart.setDataHandler(new DataHandler(new FileDataSource(new File(filePath))));
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(attachPart);
			
			msg.setContent(multipart);
			msg.setFileName(fileName);
			
			Transport.send(msg);
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "sent");
			}
		} catch (Exception e) {
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "Exception occured : ");
				Log.d(TAG, e.toString());
				Log.d(TAG, e.getMessage());
			}
		} // try-catch
	} // void sendMailWithFile
	
	public void sendMultipleMailWithFile(String subject, String body, String sender,
			InternetAddress[] recipientsList, String filePath, String fileName)
	{
		try {
			Message msg = new MimeMessage(mSession);
			msg.setFrom(new InternetAddress(sender));
			msg.setSubject(subject);
			msg.setContent(body, type);
			msg.setSentDate(new Date());
			msg.setRecipients(Message.RecipientType.TO, recipientsList);

			MimeBodyPart attachPart = new MimeBodyPart();
			attachPart.setDataHandler(new DataHandler(new FileDataSource(new File(filePath))));
			attachPart.setFileName(fileName);
			
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(attachPart);
			
			msg.setContent(multipart);
			msg.setFileName(fileName);
			
			Transport.send(msg);
			Log.d("lastiverse", "sent");
		} catch (Exception e) {
			
			if(DEVELOPE_MODE) {
				Log.d("lastiverse", "Exception occured : ");
				Log.d("lastiverse", e.toString());
				Log.d("lastiverse", e.getMessage());
			}
		} // try-catch
	} // void sendMailWithFile

	class EmailAuthenticator extends Authenticator
	{
		private String id;
		private String pw;

		public EmailAuthenticator(String id, String pw)
		{
			super();
			this.id = id;
			this.pw = pw;
		} // constructor

		protected PasswordAuthentication getPasswordAuthentication()
		{
			return new PasswordAuthentication(id, pw);
		} // PasswordAuthentication getPasswordAuthentication
	} // class EmailAuthenticator
}
