package madcat.safereturn.user;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class UserDefault1st extends Activity {

//	private static final String TAG = "UserDefaultFirst";
	
	private EditText mEditName;
	private ImageButton mNextBtn;
	private Context mContext;
	private SharedPreferences mTempPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		Log.d(TAG, "onCreate 호출");
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_default_1st);
		
		mContext = this;
		mTempPref = getSharedPreferences(Constants.TEMP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		if(mTempPref.getString("TEMP_NAME", "").length() == 0) {
			Toast.makeText(mContext, "사용자 성함을 입력해 주세요.", Toast.LENGTH_SHORT).show();
		}
		
		mEditName = (EditText)findViewById(R.id.default_first_name);
		mNextBtn = (ImageButton)findViewById(R.id.default_first_btn_next);
		
		mEditName.setText(mTempPref.getString("TEMP_NAME", ""));		// 일단 사용자 이름을 temp 설정에서 읽어온다음
		
		mNextBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(mEditName.getText().length() != 0) {
					SharedPreferences.Editor tempPrefEditor = mTempPref.edit();
					tempPrefEditor.putString("TEMP_NAME", mEditName.getText().toString());		// temp 설정에 사용자 이름을 일단 저장
					tempPrefEditor.commit();
					
					Intent intent = new Intent(UserDefault1st.this, UserDefault2nd.class);
					startActivity(intent);
					overridePendingTransition(R.anim.fade, R.anim.hold);
					finish();
				} else {
					Toast.makeText(mContext, "이름을 입력해 주세요.", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		finish();
		android.os.Process.killProcess(android.os.Process.myPid());
	}
}
