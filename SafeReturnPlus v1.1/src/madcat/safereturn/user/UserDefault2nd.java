package madcat.safereturn.user;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class UserDefault2nd extends Activity {
	
//	private static final String TAG = "user_default_second";
	
	private ImageButton mBtnPrev, mBtnNext;
	private EditText mEditEmergency;
	private SharedPreferences mTempPref;
	private Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_default_2nd);
		
		mContext = this;
		
		mBtnPrev = (ImageButton)findViewById(R.id.default_second_btn_prev);
		mBtnNext = (ImageButton)findViewById(R.id.default_second_btn_next);
		mEditEmergency = (EditText)findViewById(R.id.default_second_emg);
		mTempPref = getSharedPreferences(Constants.TEMP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		emergencyInit();
		
		mBtnPrev.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				SharedPreferences.Editor tempPrefEditor = mTempPref.edit();
				tempPrefEditor.putString("TEMP_EMG_MSG", mEditEmergency.getText().toString());
				tempPrefEditor.commit();
				
				Intent intent = new Intent(UserDefault2nd.this, UserDefault1st.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.fade_out);
				finish();
			}
		});
		
		mBtnNext.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(mEditEmergency.getText().toString().length() != 0) {
					SharedPreferences.Editor tempPrefEditor = mTempPref.edit();
					tempPrefEditor.putString("TEMP_EMG_MSG", mEditEmergency.getText().toString());
					tempPrefEditor.commit();
					
					Intent intent = new Intent(UserDefault2nd.this, UserDefault3rd.class);
					startActivity(intent);
					overridePendingTransition(R.anim.fade, R.anim.hold);
					finish();
				} else {
					Toast.makeText(mContext, "긴급상황 발생시 알림 메시지를 입력해주세요.", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	private void emergencyInit() {
		mEditEmergency.setText(mTempPref.getString("TEMP_EMG_MSG", Constants.DEFAULT_EMERGENCY_MESSAGE));
	}
	
	@Override
	public void onBackPressed() {}
}
