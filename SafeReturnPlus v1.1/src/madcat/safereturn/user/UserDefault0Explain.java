package madcat.safereturn.user;

import madcat.safereturn.pro.R;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

public class UserDefault0Explain extends Activity {

//	private static final String TAG = "UserDefaultExplain";
	
	private RelativeLayout mainLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		Log.d(TAG, "onCreate ȣ��");
		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_default_0_explain);
		
		mainLayout = (RelativeLayout)findViewById(R.id.default_explain_layout);
		
		mainLayout.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(UserDefault0Explain.this, UserDefault1st.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				finish();
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		finish();
		android.os.Process.killProcess(android.os.Process.myPid());
	}
}
