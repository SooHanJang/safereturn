package madcat.safereturn.user;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.SettingSensorDialog;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

public class UserDefault3rd extends Activity {
	
	private SeekBar mSeekBar;
	private TextView mSeekText;
	private ImageButton mBtnPrev, mBtnNext, mBtnTest;
	private int mThresHold, mTempThresHold;
	private SharedPreferences mTempPref;
	private Context mContext;
	
	private static final int View_TEST_FLAG = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_default_3rd);
		
		mContext = this;
		
		mSeekText = (TextView)findViewById(R.id.user_third_sensor_viewer);
		mSeekBar = (SeekBar)findViewById(R.id.user_third_seekbar);
		mBtnTest = (ImageButton)findViewById(R.id.user_third_sensor_test);
		mBtnPrev = (ImageButton)findViewById(R.id.default_third_btn_prev);
		mBtnNext = (ImageButton)findViewById(R.id.default_third_btn_next);
		mTempPref = getSharedPreferences(Constants.TEMP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		sensorInit();
		
		mBtnPrev.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				SharedPreferences.Editor tempPrefEditor = mTempPref.edit();
				tempPrefEditor.putInt("TEMP_THRESHOLD", mTempThresHold);
				tempPrefEditor.commit();
				
				Intent intent = new Intent(UserDefault3rd.this, UserDefault2nd.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.fade_out);
				finish();
			}
		});
		
		mBtnNext.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				SharedPreferences.Editor tempPrefEditor = mTempPref.edit();
				tempPrefEditor.putInt("TEMP_THRESHOLD", mTempThresHold);
				tempPrefEditor.commit();
				
				Intent intent = new Intent(UserDefault3rd.this, UserDefault4th.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				finish();
			}
		});
	}
	
	private void sensorInit() {
		// Sensor Seekbar init
		mThresHold = mTempPref.getInt("TEMP_THRESHOLD", Constants.DEFAULT_USER_THRESHOLD);
		mTempThresHold = mThresHold;
		
		mSeekBar.setMax(30);
		mSeekBar.setProgress((mThresHold-1500)/100 * 2);
		
		mSeekText.setText(String.valueOf(mThresHold));
		
		mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			public void onStopTrackingTouch(SeekBar seekBar) {}
			public void onStartTrackingTouch(SeekBar seekBar) {}
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				mTempThresHold = (progress*50)+1500;
				mSeekText.setText(String.valueOf((progress*50)+1500));
			}
		});
		
		mBtnTest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				SettingSensorDialog emergencySensorDialog = new SettingSensorDialog(mContext);
				emergencySensorDialog.setThresHold(mTempThresHold);
				emergencySensorDialog.showTestingDialog(View_TEST_FLAG);
				emergencySensorDialog.show();
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {}	
}
