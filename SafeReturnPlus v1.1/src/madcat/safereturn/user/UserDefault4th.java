package madcat.safereturn.user;

import java.util.ArrayList;

import madcat.safereturn.pro.R;
import madcat.safereturn.adapter.EmergencyAddress;
import madcat.safereturn.adapter.EmergencyAddressAdapter;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.EmergencyAddressDialog;
import madcat.safereturn.pro.MainMenu;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Toast;

public class UserDefault4th extends ListActivity {
	
	private Context mContext;
	private ArrayList<EmergencyAddress> mOrders;
	private EmergencyAddressAdapter mAdapter;
	
	private ImageButton mBtnPrev, mBtnAdd, mBtnFinish;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_default_4th);
		
		mContext = this;
		mOrders = Utils.getEmergencyFriendsList();
		
		mBtnPrev = (ImageButton)findViewById(R.id.default_fourth_btn_prev);
		mBtnAdd = (ImageButton)findViewById(R.id.default_btn_fourth_add);
		mBtnFinish = (ImageButton)findViewById(R.id.default_fourth_btn_confirm);
		
		if(mOrders.size() == 0) {
			Toast.makeText(mContext, "긴급 메시지를 받을 사람을 최소한 한명 이상 등록해 주세요.", Toast.LENGTH_SHORT).show();
		}
		
		mAdapter = new EmergencyAddressAdapter(getApplicationContext(), R.layout.user_listview_row, mOrders);
		mAdapter.setTouchable(false);
		setListAdapter(mAdapter);
		
		mBtnPrev.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(UserDefault4th.this, UserDefault3rd.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.fade_out);
				finish();
			}
		});
		
		mBtnAdd.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				showFriendsDialog(mContext);
				
				final EmergencyAddressDialog addAddressDialog = new EmergencyAddressDialog(mContext, R.style.PopupDialog, EmergencyAddressDialog.SHOW_ADD_DIALOG);
				addAddressDialog.show();
				addAddressDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(addAddressDialog.getConfirmFlag()) {		// click Ok button
							Utils.insertDBFriends(addAddressDialog.getAddressName(), addAddressDialog.getAddressNumber());
							refreshFriendsList();
						} 
					}
				});
			}
		});
		
		mBtnFinish.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(mOrders.size() != 0) {
					SharedPreferences tempPref = getSharedPreferences(Constants.TEMP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
					SharedPreferences pref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
					SharedPreferences.Editor prefEditor = pref.edit();
					prefEditor.putBoolean(Constants.APP_FIRST, true);
					prefEditor.putBoolean(Constants.APP_UPDATE_1, true);
					prefEditor.putString(Constants.SETTING_USER_NAME, tempPref.getString("TEMP_NAME", ""));
					prefEditor.putInt(Constants.SETTING_USER_THRESHOLD, tempPref.getInt("TEMP_THRESHOLD", 0));
					prefEditor.putString(Constants.SETTING_EMERGENCY_MESSAGE, tempPref.getString("TEMP_EMG_MSG", ""));
					prefEditor.putString(Constants.SETTING_EMERGENCY_TEL, mOrders.get(0).getFriendsTel());
					
					prefEditor.commit();
					
					// 사용자의 시스템에서 사전에 메일을 읽어와 등록한다.
					if(Utils.getAccountInfo(mContext) != null) {
						Utils.insertDbEmail(tempPref.getString("TEMP_NAME", ""), Utils.getAccountInfo(mContext));
					}
					
					Intent intent = new Intent(UserDefault4th.this, MainMenu.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					overridePendingTransition(R.anim.fade, R.anim.hold);
					finish();
				} else {
					Toast.makeText(mContext, "긴급 메시지를 받을 사람을 최소한 한명 이상 등록해 주세요.", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private void refreshFriendsList() {
		mAdapter.clear();
		mOrders = Utils.getEmergencyFriendsList();
		mAdapter = new EmergencyAddressAdapter(getApplicationContext(), R.layout.user_listview_row, mOrders);
		mAdapter.setTouchable(false);
		setListAdapter(mAdapter);
		
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {}
}