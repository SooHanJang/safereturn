package madcat.safereturn.widget;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.WidgetPathDialog;
import madcat.safereturn.menu.EmergencyMode;
import madcat.safereturn.menu.StartReturn;
import madcat.safereturn.pro.Loading;
import madcat.safereturn.utils.MessagePool;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

public class WidgetSafeReturn extends AppWidgetProvider {
	
	private final String TAG										=	"WidgetSafeReturn";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	public static final int WIDGET_STATE_DEFAULT					=	0;
	public static final int WIDGET_STATE_START_NEW					=	1;
	public static final int WIDGET_STATE_START_CHOICE				=	2;
	public static final int WIDGET_STATE_START_EMERGENCY			=	3;
	public static final int WIDGET_STATE_START_RETURNING			=	4;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "받은 액션 : " + intent.getAction());
		}
		
		if(intent.getAction().equals(Constants.ACTION_WIDGET_STATE_UPDATE) ||
				intent.getAction().equals(Constants.ACTION_WIDGET_SAFE_STATE_CHANGE)) {
			
			
			updateWidget(context);
		}
	}
	
	private void updateWidget(Context context) {
		ComponentName thisAppWidget = new ComponentName(context.getPackageName(), WidgetSafeReturn.class.getName());
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
		
		onUpdate(context, appWidgetManager, appWidgetIds);
	}

	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		for(int i=0; i < appWidgetIds.length; i++) {
			int appwidgetId = appWidgetIds[i];
			updateAppWidget(context, appWidgetManager, appwidgetId);
		}
	}

	public void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
		RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.widget_safe_return);
		
		MessagePool messagePool = (MessagePool)context.getApplicationContext();
		
		int widgetState = messagePool.getCurrentWidgetState();
		String safeState = messagePool.getCurrentSafeState();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "현재 안전 귀가 상태 : " + widgetState);
			Log.d(TAG, "현재 안전도 상태 : " + messagePool.getCurrentSafeState());
		}
		
		// 텍스트 설정
		switch(widgetState) {
			case 0:
				// 안전도 텍스트 표시 설정
				updateViews.setViewVisibility(R.id.widget_text_layout, View.GONE);
				
				break;
			case 1:
				// 안전도 텍스트 표시 설정
				updateViews.setTextViewText(R.id.widget_safe_text, "안전 귀가 설정 중");
				updateViews.setTextColor(R.id.widget_safe_text, Color.BLACK);
				updateViews.setViewVisibility(R.id.widget_safe_text_prefix, View.GONE);
				updateViews.setViewVisibility(R.id.widget_text_layout, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_safe_text_postfix, View.GONE);
				
				break;
			
			case 2:
			case 3:
			case 4:
				if(safeState == null || safeState.equals("init")) {
					updateViews.setViewVisibility(R.id.widget_safe_text_prefix, View.GONE);
					updateViews.setTextViewText(R.id.widget_safe_text, "현재 위치 확인 중");
					updateViews.setTextColor(R.id.widget_safe_text, Color.BLACK);
					updateViews.setViewVisibility(R.id.widget_safe_text_postfix, View.GONE);
				} else {
					updateViews.setViewVisibility(R.id.widget_safe_text_prefix, View.VISIBLE);
					
					if(safeState.equals("안전 지역")) {
						updateViews.setTextColor(R.id.widget_safe_text, Color.BLUE);
					} else if(safeState.equals("일반 지역")) {
						updateViews.setTextColor(R.id.widget_safe_text, Color.GREEN);
					} else if(safeState.equals("주의 지역")) {
						updateViews.setTextColor(R.id.widget_safe_text, Color.MAGENTA);
					} else if(safeState.equals("위험 지역")) {
						updateViews.setTextColor(R.id.widget_safe_text, Color.RED);
					}
					
					updateViews.setViewVisibility(R.id.widget_safe_text_postfix, View.VISIBLE);
					
					updateViews.setTextViewText(R.id.widget_safe_text, safeState);
				}
				
				
				updateViews.setViewVisibility(R.id.widget_text_layout, View.VISIBLE);
				
				break;
		}
		
		// 아이콘 설정
		switch(widgetState) {
		
			case 0:		//	Default
				// 위젯 아이콘 표시 설정
				updateViews.setViewVisibility(R.id.widget_btn_safe_new, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_btn_safe_choice_d, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_btn_safe_emergency, View.VISIBLE);
				
				// 위젯 마진 표시 설정
				updateViews.setViewVisibility(R.id.widget_margin_first, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_margin_second, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_margin_third, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_margin_forth, View.VISIBLE);
				
				break;
				
				
			case 1:		//	Only START NEW
				// 위젯 아이콘 표시 설정
				updateViews.setViewVisibility(R.id.widget_btn_safe_new, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_btn_safe_choice_d, View.GONE);
				updateViews.setViewVisibility(R.id.widget_btn_safe_emergency, View.GONE);
				
				// 위젯 마진 표시 설정
				updateViews.setViewVisibility(R.id.widget_margin_first, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_margin_second, View.GONE);
				updateViews.setViewVisibility(R.id.widget_margin_third, View.GONE);
				updateViews.setViewVisibility(R.id.widget_margin_forth, View.GONE);
				
				break;
				
				
			case 2:		//	Only START CHOICE
				// 위젯 아이콘 표시 설정
				updateViews.setViewVisibility(R.id.widget_btn_safe_new, View.GONE);
				updateViews.setViewVisibility(R.id.widget_btn_safe_choice_d, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_btn_safe_emergency, View.GONE);
				
				// 위젯 마진 표시 설정
				updateViews.setViewVisibility(R.id.widget_margin_first, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_margin_second, View.GONE);
				updateViews.setViewVisibility(R.id.widget_margin_third, View.GONE);
				updateViews.setViewVisibility(R.id.widget_margin_forth, View.GONE);
				
				
				break;
				
				
			case 3:		//	Only START EMERGENCY
				// 위젯 아이콘 표시 설정
				updateViews.setViewVisibility(R.id.widget_btn_safe_new, View.GONE);
				updateViews.setViewVisibility(R.id.widget_btn_safe_choice_d, View.GONE);
				updateViews.setViewVisibility(R.id.widget_btn_safe_emergency, View.VISIBLE);
				
				// 위젯 마진 표시 설정
				updateViews.setViewVisibility(R.id.widget_margin_first, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_margin_second, View.GONE);
				updateViews.setViewVisibility(R.id.widget_margin_third, View.GONE);
				updateViews.setViewVisibility(R.id.widget_margin_forth, View.GONE);
				
				break;
				
				
			case 4:		//	Only START RETURNING
				// 위젯 아이콘 표시 설정
				updateViews.setViewVisibility(R.id.widget_btn_safe_new, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_btn_safe_choice_d, View.GONE);
				updateViews.setViewVisibility(R.id.widget_btn_safe_emergency, View.GONE);
				
				// 위젯 마진 표시 설정
				updateViews.setViewVisibility(R.id.widget_margin_first, View.VISIBLE);
				updateViews.setViewVisibility(R.id.widget_margin_second, View.GONE);
				updateViews.setViewVisibility(R.id.widget_margin_third, View.GONE);
				updateViews.setViewVisibility(R.id.widget_margin_forth, View.GONE);
				
				break;
		}
		
		Intent returnIntent = new Intent(context, StartReturn.class);
		returnIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK 
				   | Intent.FLAG_ACTIVITY_CLEAR_TOP 
   				   | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		updateViews.setOnClickPendingIntent(R.id.widget_btn_safe_new, 
				PendingIntent.getActivity(context, 0, returnIntent, PendingIntent.FLAG_UPDATE_CURRENT));
		
		
		Intent choiceIntent = new Intent(context, WidgetPathDialog.class);
		updateViews.setOnClickPendingIntent(R.id.widget_btn_safe_choice_d, 
		PendingIntent.getActivity(context, 0, choiceIntent, PendingIntent.FLAG_UPDATE_CURRENT));
		
		Intent emergencyIntent = new Intent(context, EmergencyMode.class);
		emergencyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK 
				   | Intent.FLAG_ACTIVITY_CLEAR_TOP 
   				   | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		updateViews.setOnClickPendingIntent(R.id.widget_btn_safe_emergency, 
				PendingIntent.getActivity(context, 0, emergencyIntent, PendingIntent.FLAG_UPDATE_CURRENT));
		
		
		appWidgetManager.updateAppWidget(appWidgetId, updateViews);
	}
}
