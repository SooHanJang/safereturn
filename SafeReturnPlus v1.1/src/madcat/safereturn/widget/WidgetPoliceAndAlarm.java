package madcat.safereturn.widget;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.WidgetSendSmsDialog;
import madcat.safereturn.service.FakeCallAlarmService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.RemoteViews;

public class WidgetPoliceAndAlarm extends AppWidgetProvider {
	
	private final String TAG										=	"EmergencyWidget";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "받은 액션 : " + intent.getAction());
		}
	}
	
	private void updateWidget(Context context) {
		ComponentName thisAppWidget = new ComponentName(context.getPackageName(), WidgetSafeReturn.class.getName());
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
		
		onUpdate(context, appWidgetManager, appWidgetIds);
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		for(int i=0; i < appWidgetIds.length; i++) {
			int appwidgetId = appWidgetIds[i];
			updateAppWidget(context, appWidgetManager, appwidgetId);
		}
	}

	public void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
		RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.widget_police_and_alarm);
		
		// 경찰 전화 설정
		Intent policeIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:02-112"));
		policeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		updateViews.setOnClickPendingIntent(R.id.widget_emergency_police_tel, 
				PendingIntent.getActivity(context, 0, policeIntent, PendingIntent.FLAG_UPDATE_CURRENT));
		
		
		// 긴급 메시지 알람 설정
		Intent sendMsgIntent = new Intent(context, WidgetSendSmsDialog.class);
		sendMsgIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		updateViews.setOnClickPendingIntent(
				R.id.widget_emergency_message, PendingIntent.getActivity(context, 0, sendMsgIntent, PendingIntent.FLAG_UPDATE_CURRENT));
		
		appWidgetManager.updateAppWidget(appWidgetId, updateViews);
	}
}
