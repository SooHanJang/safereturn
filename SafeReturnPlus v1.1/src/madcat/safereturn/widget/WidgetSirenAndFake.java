package madcat.safereturn.widget;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.service.FakeCallAlarmService;
import madcat.safereturn.service.SirenAlarmService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;

public class WidgetSirenAndFake extends AppWidgetProvider {

	private final String TAG										=	"WidgetSirenAndPolice";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "받은 액션 : " + intent.getAction());
		}
	}

	private void updateWidget(Context context) {
		ComponentName thisAppWidget = new ComponentName(context.getPackageName(), WidgetSafeReturn.class.getName());
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
		
		onUpdate(context, appWidgetManager, appWidgetIds);
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		for(int i=0; i < appWidgetIds.length; i++) {
			int appwidgetId = appWidgetIds[i];
			updateAppWidget(context, appWidgetManager, appwidgetId);
		}
	}

	public void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
		RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.widget_siren_and_fake);

		// 사이렌 설정
		Intent sirenIntent = new Intent(context, SirenAlarmService.class);
		sirenIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK 
				   		   | Intent.FLAG_ACTIVITY_CLEAR_TOP 
				   		   | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		updateViews.setOnClickPendingIntent(R.id.widget_emergency_siren, 
				PendingIntent.getActivity(context, 0, sirenIntent, PendingIntent.FLAG_UPDATE_CURRENT));
		
		// 페이크 전화 설정
		Intent fakeCallIntent = new Intent(context, FakeCallAlarmService.class);
		updateViews.setOnClickPendingIntent(R.id.widget_emergency_fake_call, 
				PendingIntent.getActivity(context, 0, fakeCallIntent, PendingIntent.FLAG_UPDATE_CURRENT));
		
		appWidgetManager.updateAppWidget(appWidgetId, updateViews);
	}
}
