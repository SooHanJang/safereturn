package madcat.safereturn.menu;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import madcat.safereturn.pro.R;
import madcat.safereturn.adapter.PathLocation;
import madcat.safereturn.blackbox.BBoxSendEmail;
import madcat.safereturn.blackbox.BCtrlService;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.AddressDetailView;
import madcat.safereturn.dialog.AddressModifyDialog;
import madcat.safereturn.dialog.AlertGpsDialog;
import madcat.safereturn.dialog.AppExitPopUpDialog;
import madcat.safereturn.dialog.MapInputDialog;
import madcat.safereturn.dialog.MapPopUpDialog;
import madcat.safereturn.dialog.MapSelectDialog;
import madcat.safereturn.dialog.TimeSetDialog;
import madcat.safereturn.geopoint.PathPaint;
import madcat.safereturn.geopoint.PathPoint;
import madcat.safereturn.geopoint.SavePathGeoPoint;
import madcat.safereturn.maps.CCTVOverlay;
import madcat.safereturn.maps.DestPinEventOverlay;
import madcat.safereturn.maps.DestPinOverlay;
import madcat.safereturn.maps.FireOverlay;
import madcat.safereturn.maps.MyOverlay;
import madcat.safereturn.maps.MyPathOverlay;
import madcat.safereturn.maps.PoliceOverlay;
import madcat.safereturn.maps.StartPinOverlay;
import madcat.safereturn.maps.StoreOverlay;
import madcat.safereturn.maps.ZoomOverlay;
import madcat.safereturn.service.MapReceiver;
import madcat.safereturn.service.NotificationSafeReturn;
import madcat.safereturn.service.SensorCallService;
import madcat.safereturn.service.WakeLockService;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.Utils;
import madcat.safereturn.widget.WidgetSafeReturn;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class StartReturn extends MapActivity implements OnClickListener, LocationListener {

	private static final String TAG										=	"StartReturn";
	private static final boolean DEVELOPE_MODE							=	Constants.DEVELOPE_MODE;
	
	private final int START_RETURN_FAIL									=	0;
	private final int START_RETURN_ONLY									=	1;
	private final int START_RETURN_WITH_BLACK_BOX						=	2;
	
	private Context mContext;
	private MessagePool mMessagePool;
	
	// Path 그리기 관련 변수
	private PathPaint mPaint;
	private PathPoint mP1, mP2;
	private ArrayList<SavePathGeoPoint> mArraySavePathGP;
	
	// 도착 위치 등록 관련 변수
	private String mStartAddress, mDestAddress;
	
	private MapView mMapView;
	private List<Overlay> mListOverlay;
	private Drawable mCCTVMarker, mPoliceMarker, mStoreMarker, mFireMarker;
	private MapController mMapController;
	private String mBestProvider;
	private LocationManager mLocManager;
	private Location mCurrentLocation;
	private MyOverlay mMyLocationOverlay;
	private CCTVOverlay mCCTVOverlay;
	private FireOverlay mFireOverlay;
	private StoreOverlay mStoreOverlay;
	private PoliceOverlay mPoliceOverlay;
	private StartPinOverlay mStartPinOverlay = null;
	private DestPinOverlay mDestPinOverlay = null;
	private DestPinEventOverlay mDestPinEventOverlay;
	
	private EditText mHourEditText, mMinEditText;
	private ImageButton mBtnLocSetting, mBtnReturnStart, mBtnSummarySet;
	private RelativeLayout mFullSettingLayout;
	private LinearLayout mSummarySettingLayout, mStartReturningLayout, mEmergencyLayout;
	private EditText mEditDest;
	private ImageButton mBtnCallPolice, mBtnSendSms;
	
	private SharedPreferences mConfigPref;
	
	private Intent mIntent;
	private PendingIntent mPending;
	private Intent mServiceIntent;
	
	private GeoPoint mStartGeoPoint, mDestGeoPoint;
	private ArrayList<PathLocation> pathLocation;
	
	private int mDestHour, mDestMin;
	private int mTotalSecond;
	private String mSafeCheck = "init";
	
	// 반경 관련 변수
	private int mRangeValue;
	private boolean mCCTVOverlayState, mPoliceOverlayState, mStoreOverlayState, mFireOverlayState;
	
//	private ArrayList<Integer> compareFavoriteArray;
	
	private NotificationSafeReturn mSafeNotification;
	private int mCCTVNumber, mPoliceNumber, mStoreNumber, mFireNumber;
	private String mClosePoliceTel;

	private Thread mRangeTimerThread;
	private int mRangeTimerCount = 0;
	private int mOutOfRangeCount = 0;
	private Handler mRangeTimerHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if(mMessagePool.getWarningTimerFlag() != false) {		// 경고 타이머가 작동되고 있는 상태에서
				if(DEVELOPE_MODE) {
					Log.d(TAG, "경로 이탈 체크 시간 : " + mRangeTimerCount);
				}
				
				if(mRangeTimerCount >= Constants.WARNING_WAIT_TIME) {		// 경로를 이탈한지 2분이 지나면
					if((mRangeTimerCount % Constants.WARNING_WAIT_TIME)%60 == 0) {
						// 경로 이탈 후 1분마다 지속적으로 메시지를 알림
						mOutOfRangeCount++;
						Toast.makeText(mContext, "경로를 이탈한지 " + mOutOfRangeCount + "분이 지남", Toast.LENGTH_SHORT).show();
						
						String helpMsg = mUserName + "님이 귀가 중 경로를 이탈한지  " + mOutOfRangeCount + "분 지났습니다." 
												+ " 안전을 위해 빠른 확인 부탁드립니다. (현재위치 : " + mMyAddress + ")";
						String locMsg = null;
						
						if(mCurrentLocation != null) {
							locMsg = "(위치 확인 : http://maps.google.co.kr/maps?q=" +
								mCurrentLocation.getLatitude() + "+" + mCurrentLocation.getLongitude() + ")";
						} 
						
						Utils.sendMMS(mContext, helpMsg, locMsg);
					}
				}
			} 
		}
	};

	private Thread mReturningTimerThread;
	private int mReturningTimerCount = 0;
	private int mOutOfTimeCount = 0;
	private Handler mReturningTimerHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if(mMessagePool.getReturningTimerFlag() != false) {
				if(DEVELOPE_MODE) {
					Log.d(TAG, "귀가 초과 체크 시간 : " + mReturningTimerCount + ", 최종 도착 시간 (초) : " + mTotalSecond);
				}
				
				
				if(mReturningTimerCount >= mTotalSecond) {			// 귀가 예상 시간을 벗어났다면
					if((mReturningTimerCount%mTotalSecond)%Constants.WARNING_WAIT_TIME == 0) {
						mOutOfTimeCount++;
						Toast.makeText(mContext, "예상 도착 시간을 벗어난지 " + mOutOfTimeCount + "분이 지남", Toast.LENGTH_SHORT).show();
						
						String helpMsg = mUserName + "님이 예상 귀가 시간을 초과한지 " + mOutOfRangeCount + "분 지났습니다." 
												+ " 안전을 위해 빠른 확인 부탁드립니다. (현재위치 : " + mMyAddress + ")";
						String locMsg = null;
						
						if(mCurrentLocation != null) {
							locMsg = "(위치 확인 : http://maps.google.co.kr/maps?q=" +
								mCurrentLocation.getLatitude() + "+" + mCurrentLocation.getLongitude() + ")";
						}
						
						Utils.sendMMS(mContext, helpMsg, locMsg);
					}
				}
			}
		}
	};
	
	
	private Thread mStandTimerThrad;
	private int mStandTimerCount = 0;
	private int mStandOutTimeCount = 0;
	private Handler mStandTimerHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if(mMessagePool.getReturningFlag() != false) {
				if(DEVELOPE_MODE) {
					Log.d(TAG, "한 곳에 머무른지 체크 시간 : " + mStandTimerCount);
				}
				
				if(mStandTimerCount >= Constants.WARNING_WAIT_TIME) {
					if((mStandTimerCount % Constants.WARNING_WAIT_TIME)%60 == 0) {
						mStandOutTimeCount++;
						Toast.makeText(mContext, "한 곳에 머무른지 " + mStandOutTimeCount + "분이 지남", Toast.LENGTH_SHORT).show();
						
						String helpMsg = mUserName + "님이 귀가 중 이동을 안한지 " + mStandOutTimeCount + "분 지났습니다." 
											+ " 안전을 위해 빠른 확인 부탁드립니다. (현재위치 : " + mMyAddress + ")";
						String locMsg = null;
						
						if(mCurrentLocation != null) {
							locMsg = "(위치 확인 : http://maps.google.co.kr/maps?q=" 
											+ mCurrentLocation.getLatitude() + "+" + mCurrentLocation.getLongitude() + ")";
						}
						
						Utils.sendMMS(mContext, helpMsg, locMsg);
					}
				}
			}
		}
	};
	
	private Thread mPoliceTelThread;
	private Thread mMyAddressThread;
	
	private String mUserName;
	private String mMyAddress = "";
	
	private Thread mThreadPoliceCheck, mThreadEmergencyCheck;
	private boolean mPoliceBtnFlag									=	false;
	private boolean mEmergencyBtnFlag								=	false;
	
	private int mCallWidgetState									=	WidgetSafeReturn.WIDGET_STATE_START_NEW;
	
	// Provider 관련 변수
	private Thread mProviderThread;
	private boolean mProviderFlag;
	
	// WakeLock 관련 변수
	private WakeLockService mServiceWakeLock;
	
	// BlackBox 관련 변수
	private Intent mBServiceIntent;
	
	// 위급 상황 취소 버튼
	private Button mBtnEmergencyCancel;
	
	//TestCode
	private TextView mTextTest;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	if(DEVELOPE_MODE) {
    		Log.d(TAG, "onCreate 호출");
    	}
    	
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.start_safe_return);
        
        mContext = this;
        mMessagePool = (MessagePool)mContext.getApplicationContext();
        mCCTVNumber = 0;
        mPoliceNumber = 0;
        
        // MessagePool 초기 설정
        mMessagePool.setContext(mContext);
        mMessagePool.setReturningTimerFlag(false);							// 귀가 타이머 플래그를 false 로 초기화
        
        // 사용자 이름 설정
        mConfigPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        mUserName = mConfigPref.getString(Constants.SETTING_USER_NAME, "");
        
        // Notification 객체 생성
        mSafeNotification = NotificationSafeReturn.getInstance();
        mSafeNotification.setContext(mContext);
        
        // Map 관련 초기화
        mMapView = (MapView)findViewById(R.id.map_view);
        mMapView.setSatellite(false);													// 위성으로 표시 안하도록 설정
		mMapController = mMapView.getController();										// 맵 컨트롤러를 가져온다.
	    mMapController.setZoom(Constants.MAP_ZOOM_LEVEL);								// 초기 맵 확대는 17정도로 설정
	    
	    mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);		// GPS를 관리하는 LocationManager 설정
	    mBestProvider = mLocManager.getBestProvider(Utils.setCriteria(), true);
	    
	    if(mBestProvider != null) {
	    	mCurrentLocation = mLocManager.getLastKnownLocation(mBestProvider);
	    }
	    
        mListOverlay = mMapView.getOverlays();
        
        // 위젯 관련 초기화
        mMessagePool.setCurrentWidgetState(mCallWidgetState);
        
        if(DEVELOPE_MODE) {
        	Log.d(TAG, "현재 Provider : " + mBestProvider + ", 현재 위치 : " + mCurrentLocation);
        	Toast.makeText(mContext, "현재 Provider : " + mBestProvider + ", 현재 위치 : " + mCurrentLocation, Toast.LENGTH_LONG).show();
        }
        
        // Path 그리기 초기화
        initPathPaint();
        mArraySavePathGP = new ArrayList<SavePathGeoPoint>();
        
        // 사용자 반경 설정
        mRangeValue = mConfigPref.getInt(Constants.SETTING_RANGE_VALUE, Constants.DEFAULT_USER_RANGE);
        
        // 오버레이 표시 상태 설정
	    mCCTVOverlayState = mConfigPref.getBoolean(Constants.SETTING_CCTV_OVERLAY_STATE, true);
	    mPoliceOverlayState = mConfigPref.getBoolean(Constants.SETTING_POLICE_OVERLAY_STATE, true);
	    mStoreOverlayState = mConfigPref.getBoolean(Constants.SETTING_STORE_OVERLAY_STATE, true);
	    mFireOverlayState = mConfigPref.getBoolean(Constants.SETTING_FIRE_OVERLAY_STATE, true);
        
        // 위치 설정 Layout 관련 초기화
        mHourEditText = (EditText)findViewById(R.id.start_edit_hour);
		mMinEditText = (EditText)findViewById(R.id.start_edit_min);
		mBtnLocSetting = (ImageButton)findViewById(R.id.start_setdest_btn);
		mBtnReturnStart = (ImageButton)findViewById(R.id.start_btn);
		mBtnSummarySet = (ImageButton)findViewById(R.id.btn_summary_set);
		
		mBtnCallPolice = (ImageButton)findViewById(R.id.start_btn_emergency_call_police);
		mBtnSendSms = (ImageButton)findViewById(R.id.start_btn_emergency_send_sms);
		
		mBtnEmergencyCancel = (Button)findViewById(R.id.start_btn_emergency_cancel);
		
		mFullSettingLayout = (RelativeLayout)findViewById(R.id.start_relative);
		mSummarySettingLayout = (LinearLayout)findViewById(R.id.setting_summary_layout);
		mEmergencyLayout = (LinearLayout)findViewById(R.id.start_emergency_linear);
		mEditDest = (EditText)findViewById(R.id.start_edit_dest);
		mEditDest.setInputType(0);
		
		// 다른 외부에서 레이아웃을 컨트롤 할 수 있게 메시지 풀에 해당 레이아웃을 등록
		mMessagePool.setFullLayout(mFullSettingLayout);
		mMessagePool.setSummaryLayout(mSummarySettingLayout);
		mMessagePool.setEmergencyLayout(mEmergencyLayout);
		mMessagePool.setEditDest(mEditDest);
		mMessagePool.setEditHourText(mHourEditText);
		mMessagePool.setEditMinText(mMinEditText);
		mMessagePool.setRange(mRangeValue);
		mMessagePool.setBtnEmergencyCancel(mBtnEmergencyCancel);
		
		// 기본 Pin 마크 관련 플래그 초기화
		mMessagePool.setFavoriteFlag(false);
		mMessagePool.setDestFlag(false);
		
		//버튼에 onClicke EventListenr 를 등록한다.
		mBtnLocSetting.setOnClickListener(this);
		mBtnReturnStart.setOnClickListener(this);
		mBtnSummarySet.setOnClickListener(this);
		mEditDest.setOnClickListener(this);
		mMinEditText.setOnClickListener(this);
		mHourEditText.setOnClickListener(this);
		mBtnCallPolice.setOnClickListener(this);
		mBtnSendSms.setOnClickListener(this);
		mBtnEmergencyCancel.setOnClickListener(this);
		
	    // MapView 의 ZoomLayout 을 설정하고, 미리 생성한 맵 컨트롤러를 가져와서 설정한 레이아웃에 적용하기 위한 설정 부분
	    LinearLayout zoomLayout = (LinearLayout)findViewById(R.id.layout_zoom);
	    View zoomView = mMapView.getZoomControls();
        zoomLayout.addView(zoomView, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        mMapView.displayZoomControls(true);
        
        // 내 위치를 mapView에 표시 부분
        mMyLocationOverlay = new MyOverlay(mContext, mMapView, mRangeValue);
        mMyLocationOverlay.enableMyLocation();
        
        mMyLocationOverlay.runOnFirstFix(new Runnable() {
    		public void run() {
    			mMapView.getController().animateTo(mMyLocationOverlay.getMyLocation());
 			}
 		});
         
        mMapView.getOverlays().add(mMyLocationOverlay);
        
        // 도착지 Pin 위치 관련 초기화
        mDestPinEventOverlay = new DestPinEventOverlay(mContext);
        
        // Receiver 에 Broadcasting 을 위한 Receiver 설정 초기화
        mIntent = new Intent(this, MapReceiver.class);
		mPending = PendingIntent.getBroadcast(this, 1, mIntent, 0);
		
		// MessagePool 에 사용을 위한 pendingIntent 와 locationManger 를 등록
		mMessagePool.setDestPendingIntent(mPending);
		mMessagePool.setLocationManger(mLocManager);
		
		 // Marker 설정
	    if(mCCTVOverlayState) {
	        mCCTVMarker = getResources().getDrawable(R.drawable.pin_marker_cctv);
	        mCCTVOverlay = new CCTVOverlay(mCCTVMarker, mContext);
	    }
        
	    if(mPoliceOverlayState) {
	        mPoliceMarker = getResources().getDrawable(R.drawable.pin_marker_police);
	        mPoliceOverlay = new PoliceOverlay(mPoliceMarker, mContext);
	    }
        
	    if(mStoreOverlayState) {
	    	mStoreMarker = getResources().getDrawable(R.drawable.pin_marker_store);
	        mStoreOverlay = new StoreOverlay(mStoreMarker, mContext);
	    }
        
	    if(mFireOverlayState) {
	        mFireMarker = getResources().getDrawable(R.drawable.pin_marker_fire);
	        mFireOverlay = new FireOverlay(mFireMarker, mContext);
	    }

        // MapView 시작 시 사전에 미리 그려줄 Overlay 들 초기화
        mListOverlay.add(mDestPinEventOverlay);						// 도착지 핀 이벤트를 처리할 overlay 추가 (LongClick, Press Click)
        mListOverlay.add(new ZoomOverlay(mContext, mMapView));
        
        // Test 부분
        if(DEVELOPE_MODE) {
        	mTextTest = (TextView)findViewById(R.id.text_speed_test);
        }
        
    	getIntenter();
    }
    
    @Override
    protected void onStart() {		
    	if(DEVELOPE_MODE) {
    		Log.d(TAG, "onStart 호출");
    	}
    	
    	super.onStart();
    	
    	mBestProvider = mLocManager.getBestProvider(Utils.setCriteria(), true);
    	
    	if(mBestProvider == null) {		// GPS 혹은 네트워크 위치 허용이 꺼져있다면
    		final AlertGpsDialog alertGpsDialog = new AlertGpsDialog(mContext, R.style.PopupDialog);
    		alertGpsDialog.show();
    		
    		alertGpsDialog.setOnDismissListener(new OnDismissListener() {
				public void onDismiss(DialogInterface dialog) {
					switch(alertGpsDialog.getDialogStateFlag()) {
						case 1:
							Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							startActivity(gpsOptionsIntent);
							overridePendingTransition(R.anim.fade, R.anim.hold);
//							finish();
							break;
						case 2:
//							finish();
							overridePendingTransition(R.anim.fade, R.anim.fade_out);
						default:
							break;
					}
					
					Utils.widgetStateUpdate(mContext, WidgetSafeReturn.WIDGET_STATE_DEFAULT);
					
					if(mServiceWakeLock != null) {
						mServiceWakeLock.releaseWakeLock();
					}
				}
			});
    	} else {
        	// GPS 변경 시 위치 업데이트 허용
        	mLocManager.requestLocationUpdates(mBestProvider, Constants.TIME_UPDATE_PERIOD, 
        			Constants.DISTANCE_UPDATE_PERIOD, this);
	        
    	}
    	
    	mCallWidgetState = mMessagePool.getCurrentWidgetState();
    	
    	switch(mCallWidgetState) {
	    	case WidgetSafeReturn.WIDGET_STATE_START_NEW:		//	안전 귀가 모드를 설정 중
	    		Utils.widgetStateUpdate(mContext, WidgetSafeReturn.WIDGET_STATE_START_NEW);
	    		break;
	    	case WidgetSafeReturn.WIDGET_STATE_START_RETURNING:		// 안전 귀가 모드가 작동 중
	    		Utils.widgetStateUpdate(mContext, WidgetSafeReturn.WIDGET_STATE_START_RETURNING);
	    		break;
    	}
    }
    
    @Override
    protected void onDestroy() {
    	if(mMessagePool.getAddProximityFlag() == true) {			// 도착지점에 addPoximitAlert 이 등록되었다면
    		mLocManager.removeProximityAlert(mPending);
    	}
    	
		mLocManager.removeUpdates(this);		// 사용자의 실시간 정보 위치를 받아들이는 requestUpdate() 를 제거
		mSafeNotification.removeNotification();				// 혹시나 Notificatino 이 등록되어있다면, 삭제해버린다.
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
    }
    
    private void initPathPaint() {
    	if(DEVELOPE_MODE) {
    		Log.d(TAG, "Paint 객체와 Point 객체 생성");
    	}
    	
    	mPaint = new PathPaint();
    	mP1 = new PathPoint();
    	mP2 = new PathPoint();
    	
    	mPaint.setDither(true);
		mPaint.setColor(Color.RED);
		mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		mPaint.setAntiAlias(true);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(4);
		mPaint.setAlpha(90);
    }
    
    private void updatePath(Location startLocation, Location endLocation) {
    	long startTime = System.currentTimeMillis();
    	
    	SavePathGeoPoint savePathGeoPoint = 
    		new SavePathGeoPoint((int)(startLocation.getLatitude()*1E6), (int)(startLocation.getLongitude()*1E6),
    				(int)(endLocation.getLatitude()*1E6), (int)(endLocation.getLongitude()*1E6));

    	mArraySavePathGP.add(savePathGeoPoint);
    	mMessagePool.setArraySavePathGeoPoint(mArraySavePathGP);
    	
    	// 내가 생성한 경로 Overlay 들을 MapView 에 추가
    	MyPathOverlay pathOverlay = new MyPathOverlay();
    	pathOverlay.setMyPathOverlay(savePathGeoPoint, mP1, mP2, mPaint);
    	mListOverlay.add(pathOverlay);

    	// 현재 위치를 기준으로 경찰서와 같은 위치 정보를 맵에 표시
    	updateOverlayData(endLocation);
    	
    	if(DEVELOPE_MODE) {
    		long endTime = System.currentTimeMillis();
    		Log.d(TAG, "updatePath(+updateOverlayData) 수행 속도 : " + (endTime - startTime) / 1000.0 + "초");
    	}
    }
    
    class AsyncUpdatePathAndOverlay extends AsyncTask<Location, Location, Void> {
    	long startTime = System.currentTimeMillis();
    	
    	@Override
    	protected Void doInBackground(Location... loc) {
    		publishProgress(loc);
    		return null;
    	}
    	
    	@Override
    	protected void onProgressUpdate(Location... loc) {
    		updatePath(loc[0], loc[1]);
    		long endTime = System.currentTimeMillis();
    		
    		if(DEVELOPE_MODE) {
    			mTextTest.setText("AsyncUpdatePath 속도 : " + (endTime - startTime)/1000.0 + "초\nOverlay 개수 : "+ (mListOverlay.size()-5));
    		}
    	}
    }
    
    class AsyncUpdateOnlyOverlay extends AsyncTask<Location, Location, Void> {
    	long startTime = System.currentTimeMillis();
    	
		@Override
		protected Void doInBackground(Location... loc) {
			publishProgress(loc);
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Location... loc) {
			updateOverlayData(loc[0]);
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(DEVELOPE_MODE) {
				long endTime = System.currentTimeMillis();
	    		Log.d(TAG, "AsyncUpdateMarkerOverlay 속도 : " + (endTime - startTime)/1000.0 + "초");
			}
		}
	}
    
    private void updateOverlayData(Location myLocation) {
    	// 화면에 표시된 Overlay 를 일단 삭제한다.
    	long startTime = System.currentTimeMillis();
    	
    	if(mCCTVOverlay != null) { 
    		mListOverlay.remove(mCCTVOverlay);
    		mCCTVOverlay.clearOverlay();
    	}
    	
    	if(mPoliceOverlay != null) { 
    		mListOverlay.remove(mPoliceOverlay);
    		mPoliceOverlay.clearOverlay();
    	}
    	
    	if(mStoreOverlay != null) {
    		mListOverlay.remove(mStoreOverlay);
    		mStoreOverlay.clearOverlay();
    	}
    	
    	if(mFireOverlay != null) { 
    		mListOverlay.remove(mFireOverlay);
    		mFireOverlay.clearOverlay();
    	}

    	HashMap<String, Overlay> dataItem = null;
    	
    	if(mCCTVOverlayState != false || mPoliceOverlayState != false || mStoreOverlayState != false || mFireOverlayState != false) { 
	    	dataItem = Utils.getBetweenOverlay(myLocation, mRangeValue, mCCTVOverlay, mPoliceOverlay, mStoreOverlay, mFireOverlay);
    	}
    	
    	if(mCCTVOverlayState) {
	    	mCCTVOverlay = (CCTVOverlay) dataItem.get("CCTV");
	    	mCCTVOverlay.overlayPopulate();
	    	mListOverlay.add(mCCTVOverlay);
	    	mCCTVNumber = mCCTVOverlay.size();
    	}
    	
    	if(mPoliceOverlayState) {
	    	mPoliceOverlay = (PoliceOverlay) dataItem.get("POLICE");
	    	mPoliceOverlay.overlayPopulate();
	    	mListOverlay.add(mPoliceOverlay);
	    	mPoliceNumber = mPoliceOverlay.size();
    	}
    	
    	if(mStoreOverlayState) {
	    	mStoreOverlay = (StoreOverlay) dataItem.get("STORE");
	    	mStoreOverlay.overlayPopulate();
	    	mListOverlay.add(mStoreOverlay);
	    	mStoreNumber = mStoreOverlay.size();
    	}
    	
    	if(mFireOverlayState) {
	    	mFireOverlay = (FireOverlay) dataItem.get("FIRE");
	    	mFireOverlay.overlayPopulate();
	    	mListOverlay.add(mFireOverlay);
	    	mFireNumber = mFireOverlay.size();
    	}
    	
    	mMapView.invalidate();
    	
    	String safeCheck = Utils.checkSafePercent(mCCTVNumber, mStoreNumber, mPoliceNumber, mFireNumber);
    	
    	if(mSafeCheck.equals(safeCheck)) {
    		mSafeNotification.updateNotification(safeCheck, mCCTVNumber, mStoreNumber, mPoliceNumber);
    	} else { 
    		Utils.widgetSafeUpdate(mContext, safeCheck);
    		
    		mSafeNotification.removeNotification();
    		mSafeNotification.registNotification();
    		mSafeNotification.updateNotification(safeCheck, mCCTVNumber, mStoreNumber, mPoliceNumber);
		}
    	
    	mSafeCheck = safeCheck;

    	if(DEVELOPE_MODE) {
	    	long endTime = System.currentTimeMillis();
	    	Log.d(TAG, "UpdateOverlay Data 속도 : " + (endTime - startTime) / 1000.0 + "초");
    	}
    }
	
	private void updatePoliceTel(Location myLocation) {
		Cursor cursor = Utils.getClosePoliceTel(myLocation);
		cursor.moveToFirst();

		if(myLocation != null) {
			mClosePoliceTel = cursor.getString(0);
			cursor.close();
		} else {
			mClosePoliceTel = "02-112";
		}
		
		mMessagePool.setClosePoliceTel(mClosePoliceTel);
	}
	
	private void showInputAddressDialog() {
		final MapInputDialog mapInputDialog = new MapInputDialog(mContext, R.style.PopupDialog);
		mapInputDialog.show();
		
		mapInputDialog.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				if(mapInputDialog.getDialogStateFlag() == 1) {
					mMapView.removeAllViews();		// 일단 map 위에 그려진 모든 View들을 지운 뒤
					
					GeoPoint myDestPoint = new GeoPoint((int)(mapInputDialog.getMyLat()*1E6), (int)(mapInputDialog.getMyLon()*1E6));
					
					mDestPinEventOverlay.createPinImage(myDestPoint, mMapView);
					mEditDest.setText(mapInputDialog.getMyAddr());
					
					mMessagePool.setGeoDest(myDestPoint);	// MessagePool 에 현재 좌표값을 일단 등록
					
				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.start_edit_min:
			final TimeSetDialog timeMinSetDialog = new TimeSetDialog(mContext, R.style.PopupDialog, 1);
			timeMinSetDialog.show();
			
			timeMinSetDialog.setOnDismissListener(new OnDismissListener() {
				public void onDismiss(DialogInterface dialog) {
					mMinEditText.setText(String.valueOf(timeMinSetDialog.getMin()));
				}
			});
			break;
		case R.id.start_edit_hour:
			final TimeSetDialog timeHourSetDialog = new TimeSetDialog(mContext, R.style.PopupDialog, 2);
			timeHourSetDialog.show();
			
			timeHourSetDialog.setOnDismissListener(new OnDismissListener() {
				public void onDismiss(DialogInterface dialog) {
					mHourEditText.setText(String.valueOf(timeHourSetDialog.getHour()));
				}
			});
			break;
		case R.id.start_setdest_btn:		// 위치 설정 클릭
			final MapPopUpDialog mapPopUpDialog = new MapPopUpDialog(mContext, R.style.PopupDialog);
			mapPopUpDialog.show();
			
			mapPopUpDialog.setOnDismissListener(new OnDismissListener() {
				public void onDismiss(DialogInterface dialog) {
					if(mapPopUpDialog.getDialogStateFlag() == 1) {
						showInputAddressDialog();
					} else if(mapPopUpDialog.getDialogStateFlag() == 2) {
						mFullSettingLayout.setVisibility(View.GONE);
						mSummarySettingLayout.setVisibility(View.VISIBLE);
						mMessagePool.setDestFlag(true);
					} else if(mapPopUpDialog.getDialogStateFlag() == 3) {
						showFavoritePathDialog();
					} 
				}
			});
			break;
		case R.id.start_btn:			// 안전 귀가 버튼 클릭
			// 롱 클릭을 금지시키고, 도착지 주소와, 예상시간이 설정되었는지 유무를 확인한다.
			// 도착지 값이 설정되었다면
			if(mEditDest.getText().length() != 0 && 
					(mHourEditText.getText().toString().length() != 0 || mMinEditText.getText().toString().length() != 0) && mCurrentLocation != null) {
				
				if(mHourEditText.getText().toString().length() == 0) { mHourEditText.setText("0"); }		// 시간이 공백이면 0을 강제 입력
				if(mMinEditText.getText().toString().length() == 0) { mMinEditText.setText("0"); }		// 분이 공백이면 0을 강제 입력
				
				int validTimeResult = Utils.validationDestTime(mHourEditText.getText().toString(), mMinEditText.getText().toString());
				if(validTimeResult == 1) {
					Toast.makeText(mContext, "도착 시간은 12시간을 초과할 수 없습니다.", Toast.LENGTH_SHORT).show();
				} else if(validTimeResult == 2) {
					Toast.makeText(mContext, "도착 분은 59분을 초과할 수 없습니다.", Toast.LENGTH_SHORT).show();
				} else if(validTimeResult == 3) {
					Toast.makeText(mContext, "도착 시간과 분을 선택해 주세요.", Toast.LENGTH_SHORT).show();
				} else if(validTimeResult == 4) {
					
					int startReturnType = START_RETURN_FAIL;

					// 블랙 박스 설정이 활성화 체크
					if(mConfigPref.getBoolean(Constants.SETTING_BBOX_ENABLE, true)) {
						
						// SD 카드 체크
						if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
							
							// 사용할 수 있는 최소 공간을 체크한다.
							StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
							long bytesAvailable = (long)stat.getFreeBlocks() * (long)stat.getBlockSize();
							boolean remainSpace = (int) (bytesAvailable / (1024 * 1000)) > (((mDestHour * 60) + mDestMin) * Constants.MIN_TO_MEGA) ? true : false;

							if(DEVELOPE_MODE) {
								Log.d(TAG, "현재 SDCard 남은 공간 : " + bytesAvailable);
								Log.d(TAG, "remainSpace (최소한 필요한 공간) : " + remainSpace);
							}
							
							if(remainSpace) {		//	여유 공간이 있다면 블랙 박스와 함께 안전 귀가 시작
								startReturnType = START_RETURN_WITH_BLACK_BOX;
							} else {				//	여유 공간이 부족했을 시, 경고 토스트 작동
								Toast.makeText(mContext, "블랙 박스를 저장할 용량이 부족합니다. SD 카드 내의 파일을 삭제해주세요", Toast.LENGTH_SHORT).show();
								startReturnType = START_RETURN_FAIL;
							}
						} else {					//	SD 카드가 없을 시, 경고 토스트 작동
							Toast.makeText(mContext, getString(R.string.toast_title_caution_sdcard), Toast.LENGTH_LONG).show();
							startReturnType = START_RETURN_FAIL;
						}
					} else {
						startReturnType = START_RETURN_ONLY;
					}
					
					
					if(startReturnType == START_RETURN_ONLY || startReturnType == START_RETURN_WITH_BLACK_BOX) {
						// 도착 예상 시간, 목적지를 MessagePool 에 등록
						mDestHour = Integer.parseInt(mHourEditText.getText().toString());
						mDestMin = Integer.parseInt(mMinEditText.getText().toString());
						mMessagePool.setDestHour(mDestHour);
						mMessagePool.setDestMin(mDestMin);
		
						mTotalSecond = Utils.timeToSecond(mDestHour, mDestMin);					// 도착 예상 시간을 초로 변환
						
						Utils.widgetStateUpdate(mContext, WidgetSafeReturn.WIDGET_STATE_START_RETURNING);
						
						mMessagePool.setReturningFlag(true); 								// 안전 귀가가 시작되었음을 알린다.
						mMessagePool.setLongClickFlag(false);								// 롱 클릭 금지
						
						
						// 사용자의 도착지 설정 레이아웃을 변경
						mFullSettingLayout.setVisibility(View.GONE);
						mEmergencyLayout.setVisibility(View.VISIBLE);

						double destLat = mMessagePool.getGeoDest().getLatitudeE6()/1E6;
						double destLon = mMessagePool.getGeoDest().getLongitudeE6()/1E6;
						
						// Map 에 출발, 도착 마커 등록
						mStartGeoPoint = new GeoPoint((int)(mCurrentLocation.getLatitude()*1E6), (int)(mCurrentLocation.getLongitude()*1E6));
						mDestGeoPoint = new GeoPoint((int)(destLat*1E6), (int)(destLon*1E6));
						
						// 사용자가 직접 선택하였다면 등록을 하지만, 즐겨찾는 장소를 선택하였다면 등록하지 않는다.
						if(!mMessagePool.getFavoriteFlag()) {
							registeStartPinOverlay(mStartGeoPoint);											// Map 에 출발지점 등록
							registeDestPinOverlay(mDestGeoPoint);											// Map 에 도착지점 등록
						}
						
						// 도착지 알림 등록
						mLocManager.addProximityAlert(destLat, destLon, Constants.DEFAULT_USER_ADD_PROXIMITY_RANGE, -1, mPending);		// 도착지 진입 반경을 설정
						mMessagePool.setAddProximityFlag(true);					// 도착지 지점이 설정되었다고 알림
						
						// 시작과 도착지 Pin 을 MessagePool 에 등록
						mMessagePool.setStartPinOverlay(mStartPinOverlay);
						mMessagePool.setDestPinOverlay(mDestPinOverlay);
						
						// 안전 귀가를 시작하므로 예상 시간 Spinner 를 초기화, 도착지점 값을 초기화
						mEditDest.setText("");
						mHourEditText.setText("");
						mMinEditText.setText("");
						
						mMessagePool.setReturningTimerFlag(true);			// 안전 귀가 시작 타이머가 작동되었다고 메시지 풀에 등록
						
						// 안전 귀가를 시작하므로, 귀가 시작 타이머를 작동 시킨다.
						mReturningTimerThread = new Thread(new Runnable() {
							public void run() {
								while(mMessagePool.getReturningTimerFlag()) {
									mReturningTimerHandler.sendMessage(mReturningTimerHandler.obtainMessage());
									SystemClock.sleep(1000);
									mReturningTimerCount++;
								}
								
								stopTimerThread(mReturningTimerThread);		// Thread 를 벗어나면, Thread를 중지시키고 초기화
								mReturningTimerHandler.sendMessage(mReturningTimerHandler.obtainMessage());
							}
						});
						
						mReturningTimerThread.start();
						
						// 가장 가까운 경찰서 전화번호를 가져오기 위한 쓰레드 작동
						mPoliceTelThread = new Thread(new Runnable() {
							public void run() {
								while(mMessagePool.getReturningFlag()) {
									updatePoliceTel(mCurrentLocation);
									SystemClock.sleep(Constants.DELAY_GET_POLICE_TEL);		// 10초 마다 경찰서 정보 업데이트
								}
							}
						});
						
						mPoliceTelThread.start();
						
						// 현재 사용자의 위치에 따른 실제 주소 값을 10초마다 업데이트 하는 쓰레드
						mMyAddressThread = new Thread(new Runnable() {
							public void run() {
								while(mMessagePool.getReturningFlag()) {
									if(mCurrentLocation != null) {
										mMyAddress = Utils.getAddress(mContext, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
									} else {
										mMyAddress = "현재 사용자의 위치를 확인할 수 없습니다.";
									}
									
									mMessagePool.setMyAddress(mMyAddress);
									mMessagePool.setCurrentLocation(mCurrentLocation);
									SystemClock.sleep(Constants.DELAY_GET_MY_ADDRESS);
								}
							}
						});

						mMyAddressThread.start();
						
						mStandTimerThrad = new Thread(new Runnable() {
							public void run() {
								while(mMessagePool.getReturningFlag()) {
									mStandTimerHandler.sendMessage(mStandTimerHandler.obtainMessage());
									SystemClock.sleep(1000);
									mStandTimerCount++;
								}
							}
						});
						
						mStandTimerThrad.start();
						
						// MapView 를 MessagePool 에 등록
						mMessagePool.setMapView(mMapView);
						
						// 시작 시 처음 마커를 찍어주기 위해 AsyncUpdateMarker 호출
						AsyncUpdateOnlyOverlay asyncUpdateMarkerOverlay = new AsyncUpdateOnlyOverlay();
			    		asyncUpdateMarkerOverlay.execute(mCurrentLocation);
						
						// Notification 등록
						mSafeNotification.registNotification();
						mSafeNotification.updateNotification("안전도 확인 중", mCCTVNumber, mStoreNumber, mPoliceNumber);
						
						// Service 등록
						mServiceIntent = new Intent(mContext, SensorCallService.class);
						mMessagePool.setServiceIntent(mServiceIntent);
						mMessagePool.setServiceFlag(true);
						startService(mServiceIntent);
						
						if(startReturnType == START_RETURN_WITH_BLACK_BOX) {
							// 디렉토리에 사용할 이름 생성
							Calendar bCalendar = Calendar.getInstance();
							String fileDirPath = 
								bCalendar.get(Calendar.YEAR) + Constants.FILE_PATH_DELIMETER + (bCalendar.get(Calendar.MONTH) + 1) + Constants.FILE_PATH_DELIMETER + 
								bCalendar.get(Calendar.DAY_OF_MONTH) + Constants.FILE_PATH_DELIMETER + bCalendar.get(Calendar.HOUR_OF_DAY) + Constants.FILE_PATH_DELIMETER + 
								bCalendar.get(Calendar.MINUTE) + Constants.FILE_PATH_DELIMETER + bCalendar.get(Calendar.SECOND);
							
							
							File bPath = new File(Environment.getExternalStorageDirectory().getPath() + "/SafeReturn_BBox/" + fileDirPath);
							
							// 디렉토리 생성
							if(!bPath.isDirectory()) {
								bPath.mkdirs();
							}
							
							// 블랙박스 서비스 등록
							mBServiceIntent = new Intent(mContext, BCtrlService.class);
							mBServiceIntent.putExtra("FILE_DIR_PATH", bPath.getAbsolutePath());
							mMessagePool.setBServiceIntent(mBServiceIntent);
							mMessagePool.setRunningBService(false);
							
							if(!mMessagePool.isRunningBService()) {
								startService(mBServiceIntent);
								mMessagePool.setRunningBService(true);
							}
						}
					}
				}
			} else {
				if(mCurrentLocation == null) {
					Toast.makeText(mContext, "정확한 현재 위치를 찾고 있습니다.", Toast.LENGTH_SHORT).show();
				} else if(mEditDest.getText().length() == 0) {
					Toast.makeText(mContext, "도착지를 설정해주세요.", Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(mContext, "도착 시간을 정확히 설정해 주세요.", Toast.LENGTH_SHORT).show();
				}
			}
			
			break;
		case R.id.btn_summary_set:
			// 설정 창 원래대로 버튼 클릭
			mSummarySettingLayout.setVisibility(View.GONE);
			mFullSettingLayout.setVisibility(View.VISIBLE);
			
			mMessagePool.setDestFlag(false);
			break;
		case R.id.start_edit_dest:
			// 도착지 주소 입력 버튼 클릭
			if(mEditDest.getText().toString().length() != 0) {
				AddressDetailView addressDetailViewDialog = new AddressDetailView(mContext, R.style.PopupDialog, mEditDest.getText().toString());
				addressDetailViewDialog.show();
			} else {
				Toast.makeText(mContext, "정확한 도착지를 설정해 주세요.", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.start_btn_emergency_call_police:
			if(!mPoliceBtnFlag) {
				Intent callPolice = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+ mClosePoliceTel));
	    		callPolice.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    		mContext.startActivity(callPolice);
	    		
	    		// 버튼이 계속 눌리는 것을 막기 위해 Thread 작동
				mThreadPoliceCheck = new Thread(new Runnable() {
					public void run() {
						mPoliceBtnFlag = true;
						SystemClock.sleep(Constants.DELAY_BUTTON_PUSH);
						mPoliceBtnFlag = false;
					}
				});
				
				mThreadPoliceCheck.start();
			} else {
				Toast.makeText(mContext, "긴급 버튼은 연속으로 누르실 수 없습니다.", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.start_btn_emergency_send_sms:
			if(!mEmergencyBtnFlag) {
				// 현재 상황을 위급 상황으로 변경
				mMessagePool.setEmergencyStateFlag(true);
				
				// 위급 상황을 등록된 사용자에게 알림
				String helpMsg = mConfigPref.getString(Constants.SETTING_EMERGENCY_MESSAGE, Constants.DEFAULT_EMERGENCY_MESSAGE)
									+ "(위치 : " + mMessagePool.getMyAddress() + ")";
				String locMsg = null;
				
				if(mCurrentLocation != null) {
					locMsg = "(위치 확인 : http://maps.google.co.kr/maps?q=" 
									+ mCurrentLocation.getLatitude() + "+" + mCurrentLocation.getLongitude() + ")";
				}
				
				Utils.sendMMS(mContext, helpMsg, locMsg);
				Toast.makeText(mContext, "위급 메시지를 전송하였습니다.", Toast.LENGTH_SHORT).show();
				
				mThreadEmergencyCheck = new Thread(new Runnable() {
					public void run() {
						mEmergencyBtnFlag = true;
						SystemClock.sleep(Constants.DELAY_BUTTON_PUSH);
						mEmergencyBtnFlag = false;
					}
				});
				
				mThreadEmergencyCheck.start();
				
				// 위급 상황 바로 전 시간 대의 블랙 박스 파일을 전송
				Intent sendBServiceIntent = new Intent(StartReturn.this, BBoxSendEmail.class);
				startService(sendBServiceIntent);
				
			} else {
				Toast.makeText(mContext, "긴급 버튼은 연속으로 누르실 수 없습니다.", Toast.LENGTH_SHORT).show();
			}
			break;
			
			
		case R.id.start_btn_emergency_cancel:
			
			// 위급 상황이 취소 되었다고 설정.
			mMessagePool.setEmergencyStateFlag(false);
			mBtnEmergencyCancel.setVisibility(View.GONE);
			break;
		}
	}

	private void showFavoritePathDialog() {					// 즐겨 찾는 장소를 보여주는 다이얼로그
		pathLocation = Utils.getPathList();
		
		if(pathLocation.size() != 0) {
			if(mCurrentLocation != null) {
				final MapSelectDialog mapSelectDialog = new MapSelectDialog(mContext, R.style.PopupDialog, mCurrentLocation);
				mapSelectDialog.show();
				
				mapSelectDialog.setOnDismissListener(new OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						if (mapSelectDialog.getSelectedFlag()) {
							mMapView.removeAllViews();											// 일단 map 위에 그려진 모든 View들을 지운 뒤
							
							// StartPin, DestPin, Path 재설정하는 과정
							registeStartPinOverlay(mapSelectDialog.getStartGeoPoint());			// Map 에 출발지점 등록 및 표시
							registeDestPinOverlay(mapSelectDialog.getDestGeoPoint());			// Map 에 도착지점 등록 및 표시
							restorePathOverlay(mapSelectDialog.getArraySavePathGP());			// Map 에 경로 등록 및 표시
						
							// 주소지, 예상 시간을 설정하는 과정
							mEditDest.setText(mapSelectDialog.getDestAddress());				// 도착지 주소를 표시
							mHourEditText.setText(String.valueOf(mapSelectDialog.getDestHour()));
							mMinEditText.setText(String.valueOf(mapSelectDialog.getDestMin()));
							
							mMessagePool.setFavoriteFlag(true);
						}
					}
				});
			} else {
				Toast.makeText(mContext, "현재 GPS가 사용자의 위치를 파악 중에 있습니다.", Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(mContext, "등록된 경로가 없습니다.", Toast.LENGTH_SHORT).show();
		}
	}
	
	
	public void restorePathOverlay(ArrayList<SavePathGeoPoint> savePathGP) {
		mMessagePool.setArraySavePathGeoPoint(savePathGP);
		mArraySavePathGP = savePathGP;

		int saveGPSize = mArraySavePathGP.size();
		
		for(int i=0; i < saveGPSize; i++) {
			MyPathOverlay pathOverlay = new MyPathOverlay();
	    	pathOverlay.setMyPathOverlay(mArraySavePathGP.get(i), mP1, mP2, mPaint);
	    	mListOverlay.add(pathOverlay);
		}
		
		mMapView.invalidate();
	}
	
	public void registeStartPinOverlay(GeoPoint startGeoPoint) {

		mMessagePool.setGeoStart(startGeoPoint);				// 메시지풀에 시작 지점 좌표 등록
			
		if(mStartPinOverlay == null) {									// startPinOverlay 객체가 생성되어 있지 않다면
			mStartPinOverlay = new StartPinOverlay(mContext, Constants.PIN_MAIN_VIEW_FLAG);			// startPin 을 그려주기 위한 객체를 생성해준다.
		} else {														// 이미 객체가 생성되어져 있다면
			Utils.removeStartPinOverlay(mMapView, mStartPinOverlay);
		}
		
		mStartPinOverlay.setGeoPoint(startGeoPoint);					// 시작 지점 좌표를 설정해주고
		mListOverlay.add(mStartPinOverlay);								// MapView 의 overlayList 에 추가한 다음
		mMapView.invalidate();											// MapView를 update 해준다.
		
	}
	
	public void registeDestPinOverlay(GeoPoint destGeoPoint) {

		mMessagePool.setGeoDest(destGeoPoint);					// 메시지풀에 도착 지점 좌표 등록
				
		if(mDestPinOverlay == null) {									// destPinOverlay 객체가 생성되어 있지 않다면
			mDestPinOverlay = new DestPinOverlay(mContext, Constants.PIN_MAIN_VIEW_FLAG);	// destPin 을 그려주기 위한 객체를 생성해준다.
		} else  {
			Utils.removeDestPinOverlay(mMapView, mDestPinOverlay);
		}
		
		mDestPinOverlay.setGeoPoint(destGeoPoint);						// 시작 지점 좌표를 설정해주고
		mListOverlay.add(mDestPinOverlay);								// MapView 의 overlayList 에 추가한 다음
		mMapView.invalidate();											// MapView를 update 해준다.
		
	}
	
	private void stopTimerThread(Thread thread) {
		if(thread != null && thread.isAlive()) {
			if(thread.equals(mRangeTimerThread)) {
				mRangeTimerCount = 0;
				mMessagePool.setWarningTimerFlag(false);
			} else if(thread.equals(mReturningTimerThread)) {
				mReturningTimerCount = 0;
				mMessagePool.setReturningTimerFlag(false);
			} 
			
			thread.interrupt();
		}
	}
	
	 @Override
	    public void onBackPressed() {
	    	if(mMessagePool.getReturningFlag() == true) {		// 안전 귀가 중 Back 버튼을 눌렀다면
	    		final AppExitPopUpDialog returningStopPopUpDialog = new AppExitPopUpDialog(mContext, R.style.PopupDialog, Constants.START_STOP_DIALOG);
	    		returningStopPopUpDialog.show();
	    		returningStopPopUpDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(returningStopPopUpDialog.getStopFlag() == 1) {		// 안전 귀가 중지 '확인' 버튼 클릭
							stopSafeReturn();
						} else if(returningStopPopUpDialog.getStopFlag() == 2) {	// 경로 등록 버튼을 클릭하였다면
							AsyncGetAddress asyncGetAddressPopUp = new AsyncGetAddress();
							asyncGetAddressPopUp.execute();
						}
					}
				});
	    	} else {		// 안전 귀가가 아직 시작 안된 상태에서 back 버튼을 클릭했다면
	    		final AppExitPopUpDialog startExitPopUpDialog = new AppExitPopUpDialog(mContext, R.style.PopupDialog, Constants.MAP_EXIT_DIALOG);
	    		startExitPopUpDialog.show();
	    		
	    		startExitPopUpDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(startExitPopUpDialog.getFlag() == 1) {		// 확인 클릭
							Utils.widgetStateUpdate(mContext, WidgetSafeReturn.WIDGET_STATE_DEFAULT);
							finish();
							overridePendingTransition(R.anim.fade, R.anim.fade_out);
						} 
					}
				});
	    	}
	    }

	class AsyncGetAddress extends AsyncTask<Void, Void, Void> {
	
		ProgressDialog progressDialog;
		double startLat, startLon, destLat, destLon;
		
		public AsyncGetAddress() {
			this.startLat = (double)(mStartGeoPoint.getLatitudeE6()/1E6);
			this.startLon = (double)(mStartGeoPoint.getLongitudeE6()/1E6);
			this.destLat = mCurrentLocation.getLatitude();
			this.destLon = mCurrentLocation.getLongitude();
		}
		 
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(mContext, "", "잠시만 기다려 주세요...");
		}
		 
	    protected Void doInBackground(Void... params) {
	    	mStartAddress = Utils.getAddress(mContext, startLat, startLon);
	    	mDestAddress = Utils.getAddress(mContext, destLat, destLon);
	    	
	    	if(mStartAddress == null) {
	    		mStartAddress = "";
	    	}
	    	
	    	if(mDestAddress == null) {
	    		mDestAddress = "";
	    	}
	    	
	    	return null;
	    }
		 
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			
			showRegisterPathDialog(mContext);
	    }
		 
	 }
	 
	 	private void showRegisterPathDialog(final Context context) {
			
			final Dialog registePathDialog = new Dialog(context, R.style.PopupDialog);
			
			WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
			lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
			lpWindow.dimAmount = 0.75f;
			registePathDialog.getWindow().setAttributes(lpWindow);
			
			registePathDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			registePathDialog.setContentView(R.layout.dialog_path_register);
			
			final int destHour = mMessagePool.getDestHour();
			final int destMin = mMessagePool.getDestMin();
			
			final EditText editStart = (EditText)registePathDialog.findViewById(R.id.dialog_path_register_input_start);		// 출발지명 객체 생성
			final EditText editDest = (EditText)registePathDialog.findViewById(R.id.dialog_path_register_input_dest);		// 도착지명 객체 생성
			final EditText editStartAddress = (EditText)registePathDialog.findViewById(R.id.dialog_path_register_input_start_address);
			final EditText editDestAddress = (EditText)registePathDialog.findViewById(R.id.dialog_path_register_input_dest_address);
			TextView textDestTime = (TextView)registePathDialog.findViewById(R.id.dialog_path_time);
			ImageButton btnModifyStartAddress = (ImageButton)registePathDialog.findViewById(R.id.dialog_path_register_start_address_modify);
			ImageButton btnModifyDestAddress = (ImageButton)registePathDialog.findViewById(R.id.dialog_path_register_dest_address_modify);
			ImageButton btnOk = (ImageButton)registePathDialog.findViewById(R.id.dialog_path_register_btn_ok);
			ImageButton btnCancel = (ImageButton)registePathDialog.findViewById(R.id.dialog_path_register_btn_cancel);

			editStartAddress.setText(mStartAddress);		// 시작 주소 설정
			editDestAddress.setText(mDestAddress);		// 도착주소 설정
			textDestTime.setText(destHour + "시간 " + destMin + "분 소요");
			
			btnModifyStartAddress.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					registePathDialog.dismiss();
					
					final AddressModifyDialog startAddrModifyDialog = new AddressModifyDialog(mContext, R.style.PopupDialog, 
							Constants.DIALOG_FLAG_MOIDFY_START_ADDR, editStartAddress.getText().toString());
					startAddrModifyDialog.show();
					
					startAddrModifyDialog.setOnDismissListener(new OnDismissListener() {
						public void onDismiss(DialogInterface dialog) {
							editStartAddress.setText(startAddrModifyDialog.getAddrText());
							mStartAddress = startAddrModifyDialog.getAddrText();
							registePathDialog.show();
						}
					});
				}
			});
			
			btnModifyDestAddress.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					registePathDialog.dismiss();
					
					final AddressModifyDialog destAddrModifyDialog = new AddressModifyDialog(mContext, R.style.PopupDialog, 
							Constants.DIALOG_FLAG_MOIDFY_DEST_ADDR, editDestAddress.getText().toString());
					destAddrModifyDialog.show();
					
					destAddrModifyDialog.setOnDismissListener(new OnDismissListener() {
						public void onDismiss(DialogInterface dialog) {
							editDestAddress.setText(destAddrModifyDialog.getAddrText());
							mDestAddress = destAddrModifyDialog.getAddrText();
							registePathDialog.show();
						}
					});
				}
			});
			
			btnOk.setOnClickListener(new View.OnClickListener() {	// 확인 버튼을 누르면 위치 등록 정보들을 전부 DB 에 INSERT
				public void onClick(View v) {

					byte[] pathBytes = Utils.serializeObject(mArraySavePathGP);
					
					// DB 에 Path 삽입을 위해 준비
					String startName = editStart.getText().toString();
					String destName = editDest.getText().toString();		// 사용자가 입력한 경로명을 저장하기 위한 String 변수
					
					if(startName.length() != 0 && destName.length() != 0 &&
							mStartAddress.length() != 0 && mDestAddress.length() != 0) {		// 출발, 도착지명과 주소들이 비어있지 않다면
						AsyncRegistePath asyncRegistePath = 
							new AsyncRegistePath(startName, editStartAddress.getText().toString(), destName, 
									editDestAddress.getText().toString(), destHour, destMin, pathBytes, registePathDialog);
						asyncRegistePath.execute();
					} else {
						Toast.makeText(mContext, "등록할 경로명과 주소를 입력해 주세요", Toast.LENGTH_SHORT).show();
					}
				}
			});
			
			btnCancel.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					registePathDialog.dismiss();
					onBackPressed();
				}
			});
			registePathDialog.show();
		}
	 	
	 	private class AsyncRegistePath extends AsyncTask<Void, Void, Void> {

			private double startLat, startLon, destLat, destLon;
			private String startName, destName;
			private String startAddr, destAddr;
			private int destHour, destMin;
			private byte[] pathBytes;
			private ProgressDialog progressDialog;
			private Dialog registePathDialog;
			
			public AsyncRegistePath(String startName, String startAddr, String destName, String destAddr, 
					int destHour, int destMin, byte[] pathBytes, Dialog registePathDialog) {
				this.startName = startName;
				this.destName = destName;
				this.startAddr = startAddr;
				this.destAddr = destAddr; 
				this.startLat = (double)(mStartGeoPoint.getLatitudeE6()/1E6);
				this.startLon = (double)(mStartGeoPoint.getLongitudeE6()/1E6);
				this.destLat = mCurrentLocation.getLatitude();
				this.destLon = mCurrentLocation.getLongitude();
				this.destHour = destHour;
				this.destMin = destMin;
				this.pathBytes = pathBytes;
				this.registePathDialog = registePathDialog;
			}
			
			@Override
			protected void onPreExecute() {
				progressDialog = ProgressDialog.show(mContext, null, "경로를 등록중입니다...");
			}
			
			@Override
			protected Void doInBackground(Void... params) {
				Utils.insertDbPath(startName, startLat, startLon, startAddr, 
						destName, destLat, destLon, destAddr, destHour, destMin, pathBytes);
				
				return null;
			}
			
			
			@Override
			protected void onPostExecute(Void result) {
				if(progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
				
				stopSafeReturn();
				registePathDialog.dismiss();
			}
		}

	 private void stopSafeReturn() {
		 mMessagePool.setReturningFlag(false);
		
		 if(mMessagePool.getAddProximityFlag()== true) {
		 	 mLocManager.removeProximityAlert(mPending);			// 등록된 addProximityAlert 을 제거한 뒤
		 	 mMessagePool.setAddProximityFlag(false);
		 }
		
 		 // 화면에 Layout 을 설정 레이아웃으로 바꿔준 뒤
		 mFullSettingLayout.setVisibility(View.VISIBLE);
		 mEmergencyLayout.setVisibility(View.GONE);
		 mHourEditText.setText("");
		 mMinEditText.setText("");
		 mEditDest.setText("");						// 도착지점을 표시해주는 EditText 초기화
		
		 mListOverlay.clear();
		 
 		 // 사용자의 경로를 기록해둔 ArrayList를 초기화
		 if(mArraySavePathGP != null) {
			 mArraySavePathGP.clear();
			 mMessagePool.setArraySavePathGeoPoint(mArraySavePathGP);
		 }
		 
		 
		 if(DEVELOPE_MODE) {
			 Log.d(TAG, "현재 mapView 상태 : " + mMapView.getOverlays().size());
		 }
		
		 if(mMyLocationOverlay == null) {
	 		 mMyLocationOverlay = new MyOverlay(mContext, mMapView, mRangeValue);
		 } 
		
		 mMyLocationOverlay.enableMyLocation();
		    
		 mMyLocationOverlay.runOnFirstFix(new Runnable() {
			 public void run() {
				 mMapView.getController().animateTo(mMyLocationOverlay.getMyLocation());
			 }
		 });
		     
		 mListOverlay.add(mMyLocationOverlay);
		
		 if(mDestPinEventOverlay == null) {
			 mDestPinEventOverlay = new DestPinEventOverlay(mContext);
		 }
		
		 mListOverlay.add(mDestPinEventOverlay);
		 mListOverlay.add(new ZoomOverlay(mContext, mMapView));
		
		
		 mMapView.removeAllViews();
		
		 if(mRangeTimerThread != null && mRangeTimerThread.isAlive()) {
			 stopTimerThread(mRangeTimerThread);
		 }
		
		 if(mReturningTimerThread != null && mReturningTimerThread.isAlive()) {
			 stopTimerThread(mReturningTimerThread);
		 }
		
		 mSafeNotification.removeNotification();				// Notification 이 등록되어있다면 해제
		
		 mMessagePool.setFavoriteFlag(false);
		 mMessagePool.setReturningFlag(false);
		
		 if(mServiceIntent != null) {
			 stopService(mServiceIntent);		// Service 등록되어있다면 해제
		 }
		 
		 if(mBServiceIntent != null) {
			 stopService(mBServiceIntent);
			 mMessagePool.setRunningBService(false);
		 }
		 
		
		 mMapView.invalidate();
		 mMessagePool.setMapView(mMapView);
		
		 Utils.widgetStateUpdate(mContext, WidgetSafeReturn.WIDGET_STATE_START_NEW);
	  }
	 
	  private void getIntenter() {
		 Intent intent = getIntent();
		 
		 String caller = null;
		 
		 if(intent != null) {
			 caller = intent.getDataString();
		 }
		 
		 if(DEVELOPE_MODE) {
			 Log.d(TAG, "호출한 caller : " + caller);
		 }
		 
		 if(caller != null) {
			 if(caller.equals(Constants.CALL_BY_WIDGET_PATH)) {
				 mMapView.removeAllViews();
				 
				 // 시작 지점, 도착 지점과 경로를 등록하는 과정
				 GeoPoint startGP = new GeoPoint(
						 (int)(intent.getExtras().getDouble(Constants.ATTRIBUTE_MYLOCATION_START_LAT) * 1E6), 
						 (int)(intent.getExtras().getDouble(Constants.ATTRIBUTE_MYLOCATION_START_LON) * 1E6));
				 GeoPoint destGP = new GeoPoint(
						 (int)(intent.getExtras().getDouble(Constants.ATTRIBUTE_MYLOCATION_DEST_LAT) * 1E6), 
						 (int)(intent.getExtras().getDouble(Constants.ATTRIBUTE_MYLOCATION_DEST_LON) * 1E6));
				 
				 registeStartPinOverlay(startGP);
				 registeDestPinOverlay(destGP);
				 
				 mArraySavePathGP = (ArrayList<SavePathGeoPoint>) Utils.deSerializeObject(intent.getExtras().getByteArray(Constants.ATTRIBUTE_MYLOCATION_PATH_BYTES));
				 restorePathOverlay(mArraySavePathGP);
				 
				 // 주소지, 예상 시간을 설정하는 과정
				 mEditDest.setText(intent.getExtras().getString(Constants.ATTRIBUTE_MYLOCATION_DEST_ADDR));
				 mHourEditText.setText(String.valueOf(intent.getExtras().getInt(Constants.ATTRIBUTE_MYLOCATION_DEST_HOUR)));
				 mMinEditText.setText(String.valueOf(intent.getExtras().getInt(Constants.ATTRIBUTE_MYLOCATION_DEST_MIN)));
				 
				 mMessagePool.setFavoriteFlag(true);
				 mBtnReturnStart.performClick();
				 
				 mMapController.animateTo(startGP);
				 
				 mCallWidgetState = WidgetSafeReturn.WIDGET_STATE_START_CHOICE;
			 }
		 }
	 }
	 	
	 @Override
	 protected boolean isRouteDisplayed() {
		return false;
	 }
	 

	 @Override
	 public void onLocationChanged(Location location) {
		if(mMessagePool.getReturningFlag() == true) {		// 안전 귀가를 시작하면
			if(DEVELOPE_MODE) {
				Log.d(TAG, "현재 위치가 파악 되었나? : " + location);
			}
			
			// 안전 귀가 중에는 CPU 가 유휴 상태로 들어가지 않도록 설정
			mServiceWakeLock = WakeLockService.getInstance();
			mServiceWakeLock.registeWakeLock(mContext);
			
			// 사용자가 직접 도착지 설정
			if(mMessagePool.getFavoriteFlag() == false) { 
				if(mCurrentLocation != null && location != null) {		// 사용자의 Path 를 그리기 시작
					Location loc[] = {mCurrentLocation, location};
					AsyncUpdatePathAndOverlay asyncUpdatePathAndOverlay = new AsyncUpdatePathAndOverlay();	// path 처리를 Async 를 통해 UI 업데이트
					asyncUpdatePathAndOverlay.execute(loc);
				}
			} 
			
			// 즐겨찾는 장소에서 설정
			else {		
				AsyncUpdateOnlyOverlay asyncUpdateMarkerOverlay = new AsyncUpdateOnlyOverlay();
				asyncUpdateMarkerOverlay.execute(location);
				
				boolean accuracyDistanceFlag = false;
				
				// 사용자가 등록한 Path 와 설정 반경 범위를 비교하는 부분
//					long startTime = SystemClock.currentThreadTimeMillis();
				
				int compareSize = mArraySavePathGP.size();
				for(int i=0; i < compareSize; i++) {
					int compareDestDistance = (int)Utils.distanceBetween(
							mArraySavePathGP.get(i).getDestLat(), mArraySavePathGP.get(i).getDestLon(), 
							location.getLatitude(), location.getLongitude());
					
					int compareStartDistance = (int)Utils.distanceBetween(
							mArraySavePathGP.get(i).getStartLat(), mArraySavePathGP.get(i).getStartLon(), 
							location.getLatitude(), location.getLongitude());
					
					if(DEVELOPE_MODE) {
						Log.d(TAG, "비교한 거리(시작 지점으로) : " + compareStartDistance);
						Log.d(TAG, "비교한 거리(도착 지점으로) : " + compareDestDistance);
					}
					
					if(compareDestDistance <= Constants.COMPARE_ACCURACY_DISTANCE || compareStartDistance <= Constants.COMPARE_ACCURACY_DISTANCE) {
						accuracyDistanceFlag = true;
						mMessagePool.setWarningTimerFlag(false);
					}
				}
				
				
				if(accuracyDistanceFlag == false) {		// 경로를 이탈하였다면
					// 경고 타이머 작동되었다고 알림
					Toast.makeText(mContext, "경고! 경고! 현재 등록된 경로를 이탈하였습니다!", Toast.LENGTH_SHORT).show();
					
					if(mMessagePool.getWarningTimerFlag() == false) {		// 아직 작동을 하지 않았으면 false 이므로
						mRangeTimerThread = new Thread(new Runnable() {
							public void run() {
								mMessagePool.setWarningTimerFlag(true);		// 타이머가 작동하고 있다고 변경
								while(mMessagePool.getWarningTimerFlag()) {
									mRangeTimerHandler.sendMessage(mRangeTimerHandler.obtainMessage());
									SystemClock.sleep(1000);
									mRangeTimerCount++;
								}

								stopTimerThread(mRangeTimerThread);		// Thread 를 벗어나면, Thread를 중지시키고 초기화
								mRangeTimerHandler.sendMessage(mRangeTimerHandler.obtainMessage());
							}
						});
						mRangeTimerThread.start();
					}
				}
			}
			
			// 위치가 바뀌면 제자리 위급 알림 서비스의 시간을 초기화 한다.
			mStandTimerCount = 0;
			mStandOutTimeCount = 0;
			
			mMessagePool.setMapView(mMapView);
		}
		
		mCurrentLocation = location;					// start 와 end 포인트를 바꿔준다.
		
	 }

	 public void onProviderDisabled(String provider) {}
	 public void onProviderEnabled(String provider) {}
	 public void onStatusChanged(String provider, int status, Bundle extras) {}
}
