package madcat.safereturn.menu;

import madcat.safereturn.pro.R;
import madcat.safereturn.utils.Utils;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;

public class Help extends Activity {
	
	private ImageButton mBtnPrev, mBtnNext;
	private ImageView mTextImage, mPageNumberImage;;
	
	private int mPageCount = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.help);
		
		mBtnPrev = (ImageButton)findViewById(R.id.help_prev);
		mBtnNext = (ImageButton)findViewById(R.id.help_next);
		mTextImage = (ImageView)findViewById(R.id.help_text_image);
		mPageNumberImage = (ImageView)findViewById(R.id.help_page_number_image);
		
		mBtnNext.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(mPageCount < 6) {
					mPageCount++;
				}
				
				switch(mPageCount) {
				case 1:
					mTextImage.setBackgroundResource(R.drawable.help_1st_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_1st_page_number);
					mBtnPrev.setVisibility(View.INVISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 2:
					mTextImage.setBackgroundResource(R.drawable.help_2nd_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_2nd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 3:
					mTextImage.setBackgroundResource(R.drawable.help_3rd_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_3rd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 4:
					mTextImage.setBackgroundResource(R.drawable.help_4th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_4th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 5:
					mTextImage.setBackgroundResource(R.drawable.help_5th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_5th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 6:
					mTextImage.setBackgroundResource(R.drawable.help_6th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_6th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.INVISIBLE);
					break;
				default:
				}
			}
		});
		
		mBtnPrev.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(mPageCount > 0) {
					mPageCount--;
				}
				
				switch(mPageCount) {
				case 1:
					mTextImage.setBackgroundResource(R.drawable.help_1st_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_1st_page_number);
					mBtnPrev.setVisibility(View.INVISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 2:
					mTextImage.setBackgroundResource(R.drawable.help_2nd_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_2nd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 3:
					mTextImage.setBackgroundResource(R.drawable.help_3rd_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_3rd_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 4:
					mTextImage.setBackgroundResource(R.drawable.help_4th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_4th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 5:
					mTextImage.setBackgroundResource(R.drawable.help_5th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_5th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.VISIBLE);
					break;
				case 6:
					mTextImage.setBackgroundResource(R.drawable.help_6th_text_img);
					mPageNumberImage.setBackgroundResource(R.drawable.help_6th_page_number);
					mBtnPrev.setVisibility(View.VISIBLE);
					mBtnNext.setVisibility(View.INVISIBLE);
					break;
				default:
				}
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
