package madcat.safereturn.menu;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import madcat.safereturn.pro.R;
import madcat.safereturn.blackbox.BCtrlService;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.AlertGpsDialog;
import madcat.safereturn.dialog.AppExitPopUpDialog;
import madcat.safereturn.maps.CCTVOverlay;
import madcat.safereturn.maps.FireOverlay;
import madcat.safereturn.maps.MyOverlay;
import madcat.safereturn.maps.PoliceOverlay;
import madcat.safereturn.maps.StoreOverlay;
import madcat.safereturn.service.NotificationSafeReturn;
import madcat.safereturn.service.SensorCallService;
import madcat.safereturn.service.WakeLockService;
import madcat.safereturn.utils.MessagePool;
import madcat.safereturn.utils.Utils;
import madcat.safereturn.widget.WidgetSafeReturn;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class EmergencyMode extends MapActivity implements LocationListener, OnClickListener {
	
	private final String TAG										=	"EmergencyMode";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private final int SET_TIME_MIN									=	120;
	
	private String mSafeCheck										=	"init";
	
	private Context mContext;
	private MessagePool mMessagePool;
	private SharedPreferences mConfigPref;
	
	private MapView mMapView;
	private MapController mMapController;
	
	private ImageButton mBtnPolice, mBtnNotify;
	private Button mBtnEmergencyCancel;
	
	private LocationManager mLocManager;
	private String mBestProvider;
	
	private Location mCurrentLocation;
	
	private List<Overlay> mListOverlay;
	private MyOverlay mMyLocationOverlay;
	
	private NotificationSafeReturn mSafeNotification;
	private Intent mServiceIntent;
	
	private CCTVOverlay mCCTVOverlay;
	private FireOverlay mFireOverlay;
	private StoreOverlay mStoreOverlay;
	private PoliceOverlay mPoliceOverlay;
	
	private Drawable mCCTVMarker, mPoliceMarker, mStoreMarker, mFireMarker;
	private int mCCTVNumber, mPoliceNumber, mStoreNumber, mFireNumber;
	
	private Thread mPoliceTelThread, mMyAddressThread;
	private String mClosePoliceTel, mMyAddress;
	
	private Thread mThreadPoliceCheck, mThreadEmergencyCheck;
	private boolean mPoliceBtnFlag									=	false;
	private boolean mEmergencyBtnFlag								=	false;
	
	private WakeLockService mServiceWakeLock;
	
	private int mRangeValue;
	private boolean mCCTVOverlayState, mPoliceOverlayState, mStoreOverlayState, mFireOverlayState;
	
	// BlackBox 관련 변수
	private Intent mBServiceIntent;
	
	@Override
	protected void onCreate(Bundle icicle) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}
		
		super.onCreate(icicle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.start_emergency_mode);
		
		mContext = this;
		mMessagePool = (MessagePool)mContext.getApplicationContext();
		mConfigPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		// 초기 MessagePool 설정
		mMessagePool.setEmergencyFlag(false);
		
		// Notification 설정
	    mSafeNotification = NotificationSafeReturn.getInstance();
	    mSafeNotification.setContext(mContext);
		
		// Resource Id 설정
		mMapView = (MapView)findViewById(R.id.emergency_mode_map_view);
		mBtnPolice = (ImageButton)findViewById(R.id.emergency_mode_btn_police_tel);
		mBtnNotify = (ImageButton)findViewById(R.id.emergency_mode_btn_notify);
		mBtnEmergencyCancel = (Button)findViewById(R.id.emergency_mode_btn_cancel);
		
		// 위급 상황 해제 버튼을 등록
		mMessagePool.setBtnEmergencyCancel(mBtnEmergencyCancel);
		
		// Map 설정
		mMapView.setSatellite(false);															// 위성으로 표시 안하도록 설정
		mMapController = mMapView.getController();												// 맵 컨트롤러를 가져온다.
	    mMapController.setZoom(Constants.MAP_ZOOM_LEVEL);										// 초기 맵 확대는 17정도로 설정
	    mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);				// GPS를 관리하는 LocationManager 설정
	    
	    mBestProvider = mLocManager.getBestProvider(Utils.setCriteria(), true);
	    
	    if(mBestProvider != null) {
	    	mCurrentLocation = mLocManager.getLastKnownLocation(mBestProvider);
	    }
	    
	    // 반경 범위 설정
	    mRangeValue = mConfigPref.getInt(Constants.SETTING_RANGE_VALUE, Constants.DEFAULT_USER_RANGE);
	    
	    // 오버레이 표시 상태 설정
	    mCCTVOverlayState = mConfigPref.getBoolean(Constants.SETTING_CCTV_OVERLAY_STATE, true);
	    mPoliceOverlayState = mConfigPref.getBoolean(Constants.SETTING_POLICE_OVERLAY_STATE, true);
	    mStoreOverlayState = mConfigPref.getBoolean(Constants.SETTING_STORE_OVERLAY_STATE, true);
	    mFireOverlayState = mConfigPref.getBoolean(Constants.SETTING_FIRE_OVERLAY_STATE, true);
	    
	    // Zoom Controller 설정
	    LinearLayout zoomLayout = (LinearLayout)findViewById(R.id.emergency_mode_layout_zoom);
	    View zoomView = mMapView.getZoomControls();
        zoomLayout.addView(zoomView, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        mMapView.displayZoomControls(true);
        
        // MapView 의 Overlay 를 관리할 List 설정
	    mListOverlay = mMapView.getOverlays();
        
        // Marker 설정
	    if(mCCTVOverlayState) {
	        mCCTVMarker = getResources().getDrawable(R.drawable.pin_marker_cctv);
	        mCCTVOverlay = new CCTVOverlay(mCCTVMarker, mContext);
	    }
        
	    if(mPoliceOverlayState) {
	        mPoliceMarker = getResources().getDrawable(R.drawable.pin_marker_police);
	        mPoliceOverlay = new PoliceOverlay(mPoliceMarker, mContext);
	    }
        
	    if(mStoreOverlayState) {
	    	mStoreMarker = getResources().getDrawable(R.drawable.pin_marker_store);
	        mStoreOverlay = new StoreOverlay(mStoreMarker, mContext);
	    }
        
	    if(mFireOverlayState) {
	        mFireMarker = getResources().getDrawable(R.drawable.pin_marker_fire);
	        mFireOverlay = new FireOverlay(mFireMarker, mContext);
	    }
	    
	    // 현재 위치 설정
        setMyLocationOverlay();
	   
//	    // MapView 가 사용할 Overlay 추가
//        mListOverlay.add(new ZoomOverlay(mContext, mMapView));
        
        // Event Listener 설정
        mBtnPolice.setOnClickListener(this);
        mBtnNotify.setOnClickListener(this);
        mBtnEmergencyCancel.setOnClickListener(this);
        
        // 긴급 안전 모드가 시작 되었음을 등록
        mMessagePool.setEmergencyFlag(true);
    	
    	// Notification 등록
		mSafeNotification.registNotification();
		mSafeNotification.updateNotification("안전도 확인 중", mCCTVNumber, mStoreNumber, mPoliceNumber);
		
		// 가장 가까운 경찰서 전화번호를 가져오는 쓰레드 시작
		mPoliceTelThread = new Thread(new Runnable() {
			public void run() {
				while(mMessagePool.getEmergencyFlag()) {
					updatePoliceTel(mCurrentLocation);
					SystemClock.sleep(Constants.DELAY_GET_POLICE_TEL);
				}
			}
		});
		
		mPoliceTelThread.start();
		
		// 현재 사용자의 위치에 따른 실제 주소 값을 10초마다 업데이트 하는 쓰레드
		mMyAddressThread = new Thread(new Runnable() {
			public void run() {
				while(mMessagePool.getEmergencyFlag()) {
					if(mCurrentLocation != null) {
						mMyAddress = Utils.getAddress(mContext, mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
					} else {
						mMyAddress = "현재 사용자의 위치를 확인할 수 없습니다.";
					}
					
					mMessagePool.setMyAddress(mMyAddress);
					mMessagePool.setCurrentLocation(mCurrentLocation);
					SystemClock.sleep(Constants.DELAY_GET_MY_ADDRESS);
				}
			}
		});
		
		mMyAddressThread.start();
		
		// Service 등록
		mServiceIntent = new Intent(mContext, SensorCallService.class);
		mMessagePool.setServiceIntent(mServiceIntent);
		mMessagePool.setServiceFlag(true);
		startService(mServiceIntent);
		
		if(mConfigPref.getBoolean(Constants.SETTING_BBOX_ENABLE, true)) {
		
			// BlackBox 서비스 등록
			if(DEVELOPE_MODE) {
				Log.d(TAG, "현재 SD 카드 사용 여부 : " + Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED));
			}
			
			if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
				
				// 사용할 수 있는 최소 공간을 체크한다.
				StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
				long bytesAvailable = (long)stat.getFreeBlocks() * (long)stat.getBlockSize();
				boolean remainSpace = (int) (bytesAvailable / (1024 * 1000)) > (SET_TIME_MIN * Constants.MIN_TO_MEGA) ? true : false;
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "현재 SDCard 남은 공간 : " + bytesAvailable);
					Log.d(TAG, "remainSpace (최소한 필요한 공간) : " + remainSpace);
				}
				
				if(remainSpace) {
					
					// 디렉토리에 사용할 이름 생성
					Calendar bCalendar = Calendar.getInstance();
					String fileDirPath = 
						bCalendar.get(Calendar.YEAR) + Constants.FILE_PATH_DELIMETER + (bCalendar.get(Calendar.MONTH) + 1) + Constants.FILE_PATH_DELIMETER + 
						bCalendar.get(Calendar.DAY_OF_MONTH) + Constants.FILE_PATH_DELIMETER + bCalendar.get(Calendar.HOUR_OF_DAY) + Constants.FILE_PATH_DELIMETER + 
						bCalendar.get(Calendar.MINUTE) + Constants.FILE_PATH_DELIMETER + bCalendar.get(Calendar.SECOND);
					
					
					File bPath = new File(Environment.getExternalStorageDirectory().getPath() + "/SafeReturn_BBox/" + fileDirPath);
					
					// 디렉토리 생성
					if(!bPath.isDirectory()) {
						bPath.mkdirs();
					}
					
					mBServiceIntent = new Intent(mContext, BCtrlService.class);
					mBServiceIntent.putExtra("FILE_DIR_PATH", bPath.getAbsolutePath());
					mMessagePool.setBServiceIntent(mBServiceIntent);
					mMessagePool.setBServiceFlag(true);
					mMessagePool.setRunningBService(false);
					
					// 안전 귀가 중에는 CPU 가 유휴 상태로 들어가지 않도록 설정
					mServiceWakeLock = WakeLockService.getInstance();
					mServiceWakeLock.registeWakeLock(mContext);
					
				}
			}
		}
		
	}
	
	@Override
	protected void onStart() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onStart 호출");
		}
		
		super.onStart();

		mBestProvider = mLocManager.getBestProvider(Utils.setCriteria(), true);
		
		// GPS 체크 유무 확인
		if(DEVELOPE_MODE) {
			Log.d(TAG, "현재 BestProvider : " + mBestProvider);
		}
		
		
		if(mBestProvider == null) {	
			if(DEVELOPE_MODE) {
				Log.d(TAG, "사용할 수 있는 Provider 가 존재 하지 않습니다. GPS 를 확인해 주세요");
			}
			
    		final AlertGpsDialog alertGpsDialog = new AlertGpsDialog(mContext, R.style.PopupDialog);
    		alertGpsDialog.show();
    		
    		alertGpsDialog.setOnDismissListener(new OnDismissListener() {
				public void onDismiss(DialogInterface dialog) {
					switch(alertGpsDialog.getDialogStateFlag()) {
						case 1:
							Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
							startActivity(gpsOptionsIntent);
							overridePendingTransition(R.anim.fade, R.anim.hold);
							finish();
							break;
						case 2:
							finish();
							overridePendingTransition(R.anim.fade, R.anim.fade_out);
						default:
							break;
					}

					Utils.widgetStateUpdate(mContext, WidgetSafeReturn.WIDGET_STATE_DEFAULT);
					
					if(mServiceWakeLock != null) {
						mServiceWakeLock.releaseWakeLock();
					}
				}
			});
    	} else {
	    	if(mMyLocationOverlay == null) {
	    		mMyLocationOverlay = new MyOverlay(this, mMapView, mRangeValue);
	    	}
	    	
	    	mMyLocationOverlay.enableMyLocation();
	    	mLocManager.requestLocationUpdates(mBestProvider, Constants.TIME_UPDATE_PERIOD, Constants.DISTANCE_UPDATE_PERIOD, this);
	    	
	    	Utils.widgetStateUpdate(mContext, WidgetSafeReturn.WIDGET_STATE_START_EMERGENCY);
	    	
	    	if(DEVELOPE_MODE) {
	    		Log.d(TAG,"onStart 에서 isRunningBService : " + mMessagePool.isRunningBService());
	    	}
	    	
	    	if(mConfigPref.getBoolean(Constants.SETTING_BBOX_ENABLE, true)) {
	    		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			    	if(!mMessagePool.isRunningBService()) {
			    		startService(mBServiceIntent);
			    		mMessagePool.setRunningBService(true);
			    	}
	    		} else {
	    			Toast.makeText(mContext, getString(R.string.toast_title_caution_sdcard), Toast.LENGTH_LONG).show();
	    		}
	    	}
    	}
	}
	
	@Override
    protected void onDestroy() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onDestroy 호출");
		}
		
		mLocManager.removeUpdates(this);								// 사용자의 실시간 정보 위치를 받아들이는 requestUpdate() 를 제거
		mSafeNotification.removeNotification();							// Notification 이 등록되어있다면 제거
		
		if(mServiceWakeLock != null) {
			mServiceWakeLock.releaseWakeLock();								//	WakeLock 해제
		}
		
		if(mServiceIntent != null) {
			stopService(mServiceIntent);								// Service 등록되어있다면 해제
		}
		
		if(mBServiceIntent != null) {
			stopService(mBServiceIntent);
			mMessagePool.setRunningBService(false);
		}
		
		//MessagePool 에서 사용한 값 초기화
		mMessagePool.setEmergencyFlag(false);
		
		Utils.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
    }
	
	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	@Override
	public void onLocationChanged(Location location) {
		AsyncUpdateOnlyOverlay asyncUpdateOverlay = new AsyncUpdateOnlyOverlay();
		asyncUpdateOverlay.execute(location);
		
		mCurrentLocation = location;
	}
	
	// 현재 내 위치를 실시간으로 표시한다.
    private void setMyLocationOverlay() {
    	 mMyLocationOverlay = new MyOverlay(getApplicationContext(), mMapView, mRangeValue);
         mMyLocationOverlay.enableMyLocation();
         
         mMyLocationOverlay.runOnFirstFix(new Runnable() {
 			@Override
 			public void run() {
				mMapView.getController().animateTo(mMyLocationOverlay.getMyLocation());
 			}
 		});
         
         mListOverlay.add(mMyLocationOverlay);
         mMapView.invalidate();
    }

	public void onProviderDisabled(String provider) {}
	public void onProviderEnabled(String provider) {}
	public void onStatusChanged(String provider, int status, Bundle extras) {}
	
	@Override
	public void onBackPressed() {
		// 종료하시겠습니까? 다이얼로그를 띄워준 뒤 확인을 누르면
		final AppExitPopUpDialog appExitPopDialog = new AppExitPopUpDialog(mContext, R.style.PopupDialog, Constants.EMERGENCY_EXIT_DIALOG);
		appExitPopDialog.show();
		
		appExitPopDialog.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				switch(appExitPopDialog.getEmergencyFlag()) {
					case 1:
						Utils.widgetStateUpdate(mContext, WidgetSafeReturn.WIDGET_STATE_DEFAULT);
						finish();
						overridePendingTransition(R.anim.fade, R.anim.fade_out);
						break;
				}
			}
		});
	}
	
	class AsyncUpdateOnlyOverlay extends AsyncTask<Location, Location, Void> {
		@Override
		protected Void doInBackground(Location... loc) {
			publishProgress(loc);
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Location... loc) {
			updateOverlayData(loc[0]);
		}
	}
    
    private void updateOverlayData(Location myLocation) {
    	// 화면에 표시된 Overlay 를 일단 삭제한다.
    	long startTime = System.currentTimeMillis();
    	
    	if(mCCTVOverlay != null) { 
    		mListOverlay.remove(mCCTVOverlay);
    		mCCTVOverlay.clearOverlay();
    	}
    	
    	if(mPoliceOverlay != null) { 
    		mListOverlay.remove(mPoliceOverlay);
    		mPoliceOverlay.clearOverlay();
    	}
    	
    	if(mStoreOverlay != null) { 
    		mListOverlay.remove(mStoreOverlay);
    		mStoreOverlay.clearOverlay();
    	}
    	
    	if(mFireOverlay != null) { 
    		mListOverlay.remove(mFireOverlay);
    		mFireOverlay.clearOverlay();
    	}

    	
    	HashMap<String, Overlay> dataItem = null;
    	
    	if(mCCTVOverlayState != false || mPoliceOverlayState != false || mStoreOverlayState != false || mFireOverlayState != false) { 
	    	dataItem = Utils.getBetweenOverlay(myLocation, mRangeValue, mCCTVOverlay, mPoliceOverlay, mStoreOverlay, mFireOverlay);
    	}
    	
    	if(mCCTVOverlayState) {
	    	mCCTVOverlay = (CCTVOverlay) dataItem.get("CCTV");
	    	mCCTVOverlay.overlayPopulate();
	    	mListOverlay.add(mCCTVOverlay);
	    	mCCTVNumber = mCCTVOverlay.size();
    	}
    	
    	if(mPoliceOverlayState) {
	    	mPoliceOverlay = (PoliceOverlay) dataItem.get("POLICE");
	    	mPoliceOverlay.overlayPopulate();
	    	mListOverlay.add(mPoliceOverlay);
	    	mPoliceNumber = mPoliceOverlay.size();
    	}
    	
    	if(mStoreOverlayState) {
	    	mStoreOverlay = (StoreOverlay) dataItem.get("STORE");
	    	mStoreOverlay.overlayPopulate();
	    	mListOverlay.add(mStoreOverlay);
	    	mStoreNumber = mStoreOverlay.size();
    	}
    	
    	if(mFireOverlayState) {
	    	mFireOverlay = (FireOverlay) dataItem.get("FIRE");
	    	mFireOverlay.overlayPopulate();
	    	mListOverlay.add(mFireOverlay);
	    	mFireNumber = mFireOverlay.size();
    	}
    	
    	mMapView.invalidate();
    	
    	long endTime = System.currentTimeMillis();
    	
    	if(DEVELOPE_MODE) {
    		Log.d("updateOverlayData", "속도 : " + (endTime - startTime) / 1000.0 + "초");
    	}
    	
    	String safeCheck = Utils.checkSafePercent(mCCTVNumber, mStoreNumber, mPoliceNumber, mFireNumber);
    	
    	if(DEVELOPE_MODE) {
    		Log.d(TAG, "이전 안전도 : " + mSafeCheck + ", 바뀐 안전도 : " + safeCheck);
    	}
    	
    	if(mSafeCheck.equals(safeCheck)) {
    		mSafeNotification.updateNotification(safeCheck, mCCTVNumber, mStoreNumber, mPoliceNumber);
    	} else { 
    		Utils.widgetSafeUpdate(mContext, safeCheck);
    		
    		mSafeNotification.removeNotification();
    		mSafeNotification.registNotification();
    		mSafeNotification.updateNotification(safeCheck, mCCTVNumber, mStoreNumber, mPoliceNumber);
		}
    	
    	mSafeCheck = safeCheck;
    }
    
    private void updatePoliceTel(Location myLocation) {
    	if(myLocation != null) {
			Cursor cursor = Utils.getClosePoliceTel(myLocation);
			cursor.moveToFirst();
			
	
			mClosePoliceTel = cursor.getString(0);
			cursor.close();
    	} else {
    		mClosePoliceTel = "02-112";
    	}
    	
    	mMessagePool.setClosePoliceTel(mClosePoliceTel);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.emergency_mode_btn_police_tel:
				// 가장 가까운 경찰서로 긴급 전화
				if(!mPoliceBtnFlag) {
					Intent callPolice = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+ mClosePoliceTel));
	        		callPolice.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        		mContext.startActivity(callPolice);
        		
	        		// 버튼이 계속 눌리는 것을 막기 위해 Thread 작동
					mThreadPoliceCheck = new Thread(new Runnable() {
						public void run() {
							mPoliceBtnFlag = true;
							SystemClock.sleep(Constants.DELAY_BUTTON_PUSH);
							mPoliceBtnFlag = false;
						}
					});
					
					mThreadPoliceCheck.start();
				} else {
					Toast.makeText(mContext, "긴급 버튼은 연속으로 누르실 수 없습니다.", Toast.LENGTH_SHORT).show();
				}
        		
				break;
			case R.id.emergency_mode_btn_notify:
				// 위급 상황을 등록된 사용자에게 알림
				if(!mEmergencyBtnFlag) {

					// 위급 상황에 따라 블랙 박스 파일 전송 하도록 한다.
					mMessagePool.setEmergencyStateFlag(true);
					
					// 위급 상황을 취소할 수 있는 버튼을 활성화 한다.
					mBtnEmergencyCancel.setVisibility(View.VISIBLE);
					
					String helpMsg = mConfigPref.getString(Constants.SETTING_EMERGENCY_MESSAGE, Constants.DEFAULT_EMERGENCY_MESSAGE)
					 					+ "(위치 : " + mMyAddress + ")";
					String locMsg = null;
					
					if(mCurrentLocation != null) {
						locMsg = "(위치 확인 : http://maps.google.co.kr/maps?q=" +
											 mCurrentLocation.getLatitude() + "+" + mCurrentLocation.getLongitude() + ")";
					}
					
					Utils.sendMMS(mContext, helpMsg, locMsg);
					Toast.makeText(mContext, "위급 메시지를 전송하였습니다.", Toast.LENGTH_SHORT).show();
					
					mThreadEmergencyCheck = new Thread(new Runnable() {
						public void run() {
							mEmergencyBtnFlag = true;
							SystemClock.sleep(Constants.DELAY_BUTTON_PUSH);
							mEmergencyBtnFlag = false;
						}
					});
					
					mThreadEmergencyCheck.start();
				} else {
					Toast.makeText(mContext, "긴급 버튼은 연속으로 누르실 수 없습니다.", Toast.LENGTH_SHORT).show();
				}
				break;
				
			case R.id.emergency_mode_btn_cancel:
				
				// 위급 상황이 취소 되었다고 설정.
				mMessagePool.setEmergencyStateFlag(false);
				
				mBtnEmergencyCancel.setVisibility(View.GONE);
				
				
			default:
				break;
		}
	}
}
