package madcat.safereturn.menu;

import madcat.safereturn.pro.R;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.dialog.SettingEmailSetDialog;
import madcat.safereturn.dialog.SettingEmgMsgDialog;
import madcat.safereturn.dialog.SettingFakeDialog;
import madcat.safereturn.dialog.SettingSensorDialog;
import madcat.safereturn.dialog.SettingUserNameDialog;
import madcat.safereturn.dialog.SettingUserRangeDialog;
import madcat.safereturn.settings.SettingEmgTelList;
import madcat.safereturn.settings.SettingMailList;
import madcat.safereturn.settings.SettingPathList;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.view.Window;
import android.widget.Toast;

public class Settings extends PreferenceActivity implements OnPreferenceClickListener {
	
	private final String TAG										=	"Config";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private Preference mPrefEmgMsg, mPrefEmgTelList, mPrefEmgEmail, mPrefEmgSensor;
	private Preference mPrefUserName, mPrefUserRange, mPrefUserPath;
	private Preference mPrefFake;
	
	private CheckBoxPreference mPrefBBEnable;
	private Preference mPrefBBMail, mPrefBBTimeCut;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.safe_return_config);
		
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		// Resource Id 설정
		mPrefEmgMsg = (Preference)findPreference("pref_config_emergency_messge");
		mPrefEmgTelList = (Preference)findPreference("pref_config_emergency_tel");
		mPrefEmgEmail = (Preference)findPreference("pref_config_emergency_mail");
		mPrefEmgSensor = (Preference)findPreference("pref_config_emergency_sensor");
		
		mPrefUserName = (Preference)findPreference("pref_config_user_name");
		mPrefUserRange = (Preference)findPreference("pref_config_user_range");
		mPrefUserPath = (Preference)findPreference("pref_config_user_path");
		
		mPrefFake = (Preference)findPreference("pref_config_fake");
		
		mPrefBBEnable = (CheckBoxPreference)findPreference("pref_config_blackbox_enable");
		mPrefBBTimeCut = (Preference)findPreference("pref_config_blackbox_timecut");
		mPrefBBMail = (Preference)findPreference("pref_config_blackbox_mail");
		
		// Event Listener 설정
		mPrefEmgMsg.setOnPreferenceClickListener(this);
		mPrefEmgTelList.setOnPreferenceClickListener(this);
		mPrefEmgEmail.setOnPreferenceClickListener(this);
		mPrefEmgSensor.setOnPreferenceClickListener(this);
		
		mPrefUserName.setOnPreferenceClickListener(this);
		mPrefUserRange.setOnPreferenceClickListener(this);
		mPrefUserPath.setOnPreferenceClickListener(this);
		
		mPrefFake.setOnPreferenceClickListener(this);
		
		mPrefBBEnable.setOnPreferenceClickListener(this);
		mPrefBBTimeCut.setOnPreferenceClickListener(this);
		mPrefBBMail.setOnPreferenceClickListener(this);
		
		
		// 기본 값 설정
		
		// 블랙 박스 설정
		setBBoxEnable(mConfigPref.getBoolean(Constants.SETTING_BBOX_ENABLE, true));
		
	}
	
	private void setBBoxEnable(boolean flag) {
		if(flag) {
			mPrefBBEnable.setChecked(true);
			mPrefBBTimeCut.setEnabled(true);
			mPrefBBMail.setEnabled(true);
			
			mPrefBBTimeCut.setSelectable(true);
			mPrefBBMail.setSelectable(true);
			
		} else {
			mPrefBBEnable.setChecked(false);
			mPrefBBTimeCut.setEnabled(false);
			mPrefBBMail.setEnabled(false);
			
			mPrefBBTimeCut.setSelectable(false);
			mPrefBBMail.setSelectable(false);
		}
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		if(preference.getKey().equals("pref_config_emergency_messge")) {

			SettingEmgMsgDialog modifyEmgMsgDialog = new SettingEmgMsgDialog(mContext);
			modifyEmgMsgDialog.show();
		
			return true;
		} else if(preference.getKey().equals("pref_config_emergency_tel")) {
			
			Intent emgTelIntent = new Intent(Settings.this, SettingEmgTelList.class);
			startActivity(emgTelIntent);
			overridePendingTransition(R.anim.fade, R.anim.hold);
			
			return true;
		} else if(preference.getKey().equals("pref_config_emergency_mail")) {
			
			Intent emgEmailIntent = new Intent(Settings.this, SettingMailList.class);
			startActivity(emgEmailIntent);
			overridePendingTransition(R.anim.fade, R.anim.hold);
			
			return true;
		} else if(preference.getKey().equals("pref_config_emergency_sensor")) {
			
			SettingSensorDialog modifySensorDialog = new SettingSensorDialog(mContext);
			modifySensorDialog.show();
			
			
			return true;
		} else if(preference.getKey().equals("pref_config_user_name")) {
			
			SettingUserNameDialog modifyNameDialog = new SettingUserNameDialog(mContext);
			modifyNameDialog.show();
			
			return true;
		} else if(preference.getKey().equals("pref_config_user_range")) {
			
			SettingUserRangeDialog modifyRangeDialog = new SettingUserRangeDialog(mContext);
			modifyRangeDialog.show();
			
			return true;
		} else if(preference.getKey().equals("pref_config_user_path")) {
			
			Intent pathIntent = new Intent(Settings.this, SettingPathList.class);
			startActivity(pathIntent);
			overridePendingTransition(R.anim.fade, R.anim.hold);
			
			return true;
		} else if(preference.getKey().equals("pref_config_fake")) {
			
			SettingFakeDialog modifyFakeDialog = new SettingFakeDialog(mContext);
			modifyFakeDialog.show();
			
			return true;
		} else if(preference.getKey().equals("pref_config_blackbox_enable")) {
			
			if(mPrefBBEnable.isChecked()) {
				setBBoxEnable(true);
			} else {
				setBBoxEnable(false);
			}
			
			SharedPreferences.Editor editor = mConfigPref.edit();
			editor.putBoolean(Constants.SETTING_BBOX_ENABLE, mPrefBBEnable.isChecked());
			editor.commit();
			
			return true;
		} else if(preference.getKey().equals("pref_config_blackbox_timecut")) {
			String[] timeCutItems = {"10분", "20분", "30분"};
			
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle(getString(R.string.setting_bbox_timecut));
			builder.setSingleChoiceItems(timeCutItems, ((mConfigPref.getInt(Constants.SETTING_BBOX_TIME_CUT, 20) / 10) - 1), new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					SharedPreferences.Editor editor = mConfigPref.edit();
					editor.putInt(Constants.SETTING_BBOX_TIME_CUT, ((which + 1) * 10));
					editor.commit();
					
					dialog.dismiss();
				}
			});

			builder.show();
			
			
			return true;
		} else if(preference.getKey().equals("pref_config_blackbox_mail")) {
			
			SettingEmailSetDialog emailSetDialog = new SettingEmailSetDialog(mContext);
			emailSetDialog.show();
			
			
			return true;
		} 
		
		return false;
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}

}











