package madcat.studio.update;

import java.util.ArrayList;

import madcat.safereturn.adapter.EmergencyAddress;
import madcat.safereturn.adapter.PathLocation;
import madcat.safereturn.constants.Constants;
import madcat.safereturn.load.LoadDatabase;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Update {
	
	private final String TAG										=	"Update";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	
	public Update(Context context) {
		mContext = context;
	}
	
	// 기존 db에서 친구 목록과 사용자 경로 저장 목록을 읽어와서 저장.
	
	public void execUpdate_1() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "2011-12-08 업데이트를 시작합니다.");
		}
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

		// 친구 목록을 ArrayList 에 백업하는 과정
		ArrayList<EmergencyAddress> backupFriends = new ArrayList<EmergencyAddress>();
		
		String friendsSql = "SELECT * FROM " + Constants.TABLE_FRIENDS;
		Cursor friendsCursor = sdb.rawQuery(friendsSql, null);
		
		while(friendsCursor.moveToNext()) {
			EmergencyAddress friendsData = new EmergencyAddress(friendsCursor.getString(1), friendsCursor.getString(2));
			backupFriends.add(friendsData);
		}
		
		friendsCursor.close();
		
		// 사용자 경로를 ArrayList 에 백업하는 과정
		ArrayList<PathLocation> backupPath = new ArrayList<PathLocation>();
		
		String pathSql = "SELECT * FROM " + Constants.TABLE_MYLOCATION;
		Cursor pathCursor = sdb.rawQuery(pathSql, null);
		
		while(pathCursor.moveToNext()) {
			PathLocation pathData = new PathLocation(
					pathCursor.getString(1), pathCursor.getDouble(2), pathCursor.getDouble(3), pathCursor.getString(4), 
					pathCursor.getString(5), pathCursor.getDouble(6), pathCursor.getDouble(7), pathCursor.getString(8), 
					pathCursor.getInt(9), pathCursor.getInt(10), pathCursor.getBlob(11));
			backupPath.add(pathData);
		}
		
		pathCursor.close();

		sdb.close();

		// DB 를 새롭게 갱신하였다면
		if(LoadDatabase.loadDataBase(mContext.getResources().getAssets())) {
			SQLiteDatabase updateDB = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			
			for(int i=0; i < backupFriends.size(); i++) {
				ContentValues values = new ContentValues();
				values.put(Constants.ATTRIBUTE_FRIENDS_NAME, backupFriends.get(i).getFriendsName());
				values.put(Constants.ATTRIBUTE_FRIENDS_TEL, backupFriends.get(i).getFriendsTel());
				
				updateDB.insert(Constants.TABLE_FRIENDS, null, values);
			}
			
			for(int i=0; i < backupPath.size(); i++) {
				ContentValues values = new ContentValues();
				
				values.put(Constants.ATTRIBUTE_MYLOCATION_START_NAME, backupPath.get(i).getStartName());
				values.put(Constants.ATTRIBUTE_MYLOCATION_START_LAT, backupPath.get(i).getStartLat());
				values.put(Constants.ATTRIBUTE_MYLOCATION_START_LON, backupPath.get(i).getStartLon());
				values.put(Constants.ATTRIBUTE_MYLOCATION_START_ADDR, backupPath.get(i).getStartAddr());
				values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_NAME, backupPath.get(i).getDestName());
				values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_LAT, backupPath.get(i).getDestLat());
				values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_LON, backupPath.get(i).getDestLon());
				values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_ADDR, backupPath.get(i).getDestAddr());
				values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_HOUR, backupPath.get(i).getDestHour());
				values.put(Constants.ATTRIBUTE_MYLOCATION_DEST_MIN, backupPath.get(i).getDestMin());
				values.put(Constants.ATTRIBUTE_MYLOCATION_PATH_BYTES, backupPath.get(i).getPathBytes());
			
				updateDB.insert(Constants.TABLE_MYLOCATION, null, values);
			}
			
			updateDB.close();
		}
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "업데이트를 완료하였습니다.");
		}
		
	}
}
